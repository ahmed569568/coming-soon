if (
  ((function(e, t) {
    'object' == typeof module && 'object' == typeof module.exports
      ? (module.exports = e.document
          ? t(e, !0)
          : function(e) {
              if (!e.document) throw new Error('jQuery requires a window with a document');
              return t(e);
            })
      : t(e);
  })('undefined' != typeof window ? window : this, function(e, t) {
    var n = [],
      i = e.document,
      o = n.slice,
      r = n.concat,
      a = n.push,
      s = n.indexOf,
      l = {},
      c = l.toString,
      u = l.hasOwnProperty,
      d = {},
      p = '1.12.4',
      f = function(e, t) {
        return new f.fn.init(e, t);
      },
      h = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      m = /^-ms-/,
      v = /-([\da-z])/gi,
      g = function(e, t) {
        return t.toUpperCase();
      };
    function y(e) {
      var t = !!e && 'length' in e && e.length,
        n = f.type(e);
      return (
        'function' !== n &&
        !f.isWindow(e) &&
        ('array' === n || 0 === t || ('number' == typeof t && t > 0 && t - 1 in e))
      );
    }
    (f.fn = f.prototype = {
      jquery: p,
      constructor: f,
      selector: '',
      length: 0,
      toArray: function() {
        return o.call(this);
      },
      get: function(e) {
        return null != e ? (0 > e ? this[e + this.length] : this[e]) : o.call(this);
      },
      pushStack: function(e) {
        var t = f.merge(this.constructor(), e);
        return (t.prevObject = this), (t.context = this.context), t;
      },
      each: function(e) {
        return f.each(this, e);
      },
      map: function(e) {
        return this.pushStack(
          f.map(this, function(t, n) {
            return e.call(t, n, t);
          })
        );
      },
      slice: function() {
        return this.pushStack(o.apply(this, arguments));
      },
      first: function() {
        return this.eq(0);
      },
      last: function() {
        return this.eq(-1);
      },
      eq: function(e) {
        var t = this.length,
          n = +e + (0 > e ? t : 0);
        return this.pushStack(n >= 0 && t > n ? [this[n]] : []);
      },
      end: function() {
        return this.prevObject || this.constructor();
      },
      push: a,
      sort: n.sort,
      splice: n.splice
    }),
      (f.extend = f.fn.extend = function() {
        var e,
          t,
          n,
          i,
          o,
          r,
          a = arguments[0] || {},
          s = 1,
          l = arguments.length,
          c = !1;
        for (
          'boolean' == typeof a && ((c = a), (a = arguments[s] || {}), s++),
            'object' == typeof a || f.isFunction(a) || (a = {}),
            s === l && ((a = this), s--);
          l > s;
          s++
        )
          if (null != (o = arguments[s]))
            for (i in o)
              (e = a[i]),
                a !== (n = o[i]) &&
                  (c && n && (f.isPlainObject(n) || (t = f.isArray(n)))
                    ? (t ? ((t = !1), (r = e && f.isArray(e) ? e : [])) : (r = e && f.isPlainObject(e) ? e : {}),
                      (a[i] = f.extend(c, r, n)))
                    : void 0 !== n && (a[i] = n));
        return a;
      }),
      f.extend({
        expando: 'jQuery' + (p + Math.random()).replace(/\D/g, ''),
        isReady: !0,
        error: function(e) {
          throw new Error(e);
        },
        noop: function() {},
        isFunction: function(e) {
          return 'function' === f.type(e);
        },
        isArray:
          Array.isArray ||
          function(e) {
            return 'array' === f.type(e);
          },
        isWindow: function(e) {
          return null != e && e == e.window;
        },
        isNumeric: function(e) {
          var t = e && e.toString();
          return !f.isArray(e) && t - parseFloat(t) + 1 >= 0;
        },
        isEmptyObject: function(e) {
          var t;
          for (t in e) return !1;
          return !0;
        },
        isPlainObject: function(e) {
          var t;
          if (!e || 'object' !== f.type(e) || e.nodeType || f.isWindow(e)) return !1;
          try {
            if (e.constructor && !u.call(e, 'constructor') && !u.call(e.constructor.prototype, 'isPrototypeOf'))
              return !1;
          } catch (n) {
            return !1;
          }
          if (!d.ownFirst) for (t in e) return u.call(e, t);
          for (t in e);
          return void 0 === t || u.call(e, t);
        },
        type: function(e) {
          return null == e
            ? e + ''
            : 'object' == typeof e || 'function' == typeof e
            ? l[c.call(e)] || 'object'
            : typeof e;
        },
        globalEval: function(t) {
          t &&
            f.trim(t) &&
            (
              e.execScript ||
              function(t) {
                e.eval.call(e, t);
              }
            )(t);
        },
        camelCase: function(e) {
          return e.replace(m, 'ms-').replace(v, g);
        },
        nodeName: function(e, t) {
          return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
        },
        each: function(e, t) {
          var n,
            i = 0;
          if (y(e)) for (n = e.length; n > i && !1 !== t.call(e[i], i, e[i]); i++);
          else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
          return e;
        },
        trim: function(e) {
          return null == e ? '' : (e + '').replace(h, '');
        },
        makeArray: function(e, t) {
          var n = t || [];
          return null != e && (y(Object(e)) ? f.merge(n, 'string' == typeof e ? [e] : e) : a.call(n, e)), n;
        },
        inArray: function(e, t, n) {
          var i;
          if (t) {
            if (s) return s.call(t, e, n);
            for (i = t.length, n = n ? (0 > n ? Math.max(0, i + n) : n) : 0; i > n; n++)
              if (n in t && t[n] === e) return n;
          }
          return -1;
        },
        merge: function(e, t) {
          for (var n = +t.length, i = 0, o = e.length; n > i; ) e[o++] = t[i++];
          if (n != n) for (; void 0 !== t[i]; ) e[o++] = t[i++];
          return (e.length = o), e;
        },
        grep: function(e, t, n) {
          for (var i = [], o = 0, r = e.length, a = !n; r > o; o++) !t(e[o], o) !== a && i.push(e[o]);
          return i;
        },
        map: function(e, t, n) {
          var i,
            o,
            a = 0,
            s = [];
          if (y(e)) for (i = e.length; i > a; a++) null != (o = t(e[a], a, n)) && s.push(o);
          else for (a in e) null != (o = t(e[a], a, n)) && s.push(o);
          return r.apply([], s);
        },
        guid: 1,
        proxy: function(e, t) {
          var n, i, r;
          return (
            'string' == typeof t && ((r = e[t]), (t = e), (e = r)),
            f.isFunction(e)
              ? ((n = o.call(arguments, 2)),
                ((i = function() {
                  return e.apply(t || this, n.concat(o.call(arguments)));
                }).guid = e.guid = e.guid || f.guid++),
                i)
              : void 0
          );
        },
        now: function() {
          return +new Date();
        },
        support: d
      }),
      'function' == typeof Symbol && (f.fn[Symbol.iterator] = n[Symbol.iterator]),
      f.each('Boolean Number String Function Array Date RegExp Object Error Symbol'.split(' '), function(e, t) {
        l['[object ' + t + ']'] = t.toLowerCase();
      });
    var b = (function(e) {
      var t,
        n,
        i,
        o,
        r,
        a,
        s,
        l,
        c,
        u,
        d,
        p,
        f,
        h,
        m,
        v,
        g,
        y,
        b,
        x = 'sizzle' + 1 * new Date(),
        w = e.document,
        C = 0,
        T = 0,
        S = re(),
        E = re(),
        k = re(),
        _ = function(e, t) {
          return e === t && (d = !0), 0;
        },
        A = {}.hasOwnProperty,
        D = [],
        I = D.pop,
        N = D.push,
        $ = D.push,
        O = D.slice,
        R = function(e, t) {
          for (var n = 0, i = e.length; i > n; n++) if (e[n] === t) return n;
          return -1;
        },
        M =
          'checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped',
        L = '[\\x20\\t\\r\\n\\f]',
        F = '(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+',
        j =
          '\\[' +
          L +
          '*(' +
          F +
          ')(?:' +
          L +
          '*([*^$|!~]?=)' +
          L +
          '*(?:\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)"|(' +
          F +
          '))|)' +
          L +
          '*\\]',
        P =
          ':(' +
          F +
          ')(?:\\(((\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)")|((?:\\\\.|[^\\\\()[\\]]|' +
          j +
          ')*)|.*)\\)|)',
        q = new RegExp(L + '+', 'g'),
        H = new RegExp('^' + L + '+|((?:^|[^\\\\])(?:\\\\.)*)' + L + '+$', 'g'),
        z = new RegExp('^' + L + '*,' + L + '*'),
        B = new RegExp('^' + L + '*([>+~]|' + L + ')' + L + '*'),
        W = new RegExp('=' + L + '*([^\\]\'"]*?)' + L + '*\\]', 'g'),
        U = new RegExp(P),
        Z = new RegExp('^' + F + '$'),
        V = {
          ID: new RegExp('^#(' + F + ')'),
          CLASS: new RegExp('^\\.(' + F + ')'),
          TAG: new RegExp('^(' + F + '|[*])'),
          ATTR: new RegExp('^' + j),
          PSEUDO: new RegExp('^' + P),
          CHILD: new RegExp(
            '^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(' +
              L +
              '*(even|odd|(([+-]|)(\\d*)n|)' +
              L +
              '*(?:([+-]|)' +
              L +
              '*(\\d+)|))' +
              L +
              '*\\)|)',
            'i'
          ),
          bool: new RegExp('^(?:' + M + ')$', 'i'),
          needsContext: new RegExp(
            '^' +
              L +
              '*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(' +
              L +
              '*((?:-\\d)?\\d*)' +
              L +
              '*\\)|)(?=[^-]|$)',
            'i'
          )
        },
        X = /^(?:input|select|textarea|button)$/i,
        K = /^h\d$/i,
        Y = /^[^{]+\{\s*\[native \w/,
        G = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        J = /[+~]/,
        Q = /'|\\/g,
        ee = new RegExp('\\\\([\\da-f]{1,6}' + L + '?|(' + L + ')|.)', 'ig'),
        te = function(e, t, n) {
          var i = '0x' + t - 65536;
          return i != i || n
            ? t
            : 0 > i
            ? String.fromCharCode(i + 65536)
            : String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
        },
        ie = function() {
          p();
        };
      try {
        $.apply((D = O.call(w.childNodes)), w.childNodes);
      } catch (ne) {
        $ = {
          apply: D.length
            ? function(e, t) {
                N.apply(e, O.call(t));
              }
            : function(e, t) {
                for (var n = e.length, i = 0; (e[n++] = t[i++]); );
                e.length = n - 1;
              }
        };
      }
      function oe(e, t, i, o) {
        var r,
          s,
          c,
          u,
          d,
          h,
          g,
          y,
          C = t && t.ownerDocument,
          T = t ? t.nodeType : 9;
        if (((i = i || []), 'string' != typeof e || !e || (1 !== T && 9 !== T && 11 !== T))) return i;
        if (!o && ((t ? t.ownerDocument || t : w) !== f && p(t), (t = t || f), m)) {
          if (11 !== T && (h = G.exec(e)))
            if ((r = h[1])) {
              if (9 === T) {
                if (!(c = t.getElementById(r))) return i;
                if (c.id === r) return i.push(c), i;
              } else if (C && (c = C.getElementById(r)) && b(t, c) && c.id === r) return i.push(c), i;
            } else {
              if (h[2]) return $.apply(i, t.getElementsByTagName(e)), i;
              if ((r = h[3]) && n.getElementsByClassName && t.getElementsByClassName)
                return $.apply(i, t.getElementsByClassName(r)), i;
            }
          if (n.qsa && !k[e + ' '] && (!v || !v.test(e))) {
            if (1 !== T) (C = t), (y = e);
            else if ('object' !== t.nodeName.toLowerCase()) {
              for (
                (u = t.getAttribute('id')) ? (u = u.replace(Q, '\\$&')) : t.setAttribute('id', (u = x)),
                  s = (g = a(e)).length,
                  d = Z.test(u) ? '#' + u : "[id='" + u + "']";
                s--;

              )
                g[s] = d + ' ' + me(g[s]);
              (y = g.join(',')), (C = (J.test(e) && fe(t.parentNode)) || t);
            }
            if (y)
              try {
                return $.apply(i, C.querySelectorAll(y)), i;
              } catch (S) {
              } finally {
                u === x && t.removeAttribute('id');
              }
          }
        }
        return l(e.replace(H, '$1'), t, i, o);
      }
      function re() {
        var e = [];
        return function t(n, o) {
          return e.push(n + ' ') > i.cacheLength && delete t[e.shift()], (t[n + ' '] = o);
        };
      }
      function ae(e) {
        return (e[x] = !0), e;
      }
      function se(e) {
        var t = f.createElement('div');
        try {
          return !!e(t);
        } catch (n) {
          return !1;
        } finally {
          t.parentNode && t.parentNode.removeChild(t), (t = null);
        }
      }
      function le(e, t) {
        for (var n = e.split('|'), o = n.length; o--; ) i.attrHandle[n[o]] = t;
      }
      function ce(e, t) {
        var n = t && e,
          i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || 1 << 31) - (~e.sourceIndex || 1 << 31);
        if (i) return i;
        if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
        return e ? 1 : -1;
      }
      function ue(e) {
        return function(t) {
          return 'input' === t.nodeName.toLowerCase() && t.type === e;
        };
      }
      function de(e) {
        return function(t) {
          var n = t.nodeName.toLowerCase();
          return ('input' === n || 'button' === n) && t.type === e;
        };
      }
      function pe(e) {
        return ae(function(t) {
          return (
            (t = +t),
            ae(function(n, i) {
              for (var o, r = e([], n.length, t), a = r.length; a--; ) n[(o = r[a])] && (n[o] = !(i[o] = n[o]));
            })
          );
        });
      }
      function fe(e) {
        return e && void 0 !== e.getElementsByTagName && e;
      }
      for (t in ((n = oe.support = {}),
      (r = oe.isXML = function(e) {
        var t = e && (e.ownerDocument || e).documentElement;
        return !!t && 'HTML' !== t.nodeName;
      }),
      (p = oe.setDocument = function(e) {
        var t,
          o,
          a = e ? e.ownerDocument || e : w;
        return a !== f && 9 === a.nodeType && a.documentElement
          ? ((h = (f = a).documentElement),
            (m = !r(f)),
            (o = f.defaultView) &&
              o.top !== o &&
              (o.addEventListener
                ? o.addEventListener('unload', ie, !1)
                : o.attachEvent && o.attachEvent('onunload', ie)),
            (n.attributes = se(function(e) {
              return (e.className = 'i'), !e.getAttribute('className');
            })),
            (n.getElementsByTagName = se(function(e) {
              return e.appendChild(f.createComment('')), !e.getElementsByTagName('*').length;
            })),
            (n.getElementsByClassName = Y.test(f.getElementsByClassName)),
            (n.getById = se(function(e) {
              return (h.appendChild(e).id = x), !f.getElementsByName || !f.getElementsByName(x).length;
            })),
            n.getById
              ? ((i.find.ID = function(e, t) {
                  if (void 0 !== t.getElementById && m) {
                    var n = t.getElementById(e);
                    return n ? [n] : [];
                  }
                }),
                (i.filter.ID = function(e) {
                  var t = e.replace(ee, te);
                  return function(e) {
                    return e.getAttribute('id') === t;
                  };
                }))
              : (delete i.find.ID,
                (i.filter.ID = function(e) {
                  var t = e.replace(ee, te);
                  return function(e) {
                    var n = void 0 !== e.getAttributeNode && e.getAttributeNode('id');
                    return n && n.value === t;
                  };
                })),
            (i.find.TAG = n.getElementsByTagName
              ? function(e, t) {
                  return void 0 !== t.getElementsByTagName
                    ? t.getElementsByTagName(e)
                    : n.qsa
                    ? t.querySelectorAll(e)
                    : void 0;
                }
              : function(e, t) {
                  var n,
                    i = [],
                    o = 0,
                    r = t.getElementsByTagName(e);
                  if ('*' === e) {
                    for (; (n = r[o++]); ) 1 === n.nodeType && i.push(n);
                    return i;
                  }
                  return r;
                }),
            (i.find.CLASS =
              n.getElementsByClassName &&
              function(e, t) {
                return void 0 !== t.getElementsByClassName && m ? t.getElementsByClassName(e) : void 0;
              }),
            (g = []),
            (v = []),
            (n.qsa = Y.test(f.querySelectorAll)) &&
              (se(function(e) {
                (h.appendChild(e).innerHTML =
                  "<a id='" +
                  x +
                  "'></a><select id='" +
                  x +
                  "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                  e.querySelectorAll("[msallowcapture^='']").length && v.push('[*^$]=' + L + '*(?:\'\'|"")'),
                  e.querySelectorAll('[selected]').length || v.push('\\[' + L + '*(?:value|' + M + ')'),
                  e.querySelectorAll('[id~=' + x + '-]').length || v.push('~='),
                  e.querySelectorAll(':checked').length || v.push(':checked'),
                  e.querySelectorAll('a#' + x + '+*').length || v.push('.#.+[+~]');
              }),
              se(function(e) {
                var t = f.createElement('input');
                t.setAttribute('type', 'hidden'),
                  e.appendChild(t).setAttribute('name', 'D'),
                  e.querySelectorAll('[name=d]').length && v.push('name' + L + '*[*^$|!~]?='),
                  e.querySelectorAll(':enabled').length || v.push(':enabled', ':disabled'),
                  e.querySelectorAll('*,:x'),
                  v.push(',.*:');
              })),
            (n.matchesSelector = Y.test(
              (y =
                h.matches ||
                h.webkitMatchesSelector ||
                h.mozMatchesSelector ||
                h.oMatchesSelector ||
                h.msMatchesSelector)
            )) &&
              se(function(e) {
                (n.disconnectedMatch = y.call(e, 'div')), y.call(e, "[s!='']:x"), g.push('!=', P);
              }),
            (v = v.length && new RegExp(v.join('|'))),
            (g = g.length && new RegExp(g.join('|'))),
            (t = Y.test(h.compareDocumentPosition)),
            (b =
              t || Y.test(h.contains)
                ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                      i = t && t.parentNode;
                    return (
                      e === i ||
                      !(
                        !i ||
                        1 !== i.nodeType ||
                        !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i))
                      )
                    );
                  }
                : function(e, t) {
                    if (t) for (; (t = t.parentNode); ) if (t === e) return !0;
                    return !1;
                  }),
            (_ = t
              ? function(e, t) {
                  if (e === t) return (d = !0), 0;
                  var i = !e.compareDocumentPosition - !t.compareDocumentPosition;
                  return (
                    i ||
                    (1 & (i = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) ||
                    (!n.sortDetached && t.compareDocumentPosition(e) === i)
                      ? e === f || (e.ownerDocument === w && b(w, e))
                        ? -1
                        : t === f || (t.ownerDocument === w && b(w, t))
                        ? 1
                        : u
                        ? R(u, e) - R(u, t)
                        : 0
                      : 4 & i
                      ? -1
                      : 1)
                  );
                }
              : function(e, t) {
                  if (e === t) return (d = !0), 0;
                  var n,
                    i = 0,
                    o = e.parentNode,
                    r = t.parentNode,
                    a = [e],
                    s = [t];
                  if (!o || !r) return e === f ? -1 : t === f ? 1 : o ? -1 : r ? 1 : u ? R(u, e) - R(u, t) : 0;
                  if (o === r) return ce(e, t);
                  for (n = e; (n = n.parentNode); ) a.unshift(n);
                  for (n = t; (n = n.parentNode); ) s.unshift(n);
                  for (; a[i] === s[i]; ) i++;
                  return i ? ce(a[i], s[i]) : a[i] === w ? -1 : s[i] === w ? 1 : 0;
                }),
            f)
          : f;
      }),
      (oe.matches = function(e, t) {
        return oe(e, null, null, t);
      }),
      (oe.matchesSelector = function(e, t) {
        if (
          ((e.ownerDocument || e) !== f && p(e),
          (t = t.replace(W, "='$1']")),
          n.matchesSelector && m && !k[t + ' '] && (!g || !g.test(t)) && (!v || !v.test(t)))
        )
          try {
            var i = y.call(e, t);
            if (i || n.disconnectedMatch || (e.document && 11 !== e.document.nodeType)) return i;
          } catch (o) {}
        return oe(t, f, null, [e]).length > 0;
      }),
      (oe.contains = function(e, t) {
        return (e.ownerDocument || e) !== f && p(e), b(e, t);
      }),
      (oe.attr = function(e, t) {
        (e.ownerDocument || e) !== f && p(e);
        var o = i.attrHandle[t.toLowerCase()],
          r = o && A.call(i.attrHandle, t.toLowerCase()) ? o(e, t, !m) : void 0;
        return void 0 !== r
          ? r
          : n.attributes || !m
          ? e.getAttribute(t)
          : (r = e.getAttributeNode(t)) && r.specified
          ? r.value
          : null;
      }),
      (oe.error = function(e) {
        throw new Error('Syntax error, unrecognized expression: ' + e);
      }),
      (oe.uniqueSort = function(e) {
        var t,
          i = [],
          o = 0,
          r = 0;
        if (((d = !n.detectDuplicates), (u = !n.sortStable && e.slice(0)), e.sort(_), d)) {
          for (; (t = e[r++]); ) t === e[r] && (o = i.push(r));
          for (; o--; ) e.splice(i[o], 1);
        }
        return (u = null), e;
      }),
      (o = oe.getText = function(e) {
        var t,
          n = '',
          i = 0,
          r = e.nodeType;
        if (r) {
          if (1 === r || 9 === r || 11 === r) {
            if ('string' == typeof e.textContent) return e.textContent;
            for (e = e.firstChild; e; e = e.nextSibling) n += o(e);
          } else if (3 === r || 4 === r) return e.nodeValue;
        } else for (; (t = e[i++]); ) n += o(t);
        return n;
      }),
      ((i = oe.selectors = {
        cacheLength: 50,
        createPseudo: ae,
        match: V,
        attrHandle: {},
        find: {},
        relative: {
          '>': { dir: 'parentNode', first: !0 },
          ' ': { dir: 'parentNode' },
          '+': { dir: 'previousSibling', first: !0 },
          '~': { dir: 'previousSibling' }
        },
        preFilter: {
          ATTR: function(e) {
            return (
              (e[1] = e[1].replace(ee, te)),
              (e[3] = (e[3] || e[4] || e[5] || '').replace(ee, te)),
              '~=' === e[2] && (e[3] = ' ' + e[3] + ' '),
              e.slice(0, 4)
            );
          },
          CHILD: function(e) {
            return (
              (e[1] = e[1].toLowerCase()),
              'nth' === e[1].slice(0, 3)
                ? (e[3] || oe.error(e[0]),
                  (e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ('even' === e[3] || 'odd' === e[3]))),
                  (e[5] = +(e[7] + e[8] || 'odd' === e[3])))
                : e[3] && oe.error(e[0]),
              e
            );
          },
          PSEUDO: function(e) {
            var t,
              n = !e[6] && e[2];
            return V.CHILD.test(e[0])
              ? null
              : (e[3]
                  ? (e[2] = e[4] || e[5] || '')
                  : n &&
                    U.test(n) &&
                    (t = a(n, !0)) &&
                    (t = n.indexOf(')', n.length - t) - n.length) &&
                    ((e[0] = e[0].slice(0, t)), (e[2] = n.slice(0, t))),
                e.slice(0, 3));
          }
        },
        filter: {
          TAG: function(e) {
            var t = e.replace(ee, te).toLowerCase();
            return '*' === e
              ? function() {
                  return !0;
                }
              : function(e) {
                  return e.nodeName && e.nodeName.toLowerCase() === t;
                };
          },
          CLASS: function(e) {
            var t = S[e + ' '];
            return (
              t ||
              ((t = new RegExp('(^|' + L + ')' + e + '(' + L + '|$)')) &&
                S(e, function(e) {
                  return t.test(
                    ('string' == typeof e.className && e.className) ||
                      (void 0 !== e.getAttribute && e.getAttribute('class')) ||
                      ''
                  );
                }))
            );
          },
          ATTR: function(e, t, n) {
            return function(i) {
              var o = oe.attr(i, e);
              return null == o
                ? '!=' === t
                : !t ||
                    ((o += ''),
                    '=' === t
                      ? o === n
                      : '!=' === t
                      ? o !== n
                      : '^=' === t
                      ? n && 0 === o.indexOf(n)
                      : '*=' === t
                      ? n && o.indexOf(n) > -1
                      : '$=' === t
                      ? n && o.slice(-n.length) === n
                      : '~=' === t
                      ? (' ' + o.replace(q, ' ') + ' ').indexOf(n) > -1
                      : '|=' === t && (o === n || o.slice(0, n.length + 1) === n + '-'));
            };
          },
          CHILD: function(e, t, n, i, o) {
            var r = 'nth' !== e.slice(0, 3),
              a = 'last' !== e.slice(-4),
              s = 'of-type' === t;
            return 1 === i && 0 === o
              ? function(e) {
                  return !!e.parentNode;
                }
              : function(t, n, l) {
                  var c,
                    u,
                    d,
                    p,
                    f,
                    h,
                    m = r !== a ? 'nextSibling' : 'previousSibling',
                    v = t.parentNode,
                    g = s && t.nodeName.toLowerCase(),
                    y = !l && !s,
                    b = !1;
                  if (v) {
                    if (r) {
                      for (; m; ) {
                        for (p = t; (p = p[m]); ) if (s ? p.nodeName.toLowerCase() === g : 1 === p.nodeType) return !1;
                        h = m = 'only' === e && !h && 'nextSibling';
                      }
                      return !0;
                    }
                    if (((h = [a ? v.firstChild : v.lastChild]), a && y)) {
                      for (
                        b =
                          (f =
                            (c =
                              (u = (d = (p = v)[x] || (p[x] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] || [])[0] ===
                              C && c[1]) && c[2],
                          p = f && v.childNodes[f];
                        (p = (++f && p && p[m]) || (b = f = 0) || h.pop());

                      )
                        if (1 === p.nodeType && ++b && p === t) {
                          u[e] = [C, f, b];
                          break;
                        }
                    } else if (
                      (y &&
                        (b = f =
                          (c =
                            (u = (d = (p = t)[x] || (p[x] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] || [])[0] ===
                            C && c[1]),
                      !1 === b)
                    )
                      for (
                        ;
                        (p = (++f && p && p[m]) || (b = f = 0) || h.pop()) &&
                        ((s ? p.nodeName.toLowerCase() !== g : 1 !== p.nodeType) ||
                          !++b ||
                          (y && ((u = (d = p[x] || (p[x] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] = [C, b]),
                          p !== t));

                      );
                    return (b -= o) === i || (b % i == 0 && b / i >= 0);
                  }
                };
          },
          PSEUDO: function(e, t) {
            var n,
              o = i.pseudos[e] || i.setFilters[e.toLowerCase()] || oe.error('unsupported pseudo: ' + e);
            return o[x]
              ? o(t)
              : o.length > 1
              ? ((n = [e, e, '', t]),
                i.setFilters.hasOwnProperty(e.toLowerCase())
                  ? ae(function(e, n) {
                      for (var i, r = o(e, t), a = r.length; a--; ) e[(i = R(e, r[a]))] = !(n[i] = r[a]);
                    })
                  : function(e) {
                      return o(e, 0, n);
                    })
              : o;
          }
        },
        pseudos: {
          not: ae(function(e) {
            var t = [],
              n = [],
              i = s(e.replace(H, '$1'));
            return i[x]
              ? ae(function(e, t, n, o) {
                  for (var r, a = i(e, null, o, []), s = e.length; s--; ) (r = a[s]) && (e[s] = !(t[s] = r));
                })
              : function(e, o, r) {
                  return (t[0] = e), i(t, null, r, n), (t[0] = null), !n.pop();
                };
          }),
          has: ae(function(e) {
            return function(t) {
              return oe(e, t).length > 0;
            };
          }),
          contains: ae(function(e) {
            return (
              (e = e.replace(ee, te)),
              function(t) {
                return (t.textContent || t.innerText || o(t)).indexOf(e) > -1;
              }
            );
          }),
          lang: ae(function(e) {
            return (
              Z.test(e || '') || oe.error('unsupported lang: ' + e),
              (e = e.replace(ee, te).toLowerCase()),
              function(t) {
                var n;
                do {
                  if ((n = m ? t.lang : t.getAttribute('xml:lang') || t.getAttribute('lang')))
                    return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + '-');
                } while ((t = t.parentNode) && 1 === t.nodeType);
                return !1;
              }
            );
          }),
          target: function(t) {
            var n = e.location && e.location.hash;
            return n && n.slice(1) === t.id;
          },
          root: function(e) {
            return e === h;
          },
          focus: function(e) {
            return e === f.activeElement && (!f.hasFocus || f.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
          },
          enabled: function(e) {
            return !1 === e.disabled;
          },
          disabled: function(e) {
            return !0 === e.disabled;
          },
          checked: function(e) {
            var t = e.nodeName.toLowerCase();
            return ('input' === t && !!e.checked) || ('option' === t && !!e.selected);
          },
          selected: function(e) {
            return !0 === e.selected;
          },
          empty: function(e) {
            for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
            return !0;
          },
          parent: function(e) {
            return !i.pseudos.empty(e);
          },
          header: function(e) {
            return K.test(e.nodeName);
          },
          input: function(e) {
            return X.test(e.nodeName);
          },
          button: function(e) {
            var t = e.nodeName.toLowerCase();
            return ('input' === t && 'button' === e.type) || 'button' === t;
          },
          text: function(e) {
            var t;
            return (
              'input' === e.nodeName.toLowerCase() &&
              'text' === e.type &&
              (null == (t = e.getAttribute('type')) || 'text' === t.toLowerCase())
            );
          },
          first: pe(function() {
            return [0];
          }),
          last: pe(function(e, t) {
            return [t - 1];
          }),
          eq: pe(function(e, t, n) {
            return [0 > n ? n + t : n];
          }),
          even: pe(function(e, t) {
            for (var n = 0; t > n; n += 2) e.push(n);
            return e;
          }),
          odd: pe(function(e, t) {
            for (var n = 1; t > n; n += 2) e.push(n);
            return e;
          }),
          lt: pe(function(e, t, n) {
            for (var i = 0 > n ? n + t : n; --i >= 0; ) e.push(i);
            return e;
          }),
          gt: pe(function(e, t, n) {
            for (var i = 0 > n ? n + t : n; ++i < t; ) e.push(i);
            return e;
          })
        }
      }).pseudos.nth = i.pseudos.eq),
      { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
        i.pseudos[t] = ue(t);
      for (t in { submit: !0, reset: !0 }) i.pseudos[t] = de(t);
      function he() {}
      function me(e) {
        for (var t = 0, n = e.length, i = ''; n > t; t++) i += e[t].value;
        return i;
      }
      function ve(e, t, n) {
        var i = t.dir,
          o = n && 'parentNode' === i,
          r = T++;
        return t.first
          ? function(t, n, r) {
              for (; (t = t[i]); ) if (1 === t.nodeType || o) return e(t, n, r);
            }
          : function(t, n, a) {
              var s,
                l,
                c,
                u = [C, r];
              if (a) {
                for (; (t = t[i]); ) if ((1 === t.nodeType || o) && e(t, n, a)) return !0;
              } else
                for (; (t = t[i]); )
                  if (1 === t.nodeType || o) {
                    if (
                      (s = (l = (c = t[x] || (t[x] = {}))[t.uniqueID] || (c[t.uniqueID] = {}))[i]) &&
                      s[0] === C &&
                      s[1] === r
                    )
                      return (u[2] = s[2]);
                    if (((l[i] = u), (u[2] = e(t, n, a)))) return !0;
                  }
            };
      }
      function ge(e) {
        return e.length > 1
          ? function(t, n, i) {
              for (var o = e.length; o--; ) if (!e[o](t, n, i)) return !1;
              return !0;
            }
          : e[0];
      }
      function ye(e, t, n, i, o) {
        for (var r, a = [], s = 0, l = e.length, c = null != t; l > s; s++)
          (r = e[s]) && ((n && !n(r, i, o)) || (a.push(r), c && t.push(s)));
        return a;
      }
      function be(e, t, n, i, o, r) {
        return (
          i && !i[x] && (i = be(i)),
          o && !o[x] && (o = be(o, r)),
          ae(function(r, a, s, l) {
            var c,
              u,
              d,
              p = [],
              f = [],
              h = a.length,
              m =
                r ||
                (function(e, t, n) {
                  for (var i = 0, o = t.length; o > i; i++) oe(e, t[i], n);
                  return n;
                })(t || '*', s.nodeType ? [s] : s, []),
              v = !e || (!r && t) ? m : ye(m, p, e, s, l),
              g = n ? (o || (r ? e : h || i) ? [] : a) : v;
            if ((n && n(v, g, s, l), i))
              for (c = ye(g, f), i(c, [], s, l), u = c.length; u--; ) (d = c[u]) && (g[f[u]] = !(v[f[u]] = d));
            if (r) {
              if (o || e) {
                if (o) {
                  for (c = [], u = g.length; u--; ) (d = g[u]) && c.push((v[u] = d));
                  o(null, (g = []), c, l);
                }
                for (u = g.length; u--; ) (d = g[u]) && (c = o ? R(r, d) : p[u]) > -1 && (r[c] = !(a[c] = d));
              }
            } else (g = ye(g === a ? g.splice(h, g.length) : g)), o ? o(null, a, g, l) : $.apply(a, g);
          })
        );
      }
      function xe(e) {
        for (
          var t,
            n,
            o,
            r = e.length,
            a = i.relative[e[0].type],
            s = a || i.relative[' '],
            l = a ? 1 : 0,
            u = ve(
              function(e) {
                return e === t;
              },
              s,
              !0
            ),
            d = ve(
              function(e) {
                return R(t, e) > -1;
              },
              s,
              !0
            ),
            p = [
              function(e, n, i) {
                var o = (!a && (i || n !== c)) || ((t = n).nodeType ? u(e, n, i) : d(e, n, i));
                return (t = null), o;
              }
            ];
          r > l;
          l++
        )
          if ((n = i.relative[e[l].type])) p = [ve(ge(p), n)];
          else {
            if ((n = i.filter[e[l].type].apply(null, e[l].matches))[x]) {
              for (o = ++l; r > o && !i.relative[e[o].type]; o++);
              return be(
                l > 1 && ge(p),
                l > 1 && me(e.slice(0, l - 1).concat({ value: ' ' === e[l - 2].type ? '*' : '' })).replace(H, '$1'),
                n,
                o > l && xe(e.slice(l, o)),
                r > o && xe((e = e.slice(o))),
                r > o && me(e)
              );
            }
            p.push(n);
          }
        return ge(p);
      }
      function we(e, t) {
        var n = t.length > 0,
          o = e.length > 0,
          r = function(r, a, s, l, u) {
            var d,
              h,
              v,
              g = 0,
              y = '0',
              b = r && [],
              x = [],
              w = c,
              T = r || (o && i.find.TAG('*', u)),
              S = (C += null == w ? 1 : Math.random() || 0.1),
              E = T.length;
            for (u && (c = a === f || a || u); y !== E && null != (d = T[y]); y++) {
              if (o && d) {
                for (h = 0, a || d.ownerDocument === f || (p(d), (s = !m)); (v = e[h++]); )
                  if (v(d, a || f, s)) {
                    l.push(d);
                    break;
                  }
                u && (C = S);
              }
              n && ((d = !v && d) && g--, r && b.push(d));
            }
            if (((g += y), n && y !== g)) {
              for (h = 0; (v = t[h++]); ) v(b, x, a, s);
              if (r) {
                if (g > 0) for (; y--; ) b[y] || x[y] || (x[y] = I.call(l));
                x = ye(x);
              }
              $.apply(l, x), u && !r && x.length > 0 && g + t.length > 1 && oe.uniqueSort(l);
            }
            return u && ((C = S), (c = w)), b;
          };
        return n ? ae(r) : r;
      }
      return (
        (he.prototype = i.filters = i.pseudos),
        (i.setFilters = new he()),
        (a = oe.tokenize = function(e, t) {
          var n,
            o,
            r,
            a,
            s,
            l,
            c,
            u = E[e + ' '];
          if (u) return t ? 0 : u.slice(0);
          for (s = e, l = [], c = i.preFilter; s; ) {
            for (a in ((n && !(o = z.exec(s))) || (o && (s = s.slice(o[0].length) || s), l.push((r = []))),
            (n = !1),
            (o = B.exec(s)) &&
              ((n = o.shift()), r.push({ value: n, type: o[0].replace(H, ' ') }), (s = s.slice(n.length))),
            i.filter))
              !(o = V[a].exec(s)) ||
                (c[a] && !(o = c[a](o))) ||
                ((n = o.shift()), r.push({ value: n, type: a, matches: o }), (s = s.slice(n.length)));
            if (!n) break;
          }
          return t ? s.length : s ? oe.error(e) : E(e, l).slice(0);
        }),
        (s = oe.compile = function(e, t) {
          var n,
            i = [],
            o = [],
            r = k[e + ' '];
          if (!r) {
            for (t || (t = a(e)), n = t.length; n--; ) (r = xe(t[n]))[x] ? i.push(r) : o.push(r);
            (r = k(e, we(o, i))).selector = e;
          }
          return r;
        }),
        (l = oe.select = function(e, t, o, r) {
          var l,
            c,
            u,
            d,
            p,
            f = 'function' == typeof e && e,
            h = !r && a((e = f.selector || e));
          if (((o = o || []), 1 === h.length)) {
            if (
              (c = h[0] = h[0].slice(0)).length > 2 &&
              'ID' === (u = c[0]).type &&
              n.getById &&
              9 === t.nodeType &&
              m &&
              i.relative[c[1].type]
            ) {
              if (!(t = (i.find.ID(u.matches[0].replace(ee, te), t) || [])[0])) return o;
              f && (t = t.parentNode), (e = e.slice(c.shift().value.length));
            }
            for (l = V.needsContext.test(e) ? 0 : c.length; l-- && !i.relative[(d = (u = c[l]).type)]; )
              if (
                (p = i.find[d]) &&
                (r = p(u.matches[0].replace(ee, te), (J.test(c[0].type) && fe(t.parentNode)) || t))
              ) {
                if ((c.splice(l, 1), !(e = r.length && me(c)))) return $.apply(o, r), o;
                break;
              }
          }
          return (f || s(e, h))(r, t, !m, o, !t || (J.test(e) && fe(t.parentNode)) || t), o;
        }),
        (n.sortStable =
          x
            .split('')
            .sort(_)
            .join('') === x),
        (n.detectDuplicates = !!d),
        p(),
        (n.sortDetached = se(function(e) {
          return 1 & e.compareDocumentPosition(f.createElement('div'));
        })),
        se(function(e) {
          return (e.innerHTML = "<a href='#'></a>"), '#' === e.firstChild.getAttribute('href');
        }) ||
          le('type|href|height|width', function(e, t, n) {
            return n ? void 0 : e.getAttribute(t, 'type' === t.toLowerCase() ? 1 : 2);
          }),
        (n.attributes &&
          se(function(e) {
            return (
              (e.innerHTML = '<input/>'),
              e.firstChild.setAttribute('value', ''),
              '' === e.firstChild.getAttribute('value')
            );
          })) ||
          le('value', function(e, t, n) {
            return n || 'input' !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue;
          }),
        se(function(e) {
          return null == e.getAttribute('disabled');
        }) ||
          le(M, function(e, t, n) {
            var i;
            return n
              ? void 0
              : !0 === e[t]
              ? t.toLowerCase()
              : (i = e.getAttributeNode(t)) && i.specified
              ? i.value
              : null;
          }),
        oe
      );
    })(e);
    (f.find = b),
      (f.expr = b.selectors),
      (f.expr[':'] = f.expr.pseudos),
      (f.uniqueSort = f.unique = b.uniqueSort),
      (f.text = b.getText),
      (f.isXMLDoc = b.isXML),
      (f.contains = b.contains);
    var x = function(e, t, n) {
        for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
          if (1 === e.nodeType) {
            if (o && f(e).is(n)) break;
            i.push(e);
          }
        return i;
      },
      w = function(e, t) {
        for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
        return n;
      },
      C = f.expr.match.needsContext,
      T = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
      S = /^.[^:#\[\.,]*$/;
    function E(e, t, n) {
      if (f.isFunction(t))
        return f.grep(e, function(e, i) {
          return !!t.call(e, i, e) !== n;
        });
      if (t.nodeType)
        return f.grep(e, function(e) {
          return (e === t) !== n;
        });
      if ('string' == typeof t) {
        if (S.test(t)) return f.filter(t, e, n);
        t = f.filter(t, e);
      }
      return f.grep(e, function(e) {
        return f.inArray(e, t) > -1 !== n;
      });
    }
    (f.filter = function(e, t, n) {
      var i = t[0];
      return (
        n && (e = ':not(' + e + ')'),
        1 === t.length && 1 === i.nodeType
          ? f.find.matchesSelector(i, e)
            ? [i]
            : []
          : f.find.matches(
              e,
              f.grep(t, function(e) {
                return 1 === e.nodeType;
              })
            )
      );
    }),
      f.fn.extend({
        find: function(e) {
          var t,
            n = [],
            i = this,
            o = i.length;
          if ('string' != typeof e)
            return this.pushStack(
              f(e).filter(function() {
                for (t = 0; o > t; t++) if (f.contains(i[t], this)) return !0;
              })
            );
          for (t = 0; o > t; t++) f.find(e, i[t], n);
          return (
            ((n = this.pushStack(o > 1 ? f.unique(n) : n)).selector = this.selector ? this.selector + ' ' + e : e), n
          );
        },
        filter: function(e) {
          return this.pushStack(E(this, e || [], !1));
        },
        not: function(e) {
          return this.pushStack(E(this, e || [], !0));
        },
        is: function(e) {
          return !!E(this, 'string' == typeof e && C.test(e) ? f(e) : e || [], !1).length;
        }
      });
    var k,
      _ = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    ((f.fn.init = function(e, t, n) {
      var o, r;
      if (!e) return this;
      if (((n = n || k), 'string' == typeof e)) {
        if (
          !(o = '<' === e.charAt(0) && '>' === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : _.exec(e)) ||
          (!o[1] && t)
        )
          return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
        if (o[1]) {
          if (
            (f.merge(
              this,
              f.parseHTML(o[1], (t = t instanceof f ? t[0] : t) && t.nodeType ? t.ownerDocument || t : i, !0)
            ),
            T.test(o[1]) && f.isPlainObject(t))
          )
            for (o in t) f.isFunction(this[o]) ? this[o](t[o]) : this.attr(o, t[o]);
          return this;
        }
        if ((r = i.getElementById(o[2])) && r.parentNode) {
          if (r.id !== o[2]) return k.find(e);
          (this.length = 1), (this[0] = r);
        }
        return (this.context = i), (this.selector = e), this;
      }
      return e.nodeType
        ? ((this.context = this[0] = e), (this.length = 1), this)
        : f.isFunction(e)
        ? void 0 !== n.ready
          ? n.ready(e)
          : e(f)
        : (void 0 !== e.selector && ((this.selector = e.selector), (this.context = e.context)), f.makeArray(e, this));
    }).prototype = f.fn),
      (k = f(i));
    var A = /^(?:parents|prev(?:Until|All))/,
      D = { children: !0, contents: !0, next: !0, prev: !0 };
    function I(e, t) {
      do {
        e = e[t];
      } while (e && 1 !== e.nodeType);
      return e;
    }
    f.fn.extend({
      has: function(e) {
        var t,
          n = f(e, this),
          i = n.length;
        return this.filter(function() {
          for (t = 0; i > t; t++) if (f.contains(this, n[t])) return !0;
        });
      },
      closest: function(e, t) {
        for (
          var n, i = 0, o = this.length, r = [], a = C.test(e) || 'string' != typeof e ? f(e, t || this.context) : 0;
          o > i;
          i++
        )
          for (n = this[i]; n && n !== t; n = n.parentNode)
            if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && f.find.matchesSelector(n, e))) {
              r.push(n);
              break;
            }
        return this.pushStack(r.length > 1 ? f.uniqueSort(r) : r);
      },
      index: function(e) {
        return e
          ? 'string' == typeof e
            ? f.inArray(this[0], f(e))
            : f.inArray(e.jquery ? e[0] : e, this)
          : this[0] && this[0].parentNode
          ? this.first().prevAll().length
          : -1;
      },
      add: function(e, t) {
        return this.pushStack(f.uniqueSort(f.merge(this.get(), f(e, t))));
      },
      addBack: function(e) {
        return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
      }
    }),
      f.each(
        {
          parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null;
          },
          parents: function(e) {
            return x(e, 'parentNode');
          },
          parentsUntil: function(e, t, n) {
            return x(e, 'parentNode', n);
          },
          next: function(e) {
            return I(e, 'nextSibling');
          },
          prev: function(e) {
            return I(e, 'previousSibling');
          },
          nextAll: function(e) {
            return x(e, 'nextSibling');
          },
          prevAll: function(e) {
            return x(e, 'previousSibling');
          },
          nextUntil: function(e, t, n) {
            return x(e, 'nextSibling', n);
          },
          prevUntil: function(e, t, n) {
            return x(e, 'previousSibling', n);
          },
          siblings: function(e) {
            return w((e.parentNode || {}).firstChild, e);
          },
          children: function(e) {
            return w(e.firstChild);
          },
          contents: function(e) {
            return f.nodeName(e, 'iframe') ? e.contentDocument || e.contentWindow.document : f.merge([], e.childNodes);
          }
        },
        function(e, t) {
          f.fn[e] = function(n, i) {
            var o = f.map(this, t, n);
            return (
              'Until' !== e.slice(-5) && (i = n),
              i && 'string' == typeof i && (o = f.filter(i, o)),
              this.length > 1 && (D[e] || (o = f.uniqueSort(o)), A.test(e) && (o = o.reverse())),
              this.pushStack(o)
            );
          };
        }
      );
    var N,
      $,
      O = /\S+/g;
    function R() {
      i.addEventListener
        ? (i.removeEventListener('DOMContentLoaded', M), e.removeEventListener('load', M))
        : (i.detachEvent('onreadystatechange', M), e.detachEvent('onload', M));
    }
    function M() {
      (i.addEventListener || 'load' === e.event.type || 'complete' === i.readyState) && (R(), f.ready());
    }
    for ($ in ((f.Callbacks = function(e) {
      e =
        'string' == typeof e
          ? (function(e) {
              var t = {};
              return (
                f.each(e.match(O) || [], function(e, n) {
                  t[n] = !0;
                }),
                t
              );
            })(e)
          : f.extend({}, e);
      var t,
        n,
        i,
        o,
        r = [],
        a = [],
        s = -1,
        l = function() {
          for (o = e.once, i = t = !0; a.length; s = -1)
            for (n = a.shift(); ++s < r.length; )
              !1 === r[s].apply(n[0], n[1]) && e.stopOnFalse && ((s = r.length), (n = !1));
          e.memory || (n = !1), (t = !1), o && (r = n ? [] : '');
        },
        c = {
          add: function() {
            return (
              r &&
                (n && !t && ((s = r.length - 1), a.push(n)),
                (function t(n) {
                  f.each(n, function(n, i) {
                    f.isFunction(i)
                      ? (e.unique && c.has(i)) || r.push(i)
                      : i && i.length && 'string' !== f.type(i) && t(i);
                  });
                })(arguments),
                n && !t && l()),
              this
            );
          },
          remove: function() {
            return (
              f.each(arguments, function(e, t) {
                for (var n; (n = f.inArray(t, r, n)) > -1; ) r.splice(n, 1), s >= n && s--;
              }),
              this
            );
          },
          has: function(e) {
            return e ? f.inArray(e, r) > -1 : r.length > 0;
          },
          empty: function() {
            return r && (r = []), this;
          },
          disable: function() {
            return (o = a = []), (r = n = ''), this;
          },
          disabled: function() {
            return !r;
          },
          lock: function() {
            return (o = !0), n || c.disable(), this;
          },
          locked: function() {
            return !!o;
          },
          fireWith: function(e, n) {
            return o || ((n = [e, (n = n || []).slice ? n.slice() : n]), a.push(n), t || l()), this;
          },
          fire: function() {
            return c.fireWith(this, arguments), this;
          },
          fired: function() {
            return !!i;
          }
        };
      return c;
    }),
    f.extend({
      Deferred: function(e) {
        var t = [
            ['resolve', 'done', f.Callbacks('once memory'), 'resolved'],
            ['reject', 'fail', f.Callbacks('once memory'), 'rejected'],
            ['notify', 'progress', f.Callbacks('memory')]
          ],
          n = 'pending',
          i = {
            state: function() {
              return n;
            },
            always: function() {
              return o.done(arguments).fail(arguments), this;
            },
            then: function() {
              var e = arguments;
              return f
                .Deferred(function(n) {
                  f.each(t, function(t, r) {
                    var a = f.isFunction(e[t]) && e[t];
                    o[r[1]](function() {
                      var e = a && a.apply(this, arguments);
                      e && f.isFunction(e.promise)
                        ? e
                            .promise()
                            .progress(n.notify)
                            .done(n.resolve)
                            .fail(n.reject)
                        : n[r[0] + 'With'](this === i ? n.promise() : this, a ? [e] : arguments);
                    });
                  }),
                    (e = null);
                })
                .promise();
            },
            promise: function(e) {
              return null != e ? f.extend(e, i) : i;
            }
          },
          o = {};
        return (
          (i.pipe = i.then),
          f.each(t, function(e, r) {
            var a = r[2],
              s = r[3];
            (i[r[1]] = a.add),
              s &&
                a.add(
                  function() {
                    n = s;
                  },
                  t[1 ^ e][2].disable,
                  t[2][2].lock
                ),
              (o[r[0]] = function() {
                return o[r[0] + 'With'](this === o ? i : this, arguments), this;
              }),
              (o[r[0] + 'With'] = a.fireWith);
          }),
          i.promise(o),
          e && e.call(o, o),
          o
        );
      },
      when: function(e) {
        var t,
          n,
          i,
          r = 0,
          a = o.call(arguments),
          s = a.length,
          l = 1 !== s || (e && f.isFunction(e.promise)) ? s : 0,
          c = 1 === l ? e : f.Deferred(),
          u = function(e, n, i) {
            return function(r) {
              (n[e] = this),
                (i[e] = arguments.length > 1 ? o.call(arguments) : r),
                i === t ? c.notifyWith(n, i) : --l || c.resolveWith(n, i);
            };
          };
        if (s > 1)
          for (t = new Array(s), n = new Array(s), i = new Array(s); s > r; r++)
            a[r] && f.isFunction(a[r].promise)
              ? a[r]
                  .promise()
                  .progress(u(r, n, t))
                  .done(u(r, i, a))
                  .fail(c.reject)
              : --l;
        return l || c.resolveWith(i, a), c.promise();
      }
    }),
    (f.fn.ready = function(e) {
      return f.ready.promise().done(e), this;
    }),
    f.extend({
      isReady: !1,
      readyWait: 1,
      holdReady: function(e) {
        e ? f.readyWait++ : f.ready(!0);
      },
      ready: function(e) {
        (!0 === e ? --f.readyWait : f.isReady) ||
          ((f.isReady = !0),
          (!0 !== e && --f.readyWait > 0) ||
            (N.resolveWith(i, [f]), f.fn.triggerHandler && (f(i).triggerHandler('ready'), f(i).off('ready'))));
      }
    }),
    (f.ready.promise = function(t) {
      if (!N)
        if (
          ((N = f.Deferred()),
          'complete' === i.readyState || ('loading' !== i.readyState && !i.documentElement.doScroll))
        )
          e.setTimeout(f.ready);
        else if (i.addEventListener) i.addEventListener('DOMContentLoaded', M), e.addEventListener('load', M);
        else {
          i.attachEvent('onreadystatechange', M), e.attachEvent('onload', M);
          var n = !1;
          try {
            n = null == e.frameElement && i.documentElement;
          } catch (o) {}
          n &&
            n.doScroll &&
            (function i() {
              if (!f.isReady) {
                try {
                  n.doScroll('left');
                } catch (t) {
                  return e.setTimeout(i, 50);
                }
                R(), f.ready();
              }
            })();
        }
      return N.promise(t);
    }),
    f.ready.promise(),
    f(d)))
      break;
    (d.ownFirst = '0' === $),
      (d.inlineBlockNeedsLayout = !1),
      f(function() {
        var e, t, n, o;
        (n = i.getElementsByTagName('body')[0]) &&
          n.style &&
          ((t = i.createElement('div')),
          ((o = i.createElement('div')).style.cssText =
            'position:absolute;border:0;width:0;height:0;top:0;left:-9999px'),
          n.appendChild(o).appendChild(t),
          void 0 !== t.style.zoom &&
            ((t.style.cssText = 'display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1'),
            (d.inlineBlockNeedsLayout = e = 3 === t.offsetWidth),
            e && (n.style.zoom = 1)),
          n.removeChild(o));
      }),
      (function() {
        var e = i.createElement('div');
        d.deleteExpando = !0;
        try {
          delete e.test;
        } catch (t) {
          d.deleteExpando = !1;
        }
        e = null;
      })();
    var L = function(e) {
        var t = f.noData[(e.nodeName + ' ').toLowerCase()],
          n = +e.nodeType || 1;
        return (1 === n || 9 === n) && (!t || (!0 !== t && e.getAttribute('classid') === t));
      },
      F = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      j = /([A-Z])/g;
    function P(e, t, n) {
      if (void 0 === n && 1 === e.nodeType) {
        var i = 'data-' + t.replace(j, '-$1').toLowerCase();
        if ('string' == typeof (n = e.getAttribute(i))) {
          try {
            n =
              'true' === n ||
              ('false' !== n && ('null' === n ? null : +n + '' === n ? +n : F.test(n) ? f.parseJSON(n) : n));
          } catch (o) {}
          f.data(e, t, n);
        } else n = void 0;
      }
      return n;
    }
    function q(e) {
      var t;
      for (t in e) if (('data' !== t || !f.isEmptyObject(e[t])) && 'toJSON' !== t) return !1;
      return !0;
    }
    function H(e, t, i, o) {
      if (L(e)) {
        var r,
          a,
          s = f.expando,
          l = e.nodeType,
          c = l ? f.cache : e,
          u = l ? e[s] : e[s] && s;
        if ((u && c[u] && (o || c[u].data)) || void 0 !== i || 'string' != typeof t)
          return (
            u || (u = l ? (e[s] = n.pop() || f.guid++) : s),
            c[u] || (c[u] = l ? {} : { toJSON: f.noop }),
            ('object' != typeof t && 'function' != typeof t) ||
              (o ? (c[u] = f.extend(c[u], t)) : (c[u].data = f.extend(c[u].data, t))),
            (a = c[u]),
            o || (a.data || (a.data = {}), (a = a.data)),
            void 0 !== i && (a[f.camelCase(t)] = i),
            'string' == typeof t ? null == (r = a[t]) && (r = a[f.camelCase(t)]) : (r = a),
            r
          );
      }
    }
    function z(e, t, n) {
      if (L(e)) {
        var i,
          o,
          r = e.nodeType,
          a = r ? f.cache : e,
          s = r ? e[f.expando] : f.expando;
        if (a[s]) {
          if (t && (i = n ? a[s] : a[s].data)) {
            o = (t = f.isArray(t)
              ? t.concat(f.map(t, f.camelCase))
              : t in i || (t = f.camelCase(t)) in i
              ? [t]
              : t.split(' ')).length;
            for (; o--; ) delete i[t[o]];
            if (n ? !q(i) : !f.isEmptyObject(i)) return;
          }
          (n || (delete a[s].data, q(a[s]))) &&
            (r ? f.cleanData([e], !0) : d.deleteExpando || a != a.window ? delete a[s] : (a[s] = void 0));
        }
      }
    }
    f.extend({
      cache: {},
      noData: { 'applet ': !0, 'embed ': !0, 'object ': 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' },
      hasData: function(e) {
        return !!(e = e.nodeType ? f.cache[e[f.expando]] : e[f.expando]) && !q(e);
      },
      data: function(e, t, n) {
        return H(e, t, n);
      },
      removeData: function(e, t) {
        return z(e, t);
      },
      _data: function(e, t, n) {
        return H(e, t, n, !0);
      },
      _removeData: function(e, t) {
        return z(e, t, !0);
      }
    }),
      f.fn.extend({
        data: function(e, t) {
          var n,
            i,
            o,
            r = this[0],
            a = r && r.attributes;
          if (void 0 === e) {
            if (this.length && ((o = f.data(r)), 1 === r.nodeType && !f._data(r, 'parsedAttrs'))) {
              for (n = a.length; n--; )
                a[n] && 0 === (i = a[n].name).indexOf('data-') && P(r, (i = f.camelCase(i.slice(5))), o[i]);
              f._data(r, 'parsedAttrs', !0);
            }
            return o;
          }
          return 'object' == typeof e
            ? this.each(function() {
                f.data(this, e);
              })
            : arguments.length > 1
            ? this.each(function() {
                f.data(this, e, t);
              })
            : r
            ? P(r, e, f.data(r, e))
            : void 0;
        },
        removeData: function(e) {
          return this.each(function() {
            f.removeData(this, e);
          });
        }
      }),
      f.extend({
        queue: function(e, t, n) {
          var i;
          return e
            ? ((i = f._data(e, (t = (t || 'fx') + 'queue'))),
              n && (!i || f.isArray(n) ? (i = f._data(e, t, f.makeArray(n))) : i.push(n)),
              i || [])
            : void 0;
        },
        dequeue: function(e, t) {
          var n = f.queue(e, (t = t || 'fx')),
            i = n.length,
            o = n.shift(),
            r = f._queueHooks(e, t);
          'inprogress' === o && ((o = n.shift()), i--),
            o &&
              ('fx' === t && n.unshift('inprogress'),
              delete r.stop,
              o.call(
                e,
                function() {
                  f.dequeue(e, t);
                },
                r
              )),
            !i && r && r.empty.fire();
        },
        _queueHooks: function(e, t) {
          var n = t + 'queueHooks';
          return (
            f._data(e, n) ||
            f._data(e, n, {
              empty: f.Callbacks('once memory').add(function() {
                f._removeData(e, t + 'queue'), f._removeData(e, n);
              })
            })
          );
        }
      }),
      f.fn.extend({
        queue: function(e, t) {
          var n = 2;
          return (
            'string' != typeof e && ((t = e), (e = 'fx'), n--),
            arguments.length < n
              ? f.queue(this[0], e)
              : void 0 === t
              ? this
              : this.each(function() {
                  var n = f.queue(this, e, t);
                  f._queueHooks(this, e), 'fx' === e && 'inprogress' !== n[0] && f.dequeue(this, e);
                })
          );
        },
        dequeue: function(e) {
          return this.each(function() {
            f.dequeue(this, e);
          });
        },
        clearQueue: function(e) {
          return this.queue(e || 'fx', []);
        },
        promise: function(e, t) {
          var n,
            i = 1,
            o = f.Deferred(),
            r = this,
            a = this.length,
            s = function() {
              --i || o.resolveWith(r, [r]);
            };
          for ('string' != typeof e && ((t = e), (e = void 0)), e = e || 'fx'; a--; )
            (n = f._data(r[a], e + 'queueHooks')) && n.empty && (i++, n.empty.add(s));
          return s(), o.promise(t);
        }
      }),
      (function() {
        var e;
        d.shrinkWrapBlocks = function() {
          return null != e
            ? e
            : ((e = !1),
              (n = i.getElementsByTagName('body')[0]) && n.style
                ? ((t = i.createElement('div')),
                  ((o = i.createElement('div')).style.cssText =
                    'position:absolute;border:0;width:0;height:0;top:0;left:-9999px'),
                  n.appendChild(o).appendChild(t),
                  void 0 !== t.style.zoom &&
                    ((t.style.cssText =
                      '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1'),
                    (t.appendChild(i.createElement('div')).style.width = '5px'),
                    (e = 3 !== t.offsetWidth)),
                  n.removeChild(o),
                  e)
                : void 0);
          var t, n, o;
        };
      })();
    var B = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      W = new RegExp('^(?:([+-])=|)(' + B + ')([a-z%]*)$', 'i'),
      U = ['Top', 'Right', 'Bottom', 'Left'],
      Z = function(e, t) {
        return 'none' === f.css((e = t || e), 'display') || !f.contains(e.ownerDocument, e);
      };
    function V(e, t, n, i) {
      var o,
        r = 1,
        a = 20,
        s = i
          ? function() {
              return i.cur();
            }
          : function() {
              return f.css(e, t, '');
            },
        l = s(),
        c = (n && n[3]) || (f.cssNumber[t] ? '' : 'px'),
        u = (f.cssNumber[t] || ('px' !== c && +l)) && W.exec(f.css(e, t));
      if (u && u[3] !== c) {
        (c = c || u[3]), (n = n || []), (u = +l || 1);
        do {
          f.style(e, t, (u /= r = r || '.5') + c);
        } while (r !== (r = s() / l) && 1 !== r && --a);
      }
      return (
        n &&
          ((u = +u || +l || 0),
          (o = n[1] ? u + (n[1] + 1) * n[2] : +n[2]),
          i && ((i.unit = c), (i.start = u), (i.end = o))),
        o
      );
    }
    var X = function(e, t, n, i, o, r, a) {
        var s = 0,
          l = e.length,
          c = null == n;
        if ('object' === f.type(n)) for (s in ((o = !0), n)) X(e, t, s, n[s], !0, r, a);
        else if (
          void 0 !== i &&
          ((o = !0),
          f.isFunction(i) || (a = !0),
          c &&
            (a
              ? (t.call(e, i), (t = null))
              : ((c = t),
                (t = function(e, t, n) {
                  return c.call(f(e), n);
                }))),
          t)
        )
          for (; l > s; s++) t(e[s], n, a ? i : i.call(e[s], s, t(e[s], n)));
        return o ? e : c ? t.call(e) : l ? t(e[0], n) : r;
      },
      K = /^(?:checkbox|radio)$/i,
      Y = /<([\w:-]+)/,
      G = /^$|\/(?:java|ecma)script/i,
      J = /^\s+/,
      Q =
        'abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video';
    function ee(e) {
      var t = Q.split('|'),
        n = e.createDocumentFragment();
      if (n.createElement) for (; t.length; ) n.createElement(t.pop());
      return n;
    }
    !(function() {
      var e = i.createElement('div'),
        t = i.createDocumentFragment(),
        n = i.createElement('input');
      (e.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
        (d.leadingWhitespace = 3 === e.firstChild.nodeType),
        (d.tbody = !e.getElementsByTagName('tbody').length),
        (d.htmlSerialize = !!e.getElementsByTagName('link').length),
        (d.html5Clone = '<:nav></:nav>' !== i.createElement('nav').cloneNode(!0).outerHTML),
        (n.type = 'checkbox'),
        (n.checked = !0),
        t.appendChild(n),
        (d.appendChecked = n.checked),
        (e.innerHTML = '<textarea>x</textarea>'),
        (d.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue),
        t.appendChild(e),
        (n = i.createElement('input')).setAttribute('type', 'radio'),
        n.setAttribute('checked', 'checked'),
        n.setAttribute('name', 't'),
        e.appendChild(n),
        (d.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (d.noCloneEvent = !!e.addEventListener),
        (e[f.expando] = 1),
        (d.attributes = !e.getAttribute(f.expando));
    })();
    var te = {
      option: [1, "<select multiple='multiple'>", '</select>'],
      legend: [1, '<fieldset>', '</fieldset>'],
      area: [1, '<map>', '</map>'],
      param: [1, '<object>', '</object>'],
      thead: [1, '<table>', '</table>'],
      tr: [2, '<table><tbody>', '</tbody></table>'],
      col: [2, '<table><tbody></tbody><colgroup>', '</colgroup></table>'],
      td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
      _default: d.htmlSerialize ? [0, '', ''] : [1, 'X<div>', '</div>']
    };
    function ne(e, t) {
      var n,
        i,
        o = 0,
        r =
          void 0 !== e.getElementsByTagName
            ? e.getElementsByTagName(t || '*')
            : void 0 !== e.querySelectorAll
            ? e.querySelectorAll(t || '*')
            : void 0;
      if (!r)
        for (r = [], n = e.childNodes || e; null != (i = n[o]); o++)
          !t || f.nodeName(i, t) ? r.push(i) : f.merge(r, ne(i, t));
      return void 0 === t || (t && f.nodeName(e, t)) ? f.merge([e], r) : r;
    }
    function ie(e, t) {
      for (var n, i = 0; null != (n = e[i]); i++) f._data(n, 'globalEval', !t || f._data(t[i], 'globalEval'));
    }
    (te.optgroup = te.option), (te.tbody = te.tfoot = te.colgroup = te.caption = te.thead), (te.th = te.td);
    var oe = /<|&#?\w+;/,
      re = /<tbody/i;
    function ae(e) {
      K.test(e.type) && (e.defaultChecked = e.checked);
    }
    function se(e, t, n, i, o) {
      for (var r, a, s, l, c, u, p, h = e.length, m = ee(t), v = [], g = 0; h > g; g++)
        if ((a = e[g]) || 0 === a)
          if ('object' === f.type(a)) f.merge(v, a.nodeType ? [a] : a);
          else if (oe.test(a)) {
            for (
              l = l || m.appendChild(t.createElement('div')),
                c = (Y.exec(a) || ['', ''])[1].toLowerCase(),
                l.innerHTML = (p = te[c] || te._default)[1] + f.htmlPrefilter(a) + p[2],
                r = p[0];
              r--;

            )
              l = l.lastChild;
            if ((!d.leadingWhitespace && J.test(a) && v.push(t.createTextNode(J.exec(a)[0])), !d.tbody))
              for (
                r =
                  (a = 'table' !== c || re.test(a) ? ('<table>' !== p[1] || re.test(a) ? 0 : l) : l.firstChild) &&
                  a.childNodes.length;
                r--;

              )
                f.nodeName((u = a.childNodes[r]), 'tbody') && !u.childNodes.length && a.removeChild(u);
            for (f.merge(v, l.childNodes), l.textContent = ''; l.firstChild; ) l.removeChild(l.firstChild);
            l = m.lastChild;
          } else v.push(t.createTextNode(a));
      for (l && m.removeChild(l), d.appendChecked || f.grep(ne(v, 'input'), ae), g = 0; (a = v[g++]); )
        if (i && f.inArray(a, i) > -1) o && o.push(a);
        else if (((s = f.contains(a.ownerDocument, a)), (l = ne(m.appendChild(a), 'script')), s && ie(l), n))
          for (r = 0; (a = l[r++]); ) G.test(a.type || '') && n.push(a);
      return (l = null), m;
    }
    !(function() {
      var t,
        n,
        o = i.createElement('div');
      for (t in { submit: !0, change: !0, focusin: !0 })
        (d[t] = (n = 'on' + t) in e) || (o.setAttribute(n, 't'), (d[t] = !1 === o.attributes[n].expando));
      o = null;
    })();
    var le = /^(?:input|select|textarea)$/i,
      ce = /^key/,
      ue = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      de = /^(?:focusinfocus|focusoutblur)$/,
      pe = /^([^.]*)(?:\.(.+)|)/;
    function fe() {
      return !0;
    }
    function he() {
      return !1;
    }
    function me() {
      try {
        return i.activeElement;
      } catch (e) {}
    }
    function ve(e, t, n, i, o, r) {
      var a, s;
      if ('object' == typeof t) {
        for (s in ('string' != typeof n && ((i = i || n), (n = void 0)), t)) ve(e, s, n, i, t[s], r);
        return e;
      }
      if (
        (null == i && null == o
          ? ((o = n), (i = n = void 0))
          : null == o && ('string' == typeof n ? ((o = i), (i = void 0)) : ((o = i), (i = n), (n = void 0))),
        !1 === o)
      )
        o = he;
      else if (!o) return e;
      return (
        1 === r &&
          ((a = o),
          ((o = function(e) {
            return f().off(e), a.apply(this, arguments);
          }).guid = a.guid || (a.guid = f.guid++))),
        e.each(function() {
          f.event.add(this, t, o, i, n);
        })
      );
    }
    (f.event = {
      global: {},
      add: function(e, t, n, i, o) {
        var r,
          a,
          s,
          l,
          c,
          u,
          d,
          p,
          h,
          m,
          v,
          g = f._data(e);
        if (g) {
          for (
            n.handler && ((n = (l = n).handler), (o = l.selector)),
              n.guid || (n.guid = f.guid++),
              (a = g.events) || (a = g.events = {}),
              (u = g.handle) ||
                ((u = g.handle = function(e) {
                  return void 0 === f || (e && f.event.triggered === e.type)
                    ? void 0
                    : f.event.dispatch.apply(u.elem, arguments);
                }).elem = e),
              s = (t = (t || '').match(O) || ['']).length;
            s--;

          )
            (h = v = (r = pe.exec(t[s]) || [])[1]),
              (m = (r[2] || '').split('.').sort()),
              h &&
                ((c = f.event.special[h] || {}),
                (c = f.event.special[(h = (o ? c.delegateType : c.bindType) || h)] || {}),
                (d = f.extend(
                  {
                    type: h,
                    origType: v,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: o,
                    needsContext: o && f.expr.match.needsContext.test(o),
                    namespace: m.join('.')
                  },
                  l
                )),
                (p = a[h]) ||
                  (((p = a[h] = []).delegateCount = 0),
                  (c.setup && !1 !== c.setup.call(e, i, m, u)) ||
                    (e.addEventListener ? e.addEventListener(h, u, !1) : e.attachEvent && e.attachEvent('on' + h, u))),
                c.add && (c.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)),
                o ? p.splice(p.delegateCount++, 0, d) : p.push(d),
                (f.event.global[h] = !0));
          e = null;
        }
      },
      remove: function(e, t, n, i, o) {
        var r,
          a,
          s,
          l,
          c,
          u,
          d,
          p,
          h,
          m,
          v,
          g = f.hasData(e) && f._data(e);
        if (g && (u = g.events)) {
          for (c = (t = (t || '').match(O) || ['']).length; c--; )
            if (((h = v = (s = pe.exec(t[c]) || [])[1]), (m = (s[2] || '').split('.').sort()), h)) {
              for (
                d = f.event.special[h] || {},
                  p = u[(h = (i ? d.delegateType : d.bindType) || h)] || [],
                  s = s[2] && new RegExp('(^|\\.)' + m.join('\\.(?:.*\\.|)') + '(\\.|$)'),
                  l = r = p.length;
                r--;

              )
                (a = p[r]),
                  (!o && v !== a.origType) ||
                    (n && n.guid !== a.guid) ||
                    (s && !s.test(a.namespace)) ||
                    (i && i !== a.selector && ('**' !== i || !a.selector)) ||
                    (p.splice(r, 1), a.selector && p.delegateCount--, d.remove && d.remove.call(e, a));
              l &&
                !p.length &&
                ((d.teardown && !1 !== d.teardown.call(e, m, g.handle)) || f.removeEvent(e, h, g.handle), delete u[h]);
            } else for (h in u) f.event.remove(e, h + t[c], n, i, !0);
          f.isEmptyObject(u) && (delete g.handle, f._removeData(e, 'events'));
        }
      },
      trigger: function(t, n, o, r) {
        var a,
          s,
          l,
          c,
          d,
          p,
          h,
          m = [o || i],
          v = u.call(t, 'type') ? t.type : t,
          g = u.call(t, 'namespace') ? t.namespace.split('.') : [];
        if (
          ((l = p = o = o || i),
          3 !== o.nodeType &&
            8 !== o.nodeType &&
            !de.test(v + f.event.triggered) &&
            (v.indexOf('.') > -1 && ((g = v.split('.')), (v = g.shift()), g.sort()),
            (s = v.indexOf(':') < 0 && 'on' + v),
            ((t = t[f.expando] ? t : new f.Event(v, 'object' == typeof t && t)).isTrigger = r ? 2 : 3),
            (t.namespace = g.join('.')),
            (t.rnamespace = t.namespace ? new RegExp('(^|\\.)' + g.join('\\.(?:.*\\.|)') + '(\\.|$)') : null),
            (t.result = void 0),
            t.target || (t.target = o),
            (n = null == n ? [t] : f.makeArray(n, [t])),
            (d = f.event.special[v] || {}),
            r || !d.trigger || !1 !== d.trigger.apply(o, n)))
        ) {
          if (!r && !d.noBubble && !f.isWindow(o)) {
            for (de.test((c = d.delegateType || v) + v) || (l = l.parentNode); l; l = l.parentNode) m.push(l), (p = l);
            p === (o.ownerDocument || i) && m.push(p.defaultView || p.parentWindow || e);
          }
          for (h = 0; (l = m[h++]) && !t.isPropagationStopped(); )
            (t.type = h > 1 ? c : d.bindType || v),
              (a = (f._data(l, 'events') || {})[t.type] && f._data(l, 'handle')) && a.apply(l, n),
              (a = s && l[s]) && a.apply && L(l) && ((t.result = a.apply(l, n)), !1 === t.result && t.preventDefault());
          if (
            ((t.type = v),
            !r &&
              !t.isDefaultPrevented() &&
              (!d._default || !1 === d._default.apply(m.pop(), n)) &&
              L(o) &&
              s &&
              o[v] &&
              !f.isWindow(o))
          ) {
            (p = o[s]) && (o[s] = null), (f.event.triggered = v);
            try {
              o[v]();
            } catch (y) {}
            (f.event.triggered = void 0), p && (o[s] = p);
          }
          return t.result;
        }
      },
      dispatch: function(e) {
        e = f.event.fix(e);
        var t,
          n,
          i,
          r,
          a,
          s = [],
          l = o.call(arguments),
          c = (f._data(this, 'events') || {})[e.type] || [],
          u = f.event.special[e.type] || {};
        if (((l[0] = e), (e.delegateTarget = this), !u.preDispatch || !1 !== u.preDispatch.call(this, e))) {
          for (s = f.event.handlers.call(this, e, c), t = 0; (r = s[t++]) && !e.isPropagationStopped(); )
            for (e.currentTarget = r.elem, n = 0; (a = r.handlers[n++]) && !e.isImmediatePropagationStopped(); )
              (e.rnamespace && !e.rnamespace.test(a.namespace)) ||
                ((e.handleObj = a),
                (e.data = a.data),
                void 0 !== (i = ((f.event.special[a.origType] || {}).handle || a.handler).apply(r.elem, l)) &&
                  !1 === (e.result = i) &&
                  (e.preventDefault(), e.stopPropagation()));
          return u.postDispatch && u.postDispatch.call(this, e), e.result;
        }
      },
      handlers: function(e, t) {
        var n,
          i,
          o,
          r,
          a = [],
          s = t.delegateCount,
          l = e.target;
        if (s && l.nodeType && ('click' !== e.type || isNaN(e.button) || e.button < 1))
          for (; l != this; l = l.parentNode || this)
            if (1 === l.nodeType && (!0 !== l.disabled || 'click' !== e.type)) {
              for (i = [], n = 0; s > n; n++)
                void 0 === i[(o = (r = t[n]).selector + ' ')] &&
                  (i[o] = r.needsContext ? f(o, this).index(l) > -1 : f.find(o, this, null, [l]).length),
                  i[o] && i.push(r);
              i.length && a.push({ elem: l, handlers: i });
            }
        return s < t.length && a.push({ elem: this, handlers: t.slice(s) }), a;
      },
      fix: function(e) {
        if (e[f.expando]) return e;
        var t,
          n,
          o,
          r = e.type,
          a = e,
          s = this.fixHooks[r];
        for (
          s || (this.fixHooks[r] = s = ue.test(r) ? this.mouseHooks : ce.test(r) ? this.keyHooks : {}),
            o = s.props ? this.props.concat(s.props) : this.props,
            e = new f.Event(a),
            t = o.length;
          t--;

        )
          e[(n = o[t])] = a[n];
        return (
          e.target || (e.target = a.srcElement || i),
          3 === e.target.nodeType && (e.target = e.target.parentNode),
          (e.metaKey = !!e.metaKey),
          s.filter ? s.filter(e, a) : e
        );
      },
      props: 'altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which'.split(
        ' '
      ),
      fixHooks: {},
      keyHooks: {
        props: 'char charCode key keyCode'.split(' '),
        filter: function(e, t) {
          return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e;
        }
      },
      mouseHooks: {
        props: 'button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement'.split(
          ' '
        ),
        filter: function(e, t) {
          var n,
            o,
            r,
            a = t.button,
            s = t.fromElement;
          return (
            null == e.pageX &&
              null != t.clientX &&
              ((n = (o = e.target.ownerDocument || i).body),
              (e.pageX =
                t.clientX +
                (((r = o.documentElement) && r.scrollLeft) || (n && n.scrollLeft) || 0) -
                ((r && r.clientLeft) || (n && n.clientLeft) || 0)),
              (e.pageY =
                t.clientY +
                ((r && r.scrollTop) || (n && n.scrollTop) || 0) -
                ((r && r.clientTop) || (n && n.clientTop) || 0))),
            !e.relatedTarget && s && (e.relatedTarget = s === e.target ? t.toElement : s),
            e.which || void 0 === a || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0),
            e
          );
        }
      },
      special: {
        load: { noBubble: !0 },
        focus: {
          trigger: function() {
            if (this !== me() && this.focus)
              try {
                return this.focus(), !1;
              } catch (e) {}
          },
          delegateType: 'focusin'
        },
        blur: {
          trigger: function() {
            return this === me() && this.blur ? (this.blur(), !1) : void 0;
          },
          delegateType: 'focusout'
        },
        click: {
          trigger: function() {
            return f.nodeName(this, 'input') && 'checkbox' === this.type && this.click ? (this.click(), !1) : void 0;
          },
          _default: function(e) {
            return f.nodeName(e.target, 'a');
          }
        },
        beforeunload: {
          postDispatch: function(e) {
            void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
          }
        }
      },
      simulate: function(e, t, n) {
        var i = f.extend(new f.Event(), n, { type: e, isSimulated: !0 });
        f.event.trigger(i, null, t), i.isDefaultPrevented() && n.preventDefault();
      }
    }),
      (f.removeEvent = i.removeEventListener
        ? function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n);
          }
        : function(e, t, n) {
            var i = 'on' + t;
            e.detachEvent && (void 0 === e[i] && (e[i] = null), e.detachEvent(i, n));
          }),
      (f.Event = function(e, t) {
        return this instanceof f.Event
          ? (e && e.type
              ? ((this.originalEvent = e),
                (this.type = e.type),
                (this.isDefaultPrevented =
                  e.defaultPrevented || (void 0 === e.defaultPrevented && !1 === e.returnValue) ? fe : he))
              : (this.type = e),
            t && f.extend(this, t),
            (this.timeStamp = (e && e.timeStamp) || f.now()),
            void (this[f.expando] = !0))
          : new f.Event(e, t);
      }),
      (f.Event.prototype = {
        constructor: f.Event,
        isDefaultPrevented: he,
        isPropagationStopped: he,
        isImmediatePropagationStopped: he,
        preventDefault: function() {
          var e = this.originalEvent;
          (this.isDefaultPrevented = fe), e && (e.preventDefault ? e.preventDefault() : (e.returnValue = !1));
        },
        stopPropagation: function() {
          var e = this.originalEvent;
          (this.isPropagationStopped = fe),
            e && !this.isSimulated && (e.stopPropagation && e.stopPropagation(), (e.cancelBubble = !0));
        },
        stopImmediatePropagation: function() {
          var e = this.originalEvent;
          (this.isImmediatePropagationStopped = fe),
            e && e.stopImmediatePropagation && e.stopImmediatePropagation(),
            this.stopPropagation();
        }
      }),
      f.each(
        { mouseenter: 'mouseover', mouseleave: 'mouseout', pointerenter: 'pointerover', pointerleave: 'pointerout' },
        function(e, t) {
          f.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
              var n,
                i = this,
                o = e.relatedTarget,
                r = e.handleObj;
              return (
                (o && (o === i || f.contains(i, o))) ||
                  ((e.type = r.origType), (n = r.handler.apply(this, arguments)), (e.type = t)),
                n
              );
            }
          };
        }
      ),
      d.submit ||
        (f.event.special.submit = {
          setup: function() {
            return (
              !f.nodeName(this, 'form') &&
              void f.event.add(this, 'click._submit keypress._submit', function(e) {
                var t = e.target,
                  n = f.nodeName(t, 'input') || f.nodeName(t, 'button') ? f.prop(t, 'form') : void 0;
                n &&
                  !f._data(n, 'submit') &&
                  (f.event.add(n, 'submit._submit', function(e) {
                    e._submitBubble = !0;
                  }),
                  f._data(n, 'submit', !0));
              })
            );
          },
          postDispatch: function(e) {
            e._submitBubble &&
              (delete e._submitBubble,
              this.parentNode && !e.isTrigger && f.event.simulate('submit', this.parentNode, e));
          },
          teardown: function() {
            return !f.nodeName(this, 'form') && void f.event.remove(this, '._submit');
          }
        }),
      d.change ||
        (f.event.special.change = {
          setup: function() {
            return le.test(this.nodeName)
              ? (('checkbox' !== this.type && 'radio' !== this.type) ||
                  (f.event.add(this, 'propertychange._change', function(e) {
                    'checked' === e.originalEvent.propertyName && (this._justChanged = !0);
                  }),
                  f.event.add(this, 'click._change', function(e) {
                    this._justChanged && !e.isTrigger && (this._justChanged = !1), f.event.simulate('change', this, e);
                  })),
                !1)
              : void f.event.add(this, 'beforeactivate._change', function(e) {
                  var t = e.target;
                  le.test(t.nodeName) &&
                    !f._data(t, 'change') &&
                    (f.event.add(t, 'change._change', function(e) {
                      !this.parentNode ||
                        e.isSimulated ||
                        e.isTrigger ||
                        f.event.simulate('change', this.parentNode, e);
                    }),
                    f._data(t, 'change', !0));
                });
          },
          handle: function(e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || ('radio' !== t.type && 'checkbox' !== t.type)
              ? e.handleObj.handler.apply(this, arguments)
              : void 0;
          },
          teardown: function() {
            return f.event.remove(this, '._change'), !le.test(this.nodeName);
          }
        }),
      d.focusin ||
        f.each({ focus: 'focusin', blur: 'focusout' }, function(e, t) {
          var n = function(e) {
            f.event.simulate(t, e.target, f.event.fix(e));
          };
          f.event.special[t] = {
            setup: function() {
              var i = this.ownerDocument || this,
                o = f._data(i, t);
              o || i.addEventListener(e, n, !0), f._data(i, t, (o || 0) + 1);
            },
            teardown: function() {
              var i = this.ownerDocument || this,
                o = f._data(i, t) - 1;
              o ? f._data(i, t, o) : (i.removeEventListener(e, n, !0), f._removeData(i, t));
            }
          };
        }),
      f.fn.extend({
        on: function(e, t, n, i) {
          return ve(this, e, t, n, i);
        },
        one: function(e, t, n, i) {
          return ve(this, e, t, n, i, 1);
        },
        off: function(e, t, n) {
          var i, o;
          if (e && e.preventDefault && e.handleObj)
            return (
              (i = e.handleObj),
              f(e.delegateTarget).off(i.namespace ? i.origType + '.' + i.namespace : i.origType, i.selector, i.handler),
              this
            );
          if ('object' == typeof e) {
            for (o in e) this.off(o, t, e[o]);
            return this;
          }
          return (
            (!1 !== t && 'function' != typeof t) || ((n = t), (t = void 0)),
            !1 === n && (n = he),
            this.each(function() {
              f.event.remove(this, e, n, t);
            })
          );
        },
        trigger: function(e, t) {
          return this.each(function() {
            f.event.trigger(e, t, this);
          });
        },
        triggerHandler: function(e, t) {
          var n = this[0];
          return n ? f.event.trigger(e, t, n, !0) : void 0;
        }
      });
    var ge = / jQuery\d+="(?:null|\d+)"/g,
      ye = new RegExp('<(?:' + Q + ')[\\s/>]', 'i'),
      be = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
      xe = /<script|<style|<link/i,
      we = /checked\s*(?:[^=]|=\s*.checked.)/i,
      Ce = /^true\/(.*)/,
      Te = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
      Se = ee(i).appendChild(i.createElement('div'));
    function Ee(e, t) {
      return f.nodeName(e, 'table') && f.nodeName(11 !== t.nodeType ? t : t.firstChild, 'tr')
        ? e.getElementsByTagName('tbody')[0] || e.appendChild(e.ownerDocument.createElement('tbody'))
        : e;
    }
    function ke(e) {
      return (e.type = (null !== f.find.attr(e, 'type')) + '/' + e.type), e;
    }
    function _e(e) {
      var t = Ce.exec(e.type);
      return t ? (e.type = t[1]) : e.removeAttribute('type'), e;
    }
    function Ae(e, t) {
      if (1 === t.nodeType && f.hasData(e)) {
        var n,
          i,
          o,
          r = f._data(e),
          a = f._data(t, r),
          s = r.events;
        if (s)
          for (n in (delete a.handle, (a.events = {}), s))
            for (i = 0, o = s[n].length; o > i; i++) f.event.add(t, n, s[n][i]);
        a.data && (a.data = f.extend({}, a.data));
      }
    }
    function De(e, t) {
      var n, i, o;
      if (1 === t.nodeType) {
        if (((n = t.nodeName.toLowerCase()), !d.noCloneEvent && t[f.expando])) {
          for (i in (o = f._data(t)).events) f.removeEvent(t, i, o.handle);
          t.removeAttribute(f.expando);
        }
        'script' === n && t.text !== e.text
          ? ((ke(t).text = e.text), _e(t))
          : 'object' === n
          ? (t.parentNode && (t.outerHTML = e.outerHTML),
            d.html5Clone && e.innerHTML && !f.trim(t.innerHTML) && (t.innerHTML = e.innerHTML))
          : 'input' === n && K.test(e.type)
          ? ((t.defaultChecked = t.checked = e.checked), t.value !== e.value && (t.value = e.value))
          : 'option' === n
          ? (t.defaultSelected = t.selected = e.defaultSelected)
          : ('input' !== n && 'textarea' !== n) || (t.defaultValue = e.defaultValue);
      }
    }
    function Ie(e, t, n, i) {
      t = r.apply([], t);
      var o,
        a,
        s,
        l,
        c,
        u,
        p = 0,
        h = e.length,
        m = h - 1,
        v = t[0],
        g = f.isFunction(v);
      if (g || (h > 1 && 'string' == typeof v && !d.checkClone && we.test(v)))
        return e.each(function(o) {
          var r = e.eq(o);
          g && (t[0] = v.call(this, o, r.html())), Ie(r, t, n, i);
        });
      if (
        h &&
        ((o = (u = se(t, e[0].ownerDocument, !1, e, i)).firstChild), 1 === u.childNodes.length && (u = o), o || i)
      ) {
        for (s = (l = f.map(ne(u, 'script'), ke)).length; h > p; p++)
          (a = u), p !== m && ((a = f.clone(a, !0, !0)), s && f.merge(l, ne(a, 'script'))), n.call(e[p], a, p);
        if (s)
          for (c = l[l.length - 1].ownerDocument, f.map(l, _e), p = 0; s > p; p++)
            G.test((a = l[p]).type || '') &&
              !f._data(a, 'globalEval') &&
              f.contains(c, a) &&
              (a.src
                ? f._evalUrl && f._evalUrl(a.src)
                : f.globalEval((a.text || a.textContent || a.innerHTML || '').replace(Te, '')));
        u = o = null;
      }
      return e;
    }
    function Ne(e, t, n) {
      for (var i, o = t ? f.filter(t, e) : e, r = 0; null != (i = o[r]); r++)
        n || 1 !== i.nodeType || f.cleanData(ne(i)),
          i.parentNode && (n && f.contains(i.ownerDocument, i) && ie(ne(i, 'script')), i.parentNode.removeChild(i));
      return e;
    }
    f.extend({
      htmlPrefilter: function(e) {
        return e.replace(be, '<$1></$2>');
      },
      clone: function(e, t, n) {
        var i,
          o,
          r,
          a,
          s,
          l = f.contains(e.ownerDocument, e);
        if (
          (d.html5Clone || f.isXMLDoc(e) || !ye.test('<' + e.nodeName + '>')
            ? (r = e.cloneNode(!0))
            : ((Se.innerHTML = e.outerHTML), Se.removeChild((r = Se.firstChild))),
          !((d.noCloneEvent && d.noCloneChecked) || (1 !== e.nodeType && 11 !== e.nodeType) || f.isXMLDoc(e)))
        )
          for (i = ne(r), s = ne(e), a = 0; null != (o = s[a]); ++a) i[a] && De(o, i[a]);
        if (t)
          if (n) for (s = s || ne(e), i = i || ne(r), a = 0; null != (o = s[a]); a++) Ae(o, i[a]);
          else Ae(e, r);
        return (i = ne(r, 'script')).length > 0 && ie(i, !l && ne(e, 'script')), (i = s = o = null), r;
      },
      cleanData: function(e, t) {
        for (
          var i, o, r, a, s = 0, l = f.expando, c = f.cache, u = d.attributes, p = f.event.special;
          null != (i = e[s]);
          s++
        )
          if ((t || L(i)) && (a = (r = i[l]) && c[r])) {
            if (a.events) for (o in a.events) p[o] ? f.event.remove(i, o) : f.removeEvent(i, o, a.handle);
            c[r] &&
              (delete c[r], u || void 0 === i.removeAttribute ? (i[l] = void 0) : i.removeAttribute(l), n.push(r));
          }
      }
    }),
      f.fn.extend({
        domManip: Ie,
        detach: function(e) {
          return Ne(this, e, !0);
        },
        remove: function(e) {
          return Ne(this, e);
        },
        text: function(e) {
          return X(
            this,
            function(e) {
              return void 0 === e
                ? f.text(this)
                : this.empty().append(((this[0] && this[0].ownerDocument) || i).createTextNode(e));
            },
            null,
            e,
            arguments.length
          );
        },
        append: function() {
          return Ie(this, arguments, function(e) {
            (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || Ee(this, e).appendChild(e);
          });
        },
        prepend: function() {
          return Ie(this, arguments, function(e) {
            if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
              var t = Ee(this, e);
              t.insertBefore(e, t.firstChild);
            }
          });
        },
        before: function() {
          return Ie(this, arguments, function(e) {
            this.parentNode && this.parentNode.insertBefore(e, this);
          });
        },
        after: function() {
          return Ie(this, arguments, function(e) {
            this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
          });
        },
        empty: function() {
          for (var e, t = 0; null != (e = this[t]); t++) {
            for (1 === e.nodeType && f.cleanData(ne(e, !1)); e.firstChild; ) e.removeChild(e.firstChild);
            e.options && f.nodeName(e, 'select') && (e.options.length = 0);
          }
          return this;
        },
        clone: function(e, t) {
          return (
            (e = null != e && e),
            (t = null == t ? e : t),
            this.map(function() {
              return f.clone(this, e, t);
            })
          );
        },
        html: function(e) {
          return X(
            this,
            function(e) {
              var t = this[0] || {},
                n = 0,
                i = this.length;
              if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(ge, '') : void 0;
              if (
                'string' == typeof e &&
                !xe.test(e) &&
                (d.htmlSerialize || !ye.test(e)) &&
                (d.leadingWhitespace || !J.test(e)) &&
                !te[(Y.exec(e) || ['', ''])[1].toLowerCase()]
              ) {
                e = f.htmlPrefilter(e);
                try {
                  for (; i > n; n++) 1 === (t = this[n] || {}).nodeType && (f.cleanData(ne(t, !1)), (t.innerHTML = e));
                  t = 0;
                } catch (o) {}
              }
              t && this.empty().append(e);
            },
            null,
            e,
            arguments.length
          );
        },
        replaceWith: function() {
          var e = [];
          return Ie(
            this,
            arguments,
            function(t) {
              var n = this.parentNode;
              f.inArray(this, e) < 0 && (f.cleanData(ne(this)), n && n.replaceChild(t, this));
            },
            e
          );
        }
      }),
      f.each(
        {
          appendTo: 'append',
          prependTo: 'prepend',
          insertBefore: 'before',
          insertAfter: 'after',
          replaceAll: 'replaceWith'
        },
        function(e, t) {
          f.fn[e] = function(e) {
            for (var n, i = 0, o = [], r = f(e), s = r.length - 1; s >= i; i++)
              (n = i === s ? this : this.clone(!0)), f(r[i])[t](n), a.apply(o, n.get());
            return this.pushStack(o);
          };
        }
      );
    var $e,
      Oe = { HTML: 'block', BODY: 'block' };
    function Re(e, t) {
      var n = f(t.createElement(e)).appendTo(t.body),
        i = f.css(n[0], 'display');
      return n.detach(), i;
    }
    function Me(e) {
      var t = i,
        n = Oe[e];
      return (
        n ||
          (('none' !== (n = Re(e, t)) && n) ||
            ((t = (
              ($e = ($e || f("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement))[0]
                .contentWindow || $e[0].contentDocument
            ).document).write(),
            t.close(),
            (n = Re(e, t)),
            $e.detach()),
          (Oe[e] = n)),
        n
      );
    }
    var Le = /^margin/,
      Fe = new RegExp('^(' + B + ')(?!px)[a-z%]+$', 'i'),
      je = function(e, t, n, i) {
        var o,
          r,
          a = {};
        for (r in t) (a[r] = e.style[r]), (e.style[r] = t[r]);
        for (r in ((o = n.apply(e, i || [])), t)) e.style[r] = a[r];
        return o;
      },
      Pe = i.documentElement;
    !(function() {
      var t,
        n,
        o,
        r,
        a,
        s,
        l = i.createElement('div'),
        c = i.createElement('div');
      if (c.style) {
        function u() {
          var u,
            d,
            p = i.documentElement;
          p.appendChild(l),
            (c.style.cssText =
              '-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%'),
            (t = o = s = !1),
            (n = a = !0),
            e.getComputedStyle &&
              ((d = e.getComputedStyle(c)),
              (t = '1%' !== (d || {}).top),
              (s = '2px' === (d || {}).marginLeft),
              (o = '4px' === (d || { width: '4px' }).width),
              (c.style.marginRight = '50%'),
              (n = '4px' === (d || { marginRight: '4px' }).marginRight),
              ((u = c.appendChild(i.createElement('div'))).style.cssText = c.style.cssText =
                '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0'),
              (u.style.marginRight = u.style.width = '0'),
              (c.style.width = '1px'),
              (a = !parseFloat((e.getComputedStyle(u) || {}).marginRight)),
              c.removeChild(u)),
            (c.style.display = 'none'),
            (r = 0 === c.getClientRects().length) &&
              ((c.style.display = ''),
              (c.innerHTML = '<table><tr><td></td><td>t</td></tr></table>'),
              (c.childNodes[0].style.borderCollapse = 'separate'),
              ((u = c.getElementsByTagName('td'))[0].style.cssText = 'margin:0;border:0;padding:0;display:none'),
              (r = 0 === u[0].offsetHeight) &&
                ((u[0].style.display = ''), (u[1].style.display = 'none'), (r = 0 === u[0].offsetHeight))),
            p.removeChild(l);
        }
        (c.style.cssText = 'float:left;opacity:.5'),
          (d.opacity = '0.5' === c.style.opacity),
          (d.cssFloat = !!c.style.cssFloat),
          (c.style.backgroundClip = 'content-box'),
          (c.cloneNode(!0).style.backgroundClip = ''),
          (d.clearCloneStyle = 'content-box' === c.style.backgroundClip),
          ((l = i.createElement('div')).style.cssText =
            'border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute'),
          (c.innerHTML = ''),
          l.appendChild(c),
          (d.boxSizing = '' === c.style.boxSizing || '' === c.style.MozBoxSizing || '' === c.style.WebkitBoxSizing),
          f.extend(d, {
            reliableHiddenOffsets: function() {
              return null == t && u(), r;
            },
            boxSizingReliable: function() {
              return null == t && u(), o;
            },
            pixelMarginRight: function() {
              return null == t && u(), n;
            },
            pixelPosition: function() {
              return null == t && u(), t;
            },
            reliableMarginRight: function() {
              return null == t && u(), a;
            },
            reliableMarginLeft: function() {
              return null == t && u(), s;
            }
          });
      }
    })();
    var qe,
      He,
      ze = /^(top|right|bottom|left)$/;
    function Be(e, t) {
      return {
        get: function() {
          return e() ? void delete this.get : (this.get = t).apply(this, arguments);
        }
      };
    }
    e.getComputedStyle
      ? ((qe = function(t) {
          var n = t.ownerDocument.defaultView;
          return (n && n.opener) || (n = e), n.getComputedStyle(t);
        }),
        (He = function(e, t, n) {
          var i,
            o,
            r,
            a,
            s = e.style;
          return (
            ('' !== (a = (n = n || qe(e)) ? n.getPropertyValue(t) || n[t] : void 0) && void 0 !== a) ||
              f.contains(e.ownerDocument, e) ||
              (a = f.style(e, t)),
            n &&
              !d.pixelMarginRight() &&
              Fe.test(a) &&
              Le.test(t) &&
              ((i = s.width),
              (o = s.minWidth),
              (r = s.maxWidth),
              (s.minWidth = s.maxWidth = s.width = a),
              (a = n.width),
              (s.width = i),
              (s.minWidth = o),
              (s.maxWidth = r)),
            void 0 === a ? a : a + ''
          );
        }))
      : Pe.currentStyle &&
        ((qe = function(e) {
          return e.currentStyle;
        }),
        (He = function(e, t, n) {
          var i,
            o,
            r,
            a,
            s = e.style;
          return (
            null == (a = (n = n || qe(e)) ? n[t] : void 0) && s && s[t] && (a = s[t]),
            Fe.test(a) &&
              !ze.test(t) &&
              ((i = s.left),
              (r = (o = e.runtimeStyle) && o.left) && (o.left = e.currentStyle.left),
              (s.left = 'fontSize' === t ? '1em' : a),
              (a = s.pixelLeft + 'px'),
              (s.left = i),
              r && (o.left = r)),
            void 0 === a ? a : a + '' || 'auto'
          );
        }));
    var We = /alpha\([^)]*\)/i,
      Ue = /opacity\s*=\s*([^)]*)/i,
      Ze = /^(none|table(?!-c[ea]).+)/,
      Ve = new RegExp('^(' + B + ')(.*)$', 'i'),
      Xe = { position: 'absolute', visibility: 'hidden', display: 'block' },
      Ke = { letterSpacing: '0', fontWeight: '400' },
      Ye = ['Webkit', 'O', 'Moz', 'ms'],
      Ge = i.createElement('div').style;
    function Je(e) {
      if (e in Ge) return e;
      for (var t = e.charAt(0).toUpperCase() + e.slice(1), n = Ye.length; n--; ) if ((e = Ye[n] + t) in Ge) return e;
    }
    function Qe(e, t) {
      for (var n, i, o, r = [], a = 0, s = e.length; s > a; a++)
        (i = e[a]).style &&
          ((r[a] = f._data(i, 'olddisplay')),
          (n = i.style.display),
          t
            ? (r[a] || 'none' !== n || (i.style.display = ''),
              '' === i.style.display && Z(i) && (r[a] = f._data(i, 'olddisplay', Me(i.nodeName))))
            : ((o = Z(i)), ((n && 'none' !== n) || !o) && f._data(i, 'olddisplay', o ? n : f.css(i, 'display'))));
      for (a = 0; s > a; a++)
        (i = e[a]).style &&
          ((t && 'none' !== i.style.display && '' !== i.style.display) || (i.style.display = t ? r[a] || '' : 'none'));
      return e;
    }
    function et(e, t, n) {
      var i = Ve.exec(t);
      return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || 'px') : t;
    }
    function tt(e, t, n, i, o) {
      for (var r = n === (i ? 'border' : 'content') ? 4 : 'width' === t ? 1 : 0, a = 0; 4 > r; r += 2)
        'margin' === n && (a += f.css(e, n + U[r], !0, o)),
          i
            ? ('content' === n && (a -= f.css(e, 'padding' + U[r], !0, o)),
              'margin' !== n && (a -= f.css(e, 'border' + U[r] + 'Width', !0, o)))
            : ((a += f.css(e, 'padding' + U[r], !0, o)),
              'padding' !== n && (a += f.css(e, 'border' + U[r] + 'Width', !0, o)));
      return a;
    }
    function nt(e, t, n) {
      var i = !0,
        o = 'width' === t ? e.offsetWidth : e.offsetHeight,
        r = qe(e),
        a = d.boxSizing && 'border-box' === f.css(e, 'boxSizing', !1, r);
      if (0 >= o || null == o) {
        if (((0 > (o = He(e, t, r)) || null == o) && (o = e.style[t]), Fe.test(o))) return o;
        (i = a && (d.boxSizingReliable() || o === e.style[t])), (o = parseFloat(o) || 0);
      }
      return o + tt(e, t, n || (a ? 'border' : 'content'), i, r) + 'px';
    }
    function it(e, t, n, i, o) {
      return new it.prototype.init(e, t, n, i, o);
    }
    f.extend({
      cssHooks: {
        opacity: {
          get: function(e, t) {
            if (t) {
              var n = He(e, 'opacity');
              return '' === n ? '1' : n;
            }
          }
        }
      },
      cssNumber: {
        animationIterationCount: !0,
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: { float: d.cssFloat ? 'cssFloat' : 'styleFloat' },
      style: function(e, t, n, i) {
        if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
          var o,
            r,
            a,
            s = f.camelCase(t),
            l = e.style;
          if (((t = f.cssProps[s] || (f.cssProps[s] = Je(s) || s)), (a = f.cssHooks[t] || f.cssHooks[s]), void 0 === n))
            return a && 'get' in a && void 0 !== (o = a.get(e, !1, i)) ? o : l[t];
          if (
            ('string' == (r = typeof n) && (o = W.exec(n)) && o[1] && ((n = V(e, t, o)), (r = 'number')),
            null != n &&
              n == n &&
              ('number' === r && (n += (o && o[3]) || (f.cssNumber[s] ? '' : 'px')),
              d.clearCloneStyle || '' !== n || 0 !== t.indexOf('background') || (l[t] = 'inherit'),
              !a || !('set' in a) || void 0 !== (n = a.set(e, n, i))))
          )
            try {
              l[t] = n;
            } catch (c) {}
        }
      },
      css: function(e, t, n, i) {
        var o,
          r,
          a,
          s = f.camelCase(t);
        return (
          (t = f.cssProps[s] || (f.cssProps[s] = Je(s) || s)),
          (a = f.cssHooks[t] || f.cssHooks[s]) && 'get' in a && (r = a.get(e, !0, n)),
          void 0 === r && (r = He(e, t, i)),
          'normal' === r && t in Ke && (r = Ke[t]),
          '' === n || n ? ((o = parseFloat(r)), !0 === n || isFinite(o) ? o || 0 : r) : r
        );
      }
    }),
      f.each(['height', 'width'], function(e, t) {
        f.cssHooks[t] = {
          get: function(e, n, i) {
            return n
              ? Ze.test(f.css(e, 'display')) && 0 === e.offsetWidth
                ? je(e, Xe, function() {
                    return nt(e, t, i);
                  })
                : nt(e, t, i)
              : void 0;
          },
          set: function(e, n, i) {
            var o = i && qe(e);
            return et(0, n, i ? tt(e, t, i, d.boxSizing && 'border-box' === f.css(e, 'boxSizing', !1, o), o) : 0);
          }
        };
      }),
      d.opacity ||
        (f.cssHooks.opacity = {
          get: function(e, t) {
            return Ue.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || '')
              ? 0.01 * parseFloat(RegExp.$1) + ''
              : t
              ? '1'
              : '';
          },
          set: function(e, t) {
            var n = e.style,
              i = e.currentStyle,
              o = f.isNumeric(t) ? 'alpha(opacity=' + 100 * t + ')' : '',
              r = (i && i.filter) || n.filter || '';
            (n.zoom = 1),
              ((t >= 1 || '' === t) &&
                '' === f.trim(r.replace(We, '')) &&
                n.removeAttribute &&
                (n.removeAttribute('filter'), '' === t || (i && !i.filter))) ||
                (n.filter = We.test(r) ? r.replace(We, o) : r + ' ' + o);
          }
        }),
      (f.cssHooks.marginRight = Be(d.reliableMarginRight, function(e, t) {
        return t ? je(e, { display: 'inline-block' }, He, [e, 'marginRight']) : void 0;
      })),
      (f.cssHooks.marginLeft = Be(d.reliableMarginLeft, function(e, t) {
        return t
          ? (parseFloat(He(e, 'marginLeft')) ||
              (f.contains(e.ownerDocument, e)
                ? e.getBoundingClientRect().left -
                  je(e, { marginLeft: 0 }, function() {
                    return e.getBoundingClientRect().left;
                  })
                : 0)) + 'px'
          : void 0;
      })),
      f.each({ margin: '', padding: '', border: 'Width' }, function(e, t) {
        (f.cssHooks[e + t] = {
          expand: function(n) {
            for (var i = 0, o = {}, r = 'string' == typeof n ? n.split(' ') : [n]; 4 > i; i++)
              o[e + U[i] + t] = r[i] || r[i - 2] || r[0];
            return o;
          }
        }),
          Le.test(e) || (f.cssHooks[e + t].set = et);
      }),
      f.fn.extend({
        css: function(e, t) {
          return X(
            this,
            function(e, t, n) {
              var i,
                o,
                r = {},
                a = 0;
              if (f.isArray(t)) {
                for (i = qe(e), o = t.length; o > a; a++) r[t[a]] = f.css(e, t[a], !1, i);
                return r;
              }
              return void 0 !== n ? f.style(e, t, n) : f.css(e, t);
            },
            e,
            t,
            arguments.length > 1
          );
        },
        show: function() {
          return Qe(this, !0);
        },
        hide: function() {
          return Qe(this);
        },
        toggle: function(e) {
          return 'boolean' == typeof e
            ? e
              ? this.show()
              : this.hide()
            : this.each(function() {
                Z(this) ? f(this).show() : f(this).hide();
              });
        }
      }),
      (f.Tween = it),
      ((it.prototype = {
        constructor: it,
        init: function(e, t, n, i, o, r) {
          (this.elem = e),
            (this.prop = n),
            (this.easing = o || f.easing._default),
            (this.options = t),
            (this.start = this.now = this.cur()),
            (this.end = i),
            (this.unit = r || (f.cssNumber[n] ? '' : 'px'));
        },
        cur: function() {
          var e = it.propHooks[this.prop];
          return e && e.get ? e.get(this) : it.propHooks._default.get(this);
        },
        run: function(e) {
          var t,
            n = it.propHooks[this.prop];
          return (
            (this.pos = t = this.options.duration
              ? f.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration)
              : e),
            (this.now = (this.end - this.start) * t + this.start),
            this.options.step && this.options.step.call(this.elem, this.now, this),
            n && n.set ? n.set(this) : it.propHooks._default.set(this),
            this
          );
        }
      }).init.prototype = it.prototype),
      ((it.propHooks = {
        _default: {
          get: function(e) {
            var t;
            return 1 !== e.elem.nodeType || (null != e.elem[e.prop] && null == e.elem.style[e.prop])
              ? e.elem[e.prop]
              : (t = f.css(e.elem, e.prop, '')) && 'auto' !== t
              ? t
              : 0;
          },
          set: function(e) {
            f.fx.step[e.prop]
              ? f.fx.step[e.prop](e)
              : 1 !== e.elem.nodeType || (null == e.elem.style[f.cssProps[e.prop]] && !f.cssHooks[e.prop])
              ? (e.elem[e.prop] = e.now)
              : f.style(e.elem, e.prop, e.now + e.unit);
          }
        }
      }).scrollTop = it.propHooks.scrollLeft = {
        set: function(e) {
          e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
        }
      }),
      (f.easing = {
        linear: function(e) {
          return e;
        },
        swing: function(e) {
          return 0.5 - Math.cos(e * Math.PI) / 2;
        },
        _default: 'swing'
      }),
      (f.fx = it.prototype.init),
      (f.fx.step = {});
    var ot,
      rt,
      at = /^(?:toggle|show|hide)$/,
      st = /queueHooks$/;
    function lt() {
      return (
        e.setTimeout(function() {
          ot = void 0;
        }),
        (ot = f.now())
      );
    }
    function ct(e, t) {
      var n,
        i = { height: e },
        o = 0;
      for (t = t ? 1 : 0; 4 > o; o += 2 - t) i['margin' + (n = U[o])] = i['padding' + n] = e;
      return t && (i.opacity = i.width = e), i;
    }
    function ut(e, t, n) {
      for (var i, o = (dt.tweeners[t] || []).concat(dt.tweeners['*']), r = 0, a = o.length; a > r; r++)
        if ((i = o[r].call(n, t, e))) return i;
    }
    function dt(e, t, n) {
      var i,
        o,
        r = 0,
        a = dt.prefilters.length,
        s = f.Deferred().always(function() {
          delete l.elem;
        }),
        l = function() {
          if (o) return !1;
          for (
            var t = ot || lt(),
              n = Math.max(0, c.startTime + c.duration - t),
              i = 1 - (n / c.duration || 0),
              r = 0,
              a = c.tweens.length;
            a > r;
            r++
          )
            c.tweens[r].run(i);
          return s.notifyWith(e, [c, i, n]), 1 > i && a ? n : (s.resolveWith(e, [c]), !1);
        },
        c = s.promise({
          elem: e,
          props: f.extend({}, t),
          opts: f.extend(!0, { specialEasing: {}, easing: f.easing._default }, n),
          originalProperties: t,
          originalOptions: n,
          startTime: ot || lt(),
          duration: n.duration,
          tweens: [],
          createTween: function(t, n) {
            var i = f.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
            return c.tweens.push(i), i;
          },
          stop: function(t) {
            var n = 0,
              i = t ? c.tweens.length : 0;
            if (o) return this;
            for (o = !0; i > n; n++) c.tweens[n].run(1);
            return t ? (s.notifyWith(e, [c, 1, 0]), s.resolveWith(e, [c, t])) : s.rejectWith(e, [c, t]), this;
          }
        }),
        u = c.props;
      for (
        (function(e, t) {
          var n, i, o, r, a;
          for (n in e)
            if (
              ((o = t[(i = f.camelCase(n))]),
              f.isArray((r = e[n])) && ((o = r[1]), (r = e[n] = r[0])),
              n !== i && ((e[i] = r), delete e[n]),
              (a = f.cssHooks[i]) && ('expand' in a))
            )
              for (n in ((r = a.expand(r)), delete e[i], r)) (n in e) || ((e[n] = r[n]), (t[n] = o));
            else t[i] = o;
        })(u, c.opts.specialEasing);
        a > r;
        r++
      )
        if ((i = dt.prefilters[r].call(c, e, u, c.opts)))
          return f.isFunction(i.stop) && (f._queueHooks(c.elem, c.opts.queue).stop = f.proxy(i.stop, i)), i;
      return (
        f.map(u, ut, c),
        f.isFunction(c.opts.start) && c.opts.start.call(e, c),
        f.fx.timer(f.extend(l, { elem: e, anim: c, queue: c.opts.queue })),
        c
          .progress(c.opts.progress)
          .done(c.opts.done, c.opts.complete)
          .fail(c.opts.fail)
          .always(c.opts.always)
      );
    }
    (f.Animation = f.extend(dt, {
      tweeners: {
        '*': [
          function(e, t) {
            var n = this.createTween(e, t);
            return V(n.elem, e, W.exec(t), n), n;
          }
        ]
      },
      tweener: function(e, t) {
        f.isFunction(e) ? ((t = e), (e = ['*'])) : (e = e.match(O));
        for (var n, i = 0, o = e.length; o > i; i++) (dt.tweeners[(n = e[i])] = dt.tweeners[n] || []).unshift(t);
      },
      prefilters: [
        function(e, t, n) {
          var i,
            o,
            r,
            a,
            s,
            l,
            c,
            u = this,
            p = {},
            h = e.style,
            m = e.nodeType && Z(e),
            v = f._data(e, 'fxshow');
          for (i in (n.queue ||
            (null == (s = f._queueHooks(e, 'fx')).unqueued &&
              ((s.unqueued = 0),
              (l = s.empty.fire),
              (s.empty.fire = function() {
                s.unqueued || l();
              })),
            s.unqueued++,
            u.always(function() {
              u.always(function() {
                s.unqueued--, f.queue(e, 'fx').length || s.empty.fire();
              });
            })),
          1 === e.nodeType &&
            ('height' in t || 'width' in t) &&
            ((n.overflow = [h.overflow, h.overflowX, h.overflowY]),
            'inline' === ('none' === (c = f.css(e, 'display')) ? f._data(e, 'olddisplay') || Me(e.nodeName) : c) &&
              'none' === f.css(e, 'float') &&
              (d.inlineBlockNeedsLayout && 'inline' !== Me(e.nodeName) ? (h.zoom = 1) : (h.display = 'inline-block'))),
          n.overflow &&
            ((h.overflow = 'hidden'),
            d.shrinkWrapBlocks() ||
              u.always(function() {
                (h.overflow = n.overflow[0]), (h.overflowX = n.overflow[1]), (h.overflowY = n.overflow[2]);
              })),
          t))
            if (at.exec((o = t[i]))) {
              if ((delete t[i], (r = r || 'toggle' === o), o === (m ? 'hide' : 'show'))) {
                if ('show' !== o || !v || void 0 === v[i]) continue;
                m = !0;
              }
              p[i] = (v && v[i]) || f.style(e, i);
            } else c = void 0;
          if (f.isEmptyObject(p)) 'inline' === ('none' === c ? Me(e.nodeName) : c) && (h.display = c);
          else
            for (i in (v ? 'hidden' in v && (m = v.hidden) : (v = f._data(e, 'fxshow', {})),
            r && (v.hidden = !m),
            m
              ? f(e).show()
              : u.done(function() {
                  f(e).hide();
                }),
            u.done(function() {
              var t;
              for (t in (f._removeData(e, 'fxshow'), p)) f.style(e, t, p[t]);
            }),
            p))
              (a = ut(m ? v[i] : 0, i, u)),
                i in v ||
                  ((v[i] = a.start), m && ((a.end = a.start), (a.start = 'width' === i || 'height' === i ? 1 : 0)));
        }
      ],
      prefilter: function(e, t) {
        t ? dt.prefilters.unshift(e) : dt.prefilters.push(e);
      }
    })),
      (f.speed = function(e, t, n) {
        var i =
          e && 'object' == typeof e
            ? f.extend({}, e)
            : {
                complete: n || (!n && t) || (f.isFunction(e) && e),
                duration: e,
                easing: (n && t) || (t && !f.isFunction(t) && t)
              };
        return (
          (i.duration = f.fx.off
            ? 0
            : 'number' == typeof i.duration
            ? i.duration
            : i.duration in f.fx.speeds
            ? f.fx.speeds[i.duration]
            : f.fx.speeds._default),
          (null != i.queue && !0 !== i.queue) || (i.queue = 'fx'),
          (i.old = i.complete),
          (i.complete = function() {
            f.isFunction(i.old) && i.old.call(this), i.queue && f.dequeue(this, i.queue);
          }),
          i
        );
      }),
      f.fn.extend({
        fadeTo: function(e, t, n, i) {
          return this.filter(Z)
            .css('opacity', 0)
            .show()
            .end()
            .animate({ opacity: t }, e, n, i);
        },
        animate: function(e, t, n, i) {
          var o = f.isEmptyObject(e),
            r = f.speed(t, n, i),
            a = function() {
              var t = dt(this, f.extend({}, e), r);
              (o || f._data(this, 'finish')) && t.stop(!0);
            };
          return (a.finish = a), o || !1 === r.queue ? this.each(a) : this.queue(r.queue, a);
        },
        stop: function(e, t, n) {
          var i = function(e) {
            var t = e.stop;
            delete e.stop, t(n);
          };
          return (
            'string' != typeof e && ((n = t), (t = e), (e = void 0)),
            t && !1 !== e && this.queue(e || 'fx', []),
            this.each(function() {
              var t = !0,
                o = null != e && e + 'queueHooks',
                r = f.timers,
                a = f._data(this);
              if (o) a[o] && a[o].stop && i(a[o]);
              else for (o in a) a[o] && a[o].stop && st.test(o) && i(a[o]);
              for (o = r.length; o--; )
                r[o].elem !== this || (null != e && r[o].queue !== e) || (r[o].anim.stop(n), (t = !1), r.splice(o, 1));
              (!t && n) || f.dequeue(this, e);
            })
          );
        },
        finish: function(e) {
          return (
            !1 !== e && (e = e || 'fx'),
            this.each(function() {
              var t,
                n = f._data(this),
                i = n[e + 'queue'],
                o = n[e + 'queueHooks'],
                r = f.timers,
                a = i ? i.length : 0;
              for (n.finish = !0, f.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--; )
                r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
              for (t = 0; a > t; t++) i[t] && i[t].finish && i[t].finish.call(this);
              delete n.finish;
            })
          );
        }
      }),
      f.each(['toggle', 'show', 'hide'], function(e, t) {
        var n = f.fn[t];
        f.fn[t] = function(e, i, o) {
          return null == e || 'boolean' == typeof e ? n.apply(this, arguments) : this.animate(ct(t, !0), e, i, o);
        };
      }),
      f.each(
        {
          slideDown: ct('show'),
          slideUp: ct('hide'),
          slideToggle: ct('toggle'),
          fadeIn: { opacity: 'show' },
          fadeOut: { opacity: 'hide' },
          fadeToggle: { opacity: 'toggle' }
        },
        function(e, t) {
          f.fn[e] = function(e, n, i) {
            return this.animate(t, e, n, i);
          };
        }
      ),
      (f.timers = []),
      (f.fx.tick = function() {
        var e,
          t = f.timers,
          n = 0;
        for (ot = f.now(); n < t.length; n++) (e = t[n])() || t[n] !== e || t.splice(n--, 1);
        t.length || f.fx.stop(), (ot = void 0);
      }),
      (f.fx.timer = function(e) {
        f.timers.push(e), e() ? f.fx.start() : f.timers.pop();
      }),
      (f.fx.interval = 13),
      (f.fx.start = function() {
        rt || (rt = e.setInterval(f.fx.tick, f.fx.interval));
      }),
      (f.fx.stop = function() {
        e.clearInterval(rt), (rt = null);
      }),
      (f.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
      (f.fn.delay = function(t, n) {
        return (
          (t = (f.fx && f.fx.speeds[t]) || t),
          this.queue((n = n || 'fx'), function(n, i) {
            var o = e.setTimeout(n, t);
            i.stop = function() {
              e.clearTimeout(o);
            };
          })
        );
      }),
      (function() {
        var e,
          t = i.createElement('input'),
          n = i.createElement('div'),
          o = i.createElement('select'),
          r = o.appendChild(i.createElement('option'));
        (n = i.createElement('div')).setAttribute('className', 't'),
          (n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
          (e = n.getElementsByTagName('a')[0]),
          t.setAttribute('type', 'checkbox'),
          n.appendChild(t),
          ((e = n.getElementsByTagName('a')[0]).style.cssText = 'top:1px'),
          (d.getSetAttribute = 't' !== n.className),
          (d.style = /top/.test(e.getAttribute('style'))),
          (d.hrefNormalized = '/a' === e.getAttribute('href')),
          (d.checkOn = !!t.value),
          (d.optSelected = r.selected),
          (d.enctype = !!i.createElement('form').enctype),
          (o.disabled = !0),
          (d.optDisabled = !r.disabled),
          (t = i.createElement('input')).setAttribute('value', ''),
          (d.input = '' === t.getAttribute('value')),
          (t.value = 't'),
          t.setAttribute('type', 'radio'),
          (d.radioValue = 't' === t.value);
      })();
    var pt = /\r/g,
      ft = /[\x20\t\r\n\f]+/g;
    f.fn.extend({
      val: function(e) {
        var t,
          n,
          i,
          o = this[0];
        return arguments.length
          ? ((i = f.isFunction(e)),
            this.each(function(n) {
              var o;
              1 === this.nodeType &&
                (null == (o = i ? e.call(this, n, f(this).val()) : e)
                  ? (o = '')
                  : 'number' == typeof o
                  ? (o += '')
                  : f.isArray(o) &&
                    (o = f.map(o, function(e) {
                      return null == e ? '' : e + '';
                    })),
                ((t = f.valHooks[this.type] || f.valHooks[this.nodeName.toLowerCase()]) &&
                  'set' in t &&
                  void 0 !== t.set(this, o, 'value')) ||
                  (this.value = o));
            }))
          : o
          ? (t = f.valHooks[o.type] || f.valHooks[o.nodeName.toLowerCase()]) &&
            'get' in t &&
            void 0 !== (n = t.get(o, 'value'))
            ? n
            : 'string' == typeof (n = o.value)
            ? n.replace(pt, '')
            : null == n
            ? ''
            : n
          : void 0;
      }
    }),
      f.extend({
        valHooks: {
          option: {
            get: function(e) {
              var t = f.find.attr(e, 'value');
              return null != t ? t : f.trim(f.text(e)).replace(ft, ' ');
            }
          },
          select: {
            get: function(e) {
              for (
                var t,
                  n,
                  i = e.options,
                  o = e.selectedIndex,
                  r = 'select-one' === e.type || 0 > o,
                  a = r ? null : [],
                  s = r ? o + 1 : i.length,
                  l = 0 > o ? s : r ? o : 0;
                s > l;
                l++
              )
                if (
                  ((n = i[l]).selected || l === o) &&
                  (d.optDisabled ? !n.disabled : null === n.getAttribute('disabled')) &&
                  (!n.parentNode.disabled || !f.nodeName(n.parentNode, 'optgroup'))
                ) {
                  if (((t = f(n).val()), r)) return t;
                  a.push(t);
                }
              return a;
            },
            set: function(e, t) {
              for (var n, i, o = e.options, r = f.makeArray(t), a = o.length; a--; )
                if (f.inArray(f.valHooks.option.get((i = o[a])), r) > -1)
                  try {
                    i.selected = n = !0;
                  } catch (s) {}
                else i.selected = !1;
              return n || (e.selectedIndex = -1), o;
            }
          }
        }
      }),
      f.each(['radio', 'checkbox'], function() {
        (f.valHooks[this] = {
          set: function(e, t) {
            return f.isArray(t) ? (e.checked = f.inArray(f(e).val(), t) > -1) : void 0;
          }
        }),
          d.checkOn ||
            (f.valHooks[this].get = function(e) {
              return null === e.getAttribute('value') ? 'on' : e.value;
            });
      });
    var ht,
      mt,
      vt = f.expr.attrHandle,
      gt = /^(?:checked|selected)$/i,
      yt = d.getSetAttribute,
      bt = d.input;
    f.fn.extend({
      attr: function(e, t) {
        return X(this, f.attr, e, t, arguments.length > 1);
      },
      removeAttr: function(e) {
        return this.each(function() {
          f.removeAttr(this, e);
        });
      }
    }),
      f.extend({
        attr: function(e, t, n) {
          var i,
            o,
            r = e.nodeType;
          if (3 !== r && 8 !== r && 2 !== r)
            return void 0 === e.getAttribute
              ? f.prop(e, t, n)
              : ((1 === r && f.isXMLDoc(e)) ||
                  ((t = t.toLowerCase()), (o = f.attrHooks[t] || (f.expr.match.bool.test(t) ? mt : ht))),
                void 0 !== n
                  ? null === n
                    ? void f.removeAttr(e, t)
                    : o && 'set' in o && void 0 !== (i = o.set(e, n, t))
                    ? i
                    : (e.setAttribute(t, n + ''), n)
                  : o && 'get' in o && null !== (i = o.get(e, t))
                  ? i
                  : null == (i = f.find.attr(e, t))
                  ? void 0
                  : i);
        },
        attrHooks: {
          type: {
            set: function(e, t) {
              if (!d.radioValue && 'radio' === t && f.nodeName(e, 'input')) {
                var n = e.value;
                return e.setAttribute('type', t), n && (e.value = n), t;
              }
            }
          }
        },
        removeAttr: function(e, t) {
          var n,
            i,
            o = 0,
            r = t && t.match(O);
          if (r && 1 === e.nodeType)
            for (; (n = r[o++]); )
              (i = f.propFix[n] || n),
                f.expr.match.bool.test(n)
                  ? (bt && yt) || !gt.test(n)
                    ? (e[i] = !1)
                    : (e[f.camelCase('default-' + n)] = e[i] = !1)
                  : f.attr(e, n, ''),
                e.removeAttribute(yt ? n : i);
        }
      }),
      (mt = {
        set: function(e, t, n) {
          return (
            !1 === t
              ? f.removeAttr(e, n)
              : (bt && yt) || !gt.test(n)
              ? e.setAttribute((!yt && f.propFix[n]) || n, n)
              : (e[f.camelCase('default-' + n)] = e[n] = !0),
            n
          );
        }
      }),
      f.each(f.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = vt[t] || f.find.attr;
        vt[t] =
          (bt && yt) || !gt.test(t)
            ? function(e, t, i) {
                var o, r;
                return (
                  i || ((r = vt[t]), (vt[t] = o), (o = null != n(e, t, i) ? t.toLowerCase() : null), (vt[t] = r)), o
                );
              }
            : function(e, t, n) {
                return n ? void 0 : e[f.camelCase('default-' + t)] ? t.toLowerCase() : null;
              };
      }),
      (bt && yt) ||
        (f.attrHooks.value = {
          set: function(e, t, n) {
            return f.nodeName(e, 'input') ? void (e.defaultValue = t) : ht && ht.set(e, t, n);
          }
        }),
      yt ||
        ((ht = {
          set: function(e, t, n) {
            var i = e.getAttributeNode(n);
            return (
              i || e.setAttributeNode((i = e.ownerDocument.createAttribute(n))),
              (i.value = t += ''),
              'value' === n || t === e.getAttribute(n) ? t : void 0
            );
          }
        }),
        (vt.id = vt.name = vt.coords = function(e, t, n) {
          var i;
          return n ? void 0 : (i = e.getAttributeNode(t)) && '' !== i.value ? i.value : null;
        }),
        (f.valHooks.button = {
          get: function(e, t) {
            var n = e.getAttributeNode(t);
            return n && n.specified ? n.value : void 0;
          },
          set: ht.set
        }),
        (f.attrHooks.contenteditable = {
          set: function(e, t, n) {
            ht.set(e, '' !== t && t, n);
          }
        }),
        f.each(['width', 'height'], function(e, t) {
          f.attrHooks[t] = {
            set: function(e, n) {
              return '' === n ? (e.setAttribute(t, 'auto'), n) : void 0;
            }
          };
        })),
      d.style ||
        (f.attrHooks.style = {
          get: function(e) {
            return e.style.cssText || void 0;
          },
          set: function(e, t) {
            return (e.style.cssText = t + '');
          }
        });
    var xt = /^(?:input|select|textarea|button|object)$/i,
      wt = /^(?:a|area)$/i;
    f.fn.extend({
      prop: function(e, t) {
        return X(this, f.prop, e, t, arguments.length > 1);
      },
      removeProp: function(e) {
        return (
          (e = f.propFix[e] || e),
          this.each(function() {
            try {
              (this[e] = void 0), delete this[e];
            } catch (t) {}
          })
        );
      }
    }),
      f.extend({
        prop: function(e, t, n) {
          var i,
            o,
            r = e.nodeType;
          if (3 !== r && 8 !== r && 2 !== r)
            return (
              (1 === r && f.isXMLDoc(e)) || (o = f.propHooks[(t = f.propFix[t] || t)]),
              void 0 !== n
                ? o && 'set' in o && void 0 !== (i = o.set(e, n, t))
                  ? i
                  : (e[t] = n)
                : o && 'get' in o && null !== (i = o.get(e, t))
                ? i
                : e[t]
            );
        },
        propHooks: {
          tabIndex: {
            get: function(e) {
              var t = f.find.attr(e, 'tabindex');
              return t ? parseInt(t, 10) : xt.test(e.nodeName) || (wt.test(e.nodeName) && e.href) ? 0 : -1;
            }
          }
        },
        propFix: { for: 'htmlFor', class: 'className' }
      }),
      d.hrefNormalized ||
        f.each(['href', 'src'], function(e, t) {
          f.propHooks[t] = {
            get: function(e) {
              return e.getAttribute(t, 4);
            }
          };
        }),
      d.optSelected ||
        (f.propHooks.selected = {
          get: function(e) {
            return null;
          },
          set: function(e) {}
        }),
      f.each(
        [
          'tabIndex',
          'readOnly',
          'maxLength',
          'cellSpacing',
          'cellPadding',
          'rowSpan',
          'colSpan',
          'useMap',
          'frameBorder',
          'contentEditable'
        ],
        function() {
          f.propFix[this.toLowerCase()] = this;
        }
      ),
      d.enctype || (f.propFix.enctype = 'encoding');
    var Ct = /[\t\r\n\f]/g;
    function Tt(e) {
      return f.attr(e, 'class') || '';
    }
    f.fn.extend({
      addClass: function(e) {
        var t,
          n,
          i,
          o,
          r,
          a,
          s,
          l = 0;
        if (f.isFunction(e))
          return this.each(function(t) {
            f(this).addClass(e.call(this, t, Tt(this)));
          });
        if ('string' == typeof e && e)
          for (t = e.match(O) || []; (n = this[l++]); )
            if (((o = Tt(n)), (i = 1 === n.nodeType && (' ' + o + ' ').replace(Ct, ' ')))) {
              for (a = 0; (r = t[a++]); ) i.indexOf(' ' + r + ' ') < 0 && (i += r + ' ');
              o !== (s = f.trim(i)) && f.attr(n, 'class', s);
            }
        return this;
      },
      removeClass: function(e) {
        var t,
          n,
          i,
          o,
          r,
          a,
          s,
          l = 0;
        if (f.isFunction(e))
          return this.each(function(t) {
            f(this).removeClass(e.call(this, t, Tt(this)));
          });
        if (!arguments.length) return this.attr('class', '');
        if ('string' == typeof e && e)
          for (t = e.match(O) || []; (n = this[l++]); )
            if (((o = Tt(n)), (i = 1 === n.nodeType && (' ' + o + ' ').replace(Ct, ' ')))) {
              for (a = 0; (r = t[a++]); ) for (; i.indexOf(' ' + r + ' ') > -1; ) i = i.replace(' ' + r + ' ', ' ');
              o !== (s = f.trim(i)) && f.attr(n, 'class', s);
            }
        return this;
      },
      toggleClass: function(e, t) {
        var n = typeof e;
        return 'boolean' == typeof t && 'string' === n
          ? t
            ? this.addClass(e)
            : this.removeClass(e)
          : f.isFunction(e)
          ? this.each(function(n) {
              f(this).toggleClass(e.call(this, n, Tt(this), t), t);
            })
          : this.each(function() {
              var t, i, o, r;
              if ('string' === n)
                for (i = 0, o = f(this), r = e.match(O) || []; (t = r[i++]); )
                  o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
              else
                (void 0 !== e && 'boolean' !== n) ||
                  ((t = Tt(this)) && f._data(this, '__className__', t),
                  f.attr(this, 'class', t || !1 === e ? '' : f._data(this, '__className__') || ''));
            });
      },
      hasClass: function(e) {
        var t,
          n,
          i = 0;
        for (t = ' ' + e + ' '; (n = this[i++]); )
          if (1 === n.nodeType && (' ' + Tt(n) + ' ').replace(Ct, ' ').indexOf(t) > -1) return !0;
        return !1;
      }
    }),
      f.each(
        'blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu'.split(
          ' '
        ),
        function(e, t) {
          f.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t);
          };
        }
      ),
      f.fn.extend({
        hover: function(e, t) {
          return this.mouseenter(e).mouseleave(t || e);
        }
      });
    var St = e.location,
      Et = f.now(),
      kt = /\?/,
      _t = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    (f.parseJSON = function(t) {
      if (e.JSON && e.JSON.parse) return e.JSON.parse(t + '');
      var n,
        i = null,
        o = f.trim(t + '');
      return o &&
        !f.trim(
          o.replace(_t, function(e, t, o, r) {
            return n && t && (i = 0), 0 === i ? e : ((n = o || t), (i += !r - !o), '');
          })
        )
        ? Function('return ' + o)()
        : f.error('Invalid JSON: ' + t);
    }),
      (f.parseXML = function(t) {
        var n;
        if (!t || 'string' != typeof t) return null;
        try {
          e.DOMParser
            ? (n = new e.DOMParser().parseFromString(t, 'text/xml'))
            : (((n = new e.ActiveXObject('Microsoft.XMLDOM')).async = 'false'), n.loadXML(t));
        } catch (o) {
          n = void 0;
        }
        return (
          (n && n.documentElement && !n.getElementsByTagName('parsererror').length) || f.error('Invalid XML: ' + t), n
        );
      });
    var At = /#.*$/,
      Dt = /([?&])_=[^&]*/,
      It = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
      Nt = /^(?:GET|HEAD)$/,
      $t = /^\/\//,
      Ot = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
      Rt = {},
      Mt = {},
      Lt = '*/'.concat('*'),
      Ft = St.href,
      jt = Ot.exec(Ft.toLowerCase()) || [];
    function Pt(e) {
      return function(t, n) {
        'string' != typeof t && ((n = t), (t = '*'));
        var i,
          o = 0,
          r = t.toLowerCase().match(O) || [];
        if (f.isFunction(n))
          for (; (i = r[o++]); )
            '+' === i.charAt(0)
              ? ((i = i.slice(1) || '*'), (e[i] = e[i] || []).unshift(n))
              : (e[i] = e[i] || []).push(n);
      };
    }
    function qt(e, t, n, i) {
      var o = {},
        r = e === Mt;
      function a(s) {
        var l;
        return (
          (o[s] = !0),
          f.each(e[s] || [], function(e, s) {
            var c = s(t, n, i);
            return 'string' != typeof c || r || o[c] ? (r ? !(l = c) : void 0) : (t.dataTypes.unshift(c), a(c), !1);
          }),
          l
        );
      }
      return a(t.dataTypes[0]) || (!o['*'] && a('*'));
    }
    function Ht(e, t) {
      var n,
        i,
        o = f.ajaxSettings.flatOptions || {};
      for (i in t) void 0 !== t[i] && ((o[i] ? e : n || (n = {}))[i] = t[i]);
      return n && f.extend(!0, e, n), e;
    }
    function zt(e) {
      return (e.style && e.style.display) || f.css(e, 'display');
    }
    f.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: Ft,
        type: 'GET',
        isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(jt[1]),
        global: !0,
        processData: !0,
        async: !0,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        accepts: {
          '*': Lt,
          text: 'text/plain',
          html: 'text/html',
          xml: 'application/xml, text/xml',
          json: 'application/json, text/javascript'
        },
        contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
        responseFields: { xml: 'responseXML', text: 'responseText', json: 'responseJSON' },
        converters: { '* text': String, 'text html': !0, 'text json': f.parseJSON, 'text xml': f.parseXML },
        flatOptions: { url: !0, context: !0 }
      },
      ajaxSetup: function(e, t) {
        return t ? Ht(Ht(e, f.ajaxSettings), t) : Ht(f.ajaxSettings, e);
      },
      ajaxPrefilter: Pt(Rt),
      ajaxTransport: Pt(Mt),
      ajax: function(t, n) {
        'object' == typeof t && ((n = t), (t = void 0));
        var i,
          o,
          r,
          a,
          s,
          l,
          c,
          u,
          d = f.ajaxSetup({}, (n = n || {})),
          p = d.context || d,
          h = d.context && (p.nodeType || p.jquery) ? f(p) : f.event,
          m = f.Deferred(),
          v = f.Callbacks('once memory'),
          g = d.statusCode || {},
          y = {},
          b = {},
          x = 0,
          w = 'canceled',
          C = {
            readyState: 0,
            getResponseHeader: function(e) {
              var t;
              if (2 === x) {
                if (!u) for (u = {}; (t = It.exec(a)); ) u[t[1].toLowerCase()] = t[2];
                t = u[e.toLowerCase()];
              }
              return null == t ? null : t;
            },
            getAllResponseHeaders: function() {
              return 2 === x ? a : null;
            },
            setRequestHeader: function(e, t) {
              var n = e.toLowerCase();
              return x || ((e = b[n] = b[n] || e), (y[e] = t)), this;
            },
            overrideMimeType: function(e) {
              return x || (d.mimeType = e), this;
            },
            statusCode: function(e) {
              var t;
              if (e)
                if (2 > x) for (t in e) g[t] = [g[t], e[t]];
                else C.always(e[C.status]);
              return this;
            },
            abort: function(e) {
              var t = e || w;
              return c && c.abort(t), S(0, t), this;
            }
          };
        if (
          ((m.promise(C).complete = v.add),
          (C.success = C.done),
          (C.error = C.fail),
          (d.url = ((t || d.url || Ft) + '').replace(At, '').replace($t, jt[1] + '//')),
          (d.type = n.method || n.type || d.method || d.type),
          (d.dataTypes = f
            .trim(d.dataType || '*')
            .toLowerCase()
            .match(O) || ['']),
          null == d.crossDomain &&
            ((i = Ot.exec(d.url.toLowerCase())),
            (d.crossDomain = !(
              !i ||
              (i[1] === jt[1] &&
                i[2] === jt[2] &&
                (i[3] || ('http:' === i[1] ? '80' : '443')) === (jt[3] || ('http:' === jt[1] ? '80' : '443')))
            ))),
          d.data && d.processData && 'string' != typeof d.data && (d.data = f.param(d.data, d.traditional)),
          qt(Rt, d, n, C),
          2 === x)
        )
          return C;
        for (o in ((l = f.event && d.global) && 0 == f.active++ && f.event.trigger('ajaxStart'),
        (d.type = d.type.toUpperCase()),
        (d.hasContent = !Nt.test(d.type)),
        (r = d.url),
        d.hasContent ||
          (d.data && ((r = d.url += (kt.test(r) ? '&' : '?') + d.data), delete d.data),
          !1 === d.cache &&
            (d.url = Dt.test(r) ? r.replace(Dt, '$1_=' + Et++) : r + (kt.test(r) ? '&' : '?') + '_=' + Et++)),
        d.ifModified &&
          (f.lastModified[r] && C.setRequestHeader('If-Modified-Since', f.lastModified[r]),
          f.etag[r] && C.setRequestHeader('If-None-Match', f.etag[r])),
        ((d.data && d.hasContent && !1 !== d.contentType) || n.contentType) &&
          C.setRequestHeader('Content-Type', d.contentType),
        C.setRequestHeader(
          'Accept',
          d.dataTypes[0] && d.accepts[d.dataTypes[0]]
            ? d.accepts[d.dataTypes[0]] + ('*' !== d.dataTypes[0] ? ', ' + Lt + '; q=0.01' : '')
            : d.accepts['*']
        ),
        d.headers))
          C.setRequestHeader(o, d.headers[o]);
        if (d.beforeSend && (!1 === d.beforeSend.call(p, C, d) || 2 === x)) return C.abort();
        for (o in ((w = 'abort'), { success: 1, error: 1, complete: 1 })) C[o](d[o]);
        if ((c = qt(Mt, d, n, C))) {
          if (((C.readyState = 1), l && h.trigger('ajaxSend', [C, d]), 2 === x)) return C;
          d.async &&
            d.timeout > 0 &&
            (s = e.setTimeout(function() {
              C.abort('timeout');
            }, d.timeout));
          try {
            (x = 1), c.send(y, S);
          } catch (T) {
            if (!(2 > x)) throw T;
            S(-1, T);
          }
        } else S(-1, 'No Transport');
        function S(t, n, i, o) {
          var u,
            y,
            b,
            w,
            T,
            S = n;
          2 !== x &&
            ((x = 2),
            s && e.clearTimeout(s),
            (c = void 0),
            (a = o || ''),
            (C.readyState = t > 0 ? 4 : 0),
            (u = (t >= 200 && 300 > t) || 304 === t),
            i &&
              (w = (function(e, t, n) {
                for (var i, o, r, a, s = e.contents, l = e.dataTypes; '*' === l[0]; )
                  l.shift(), void 0 === o && (o = e.mimeType || t.getResponseHeader('Content-Type'));
                if (o)
                  for (a in s)
                    if (s[a] && s[a].test(o)) {
                      l.unshift(a);
                      break;
                    }
                if (l[0] in n) r = l[0];
                else {
                  for (a in n) {
                    if (!l[0] || e.converters[a + ' ' + l[0]]) {
                      r = a;
                      break;
                    }
                    i || (i = a);
                  }
                  r = r || i;
                }
                return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0;
              })(d, C, i)),
            (w = (function(e, t, n, i) {
              var o,
                r,
                a,
                s,
                l,
                c = {},
                u = e.dataTypes.slice();
              if (u[1]) for (a in e.converters) c[a.toLowerCase()] = e.converters[a];
              for (r = u.shift(); r; )
                if (
                  (e.responseFields[r] && (n[e.responseFields[r]] = t),
                  !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
                  (l = r),
                  (r = u.shift()))
                )
                  if ('*' === r) r = l;
                  else if ('*' !== l && l !== r) {
                    if (!(a = c[l + ' ' + r] || c['* ' + r]))
                      for (o in c)
                        if ((s = o.split(' '))[1] === r && (a = c[l + ' ' + s[0]] || c['* ' + s[0]])) {
                          !0 === a ? (a = c[o]) : !0 !== c[o] && ((r = s[0]), u.unshift(s[1]));
                          break;
                        }
                    if (!0 !== a)
                      if (a && e.throws) t = a(t);
                      else
                        try {
                          t = a(t);
                        } catch (d) {
                          return { state: 'parsererror', error: a ? d : 'No conversion from ' + l + ' to ' + r };
                        }
                  }
              return { state: 'success', data: t };
            })(d, w, C, u)),
            u
              ? (d.ifModified &&
                  ((T = C.getResponseHeader('Last-Modified')) && (f.lastModified[r] = T),
                  (T = C.getResponseHeader('etag')) && (f.etag[r] = T)),
                204 === t || 'HEAD' === d.type
                  ? (S = 'nocontent')
                  : 304 === t
                  ? (S = 'notmodified')
                  : ((S = w.state), (y = w.data), (u = !(b = w.error))))
              : ((b = S), (!t && S) || ((S = 'error'), 0 > t && (t = 0))),
            (C.status = t),
            (C.statusText = (n || S) + ''),
            u ? m.resolveWith(p, [y, S, C]) : m.rejectWith(p, [C, S, b]),
            C.statusCode(g),
            (g = void 0),
            l && h.trigger(u ? 'ajaxSuccess' : 'ajaxError', [C, d, u ? y : b]),
            v.fireWith(p, [C, S]),
            l && (h.trigger('ajaxComplete', [C, d]), --f.active || f.event.trigger('ajaxStop')));
        }
        return C;
      },
      getJSON: function(e, t, n) {
        return f.get(e, t, n, 'json');
      },
      getScript: function(e, t) {
        return f.get(e, void 0, t, 'script');
      }
    }),
      f.each(['get', 'post'], function(e, t) {
        f[t] = function(e, n, i, o) {
          return (
            f.isFunction(n) && ((o = o || i), (i = n), (n = void 0)),
            f.ajax(f.extend({ url: e, type: t, dataType: o, data: n, success: i }, f.isPlainObject(e) && e))
          );
        };
      }),
      (f._evalUrl = function(e) {
        return f.ajax({ url: e, type: 'GET', dataType: 'script', cache: !0, async: !1, global: !1, throws: !0 });
      }),
      f.fn.extend({
        wrapAll: function(e) {
          if (f.isFunction(e))
            return this.each(function(t) {
              f(this).wrapAll(e.call(this, t));
            });
          if (this[0]) {
            var t = f(e, this[0].ownerDocument)
              .eq(0)
              .clone(!0);
            this[0].parentNode && t.insertBefore(this[0]),
              t
                .map(function() {
                  for (var e = this; e.firstChild && 1 === e.firstChild.nodeType; ) e = e.firstChild;
                  return e;
                })
                .append(this);
          }
          return this;
        },
        wrapInner: function(e) {
          return f.isFunction(e)
            ? this.each(function(t) {
                f(this).wrapInner(e.call(this, t));
              })
            : this.each(function() {
                var t = f(this),
                  n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e);
              });
        },
        wrap: function(e) {
          var t = f.isFunction(e);
          return this.each(function(n) {
            f(this).wrapAll(t ? e.call(this, n) : e);
          });
        },
        unwrap: function() {
          return this.parent()
            .each(function() {
              f.nodeName(this, 'body') || f(this).replaceWith(this.childNodes);
            })
            .end();
        }
      }),
      (f.expr.filters.hidden = function(e) {
        return d.reliableHiddenOffsets()
          ? e.offsetWidth <= 0 && e.offsetHeight <= 0 && !e.getClientRects().length
          : (function(e) {
              if (!f.contains(e.ownerDocument || i, e)) return !0;
              for (; e && 1 === e.nodeType; ) {
                if ('none' === zt(e) || 'hidden' === e.type) return !0;
                e = e.parentNode;
              }
              return !1;
            })(e);
      }),
      (f.expr.filters.visible = function(e) {
        return !f.expr.filters.hidden(e);
      });
    var Bt = /%20/g,
      Wt = /\[\]$/,
      Ut = /\r?\n/g,
      Zt = /^(?:submit|button|image|reset|file)$/i,
      Vt = /^(?:input|select|textarea|keygen)/i;
    function Xt(e, t, n, i) {
      var o;
      if (f.isArray(t))
        f.each(t, function(t, o) {
          n || Wt.test(e) ? i(e, o) : Xt(e + '[' + ('object' == typeof o && null != o ? t : '') + ']', o, n, i);
        });
      else if (n || 'object' !== f.type(t)) i(e, t);
      else for (o in t) Xt(e + '[' + o + ']', t[o], n, i);
    }
    (f.param = function(e, t) {
      var n,
        i = [],
        o = function(e, t) {
          (t = f.isFunction(t) ? t() : null == t ? '' : t),
            (i[i.length] = encodeURIComponent(e) + '=' + encodeURIComponent(t));
        };
      if (
        (void 0 === t && (t = f.ajaxSettings && f.ajaxSettings.traditional),
        f.isArray(e) || (e.jquery && !f.isPlainObject(e)))
      )
        f.each(e, function() {
          o(this.name, this.value);
        });
      else for (n in e) Xt(n, e[n], t, o);
      return i.join('&').replace(Bt, '+');
    }),
      f.fn.extend({
        serialize: function() {
          return f.param(this.serializeArray());
        },
        serializeArray: function() {
          return this.map(function() {
            var e = f.prop(this, 'elements');
            return e ? f.makeArray(e) : this;
          })
            .filter(function() {
              var e = this.type;
              return (
                this.name &&
                !f(this).is(':disabled') &&
                Vt.test(this.nodeName) &&
                !Zt.test(e) &&
                (this.checked || !K.test(e))
              );
            })
            .map(function(e, t) {
              var n = f(this).val();
              return null == n
                ? null
                : f.isArray(n)
                ? f.map(n, function(e) {
                    return { name: t.name, value: e.replace(Ut, '\r\n') };
                  })
                : { name: t.name, value: n.replace(Ut, '\r\n') };
            })
            .get();
        }
      }),
      (f.ajaxSettings.xhr =
        void 0 !== e.ActiveXObject
          ? function() {
              return this.isLocal
                ? Qt()
                : i.documentMode > 8
                ? Jt()
                : (/^(get|post|head|put|delete|options)$/i.test(this.type) && Jt()) || Qt();
            }
          : Jt);
    var Kt = 0,
      Yt = {},
      Gt = f.ajaxSettings.xhr();
    function Jt() {
      try {
        return new e.XMLHttpRequest();
      } catch (t) {}
    }
    function Qt() {
      try {
        return new e.ActiveXObject('Microsoft.XMLHTTP');
      } catch (t) {}
    }
    e.attachEvent &&
      e.attachEvent('onunload', function() {
        for (var e in Yt) Yt[e](void 0, !0);
      }),
      (d.cors = !!Gt && 'withCredentials' in Gt),
      (Gt = d.ajax = !!Gt) &&
        f.ajaxTransport(function(t) {
          var n;
          if (!t.crossDomain || d.cors)
            return {
              send: function(i, o) {
                var r,
                  a = t.xhr(),
                  s = ++Kt;
                if ((a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields))
                  for (r in t.xhrFields) a[r] = t.xhrFields[r];
                for (r in (t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType),
                t.crossDomain || i['X-Requested-With'] || (i['X-Requested-With'] = 'XMLHttpRequest'),
                i))
                  void 0 !== i[r] && a.setRequestHeader(r, i[r] + '');
                a.send((t.hasContent && t.data) || null),
                  (n = function(e, i) {
                    var r, l, c;
                    if (n && (i || 4 === a.readyState))
                      if ((delete Yt[s], (n = void 0), (a.onreadystatechange = f.noop), i))
                        4 !== a.readyState && a.abort();
                      else {
                        (c = {}), (r = a.status), 'string' == typeof a.responseText && (c.text = a.responseText);
                        try {
                          l = a.statusText;
                        } catch (u) {
                          l = '';
                        }
                        r || !t.isLocal || t.crossDomain ? 1223 === r && (r = 204) : (r = c.text ? 200 : 404);
                      }
                    c && o(r, l, c, a.getAllResponseHeaders());
                  }),
                  t.async ? (4 === a.readyState ? e.setTimeout(n) : (a.onreadystatechange = Yt[s] = n)) : n();
              },
              abort: function() {
                n && n(void 0, !0);
              }
            };
        }),
      f.ajaxSetup({
        accepts: {
          script: 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript'
        },
        contents: { script: /\b(?:java|ecma)script\b/ },
        converters: {
          'text script': function(e) {
            return f.globalEval(e), e;
          }
        }
      }),
      f.ajaxPrefilter('script', function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && ((e.type = 'GET'), (e.global = !1));
      }),
      f.ajaxTransport('script', function(e) {
        if (e.crossDomain) {
          var t,
            n = i.head || f('head')[0] || i.documentElement;
          return {
            send: function(o, r) {
              ((t = i.createElement('script')).async = !0),
                e.scriptCharset && (t.charset = e.scriptCharset),
                (t.src = e.url),
                (t.onload = t.onreadystatechange = function(e, n) {
                  (n || !t.readyState || /loaded|complete/.test(t.readyState)) &&
                    ((t.onload = t.onreadystatechange = null),
                    t.parentNode && t.parentNode.removeChild(t),
                    (t = null),
                    n || r(200, 'success'));
                }),
                n.insertBefore(t, n.firstChild);
            },
            abort: function() {
              t && t.onload(void 0, !0);
            }
          };
        }
      });
    var en = [],
      tn = /(=)\?(?=&|$)|\?\?/;
    f.ajaxSetup({
      jsonp: 'callback',
      jsonpCallback: function() {
        var e = en.pop() || f.expando + '_' + Et++;
        return (this[e] = !0), e;
      }
    }),
      f.ajaxPrefilter('json jsonp', function(t, n, i) {
        var o,
          r,
          a,
          s =
            !1 !== t.jsonp &&
            (tn.test(t.url)
              ? 'url'
              : 'string' == typeof t.data &&
                0 === (t.contentType || '').indexOf('application/x-www-form-urlencoded') &&
                tn.test(t.data) &&
                'data');
        return s || 'jsonp' === t.dataTypes[0]
          ? ((o = t.jsonpCallback = f.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback),
            s
              ? (t[s] = t[s].replace(tn, '$1' + o))
              : !1 !== t.jsonp && (t.url += (kt.test(t.url) ? '&' : '?') + t.jsonp + '=' + o),
            (t.converters['script json'] = function() {
              return a || f.error(o + ' was not called'), a[0];
            }),
            (t.dataTypes[0] = 'json'),
            (r = e[o]),
            (e[o] = function() {
              a = arguments;
            }),
            i.always(function() {
              void 0 === r ? f(e).removeProp(o) : (e[o] = r),
                t[o] && ((t.jsonpCallback = n.jsonpCallback), en.push(o)),
                a && f.isFunction(r) && r(a[0]),
                (a = r = void 0);
            }),
            'script')
          : void 0;
      }),
      (f.parseHTML = function(e, t, n) {
        if (!e || 'string' != typeof e) return null;
        'boolean' == typeof t && ((n = t), (t = !1)), (t = t || i);
        var o = T.exec(e),
          r = !n && [];
        return o
          ? [t.createElement(o[1])]
          : ((o = se([e], t, r)), r && r.length && f(r).remove(), f.merge([], o.childNodes));
      });
    var nn = f.fn.load;
    function on(e) {
      return f.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow);
    }
    (f.fn.load = function(e, t, n) {
      if ('string' != typeof e && nn) return nn.apply(this, arguments);
      var i,
        o,
        r,
        a = this,
        s = e.indexOf(' ');
      return (
        s > -1 && ((i = f.trim(e.slice(s, e.length))), (e = e.slice(0, s))),
        f.isFunction(t) ? ((n = t), (t = void 0)) : t && 'object' == typeof t && (o = 'POST'),
        a.length > 0 &&
          f
            .ajax({ url: e, type: o || 'GET', dataType: 'html', data: t })
            .done(function(e) {
              (r = arguments),
                a.html(
                  i
                    ? f('<div>')
                        .append(f.parseHTML(e))
                        .find(i)
                    : e
                );
            })
            .always(
              n &&
                function(e, t) {
                  a.each(function() {
                    n.apply(this, r || [e.responseText, t, e]);
                  });
                }
            ),
        this
      );
    }),
      f.each(['ajaxStart', 'ajaxStop', 'ajaxComplete', 'ajaxError', 'ajaxSuccess', 'ajaxSend'], function(e, t) {
        f.fn[t] = function(e) {
          return this.on(t, e);
        };
      }),
      (f.expr.filters.animated = function(e) {
        return f.grep(f.timers, function(t) {
          return e === t.elem;
        }).length;
      }),
      (f.offset = {
        setOffset: function(e, t, n) {
          var i,
            o,
            r,
            a,
            s,
            l,
            c = f.css(e, 'position'),
            u = f(e),
            d = {};
          'static' === c && (e.style.position = 'relative'),
            (s = u.offset()),
            (r = f.css(e, 'top')),
            (l = f.css(e, 'left')),
            ('absolute' === c || 'fixed' === c) && f.inArray('auto', [r, l]) > -1
              ? ((a = (i = u.position()).top), (o = i.left))
              : ((a = parseFloat(r) || 0), (o = parseFloat(l) || 0)),
            f.isFunction(t) && (t = t.call(e, n, f.extend({}, s))),
            null != t.top && (d.top = t.top - s.top + a),
            null != t.left && (d.left = t.left - s.left + o),
            'using' in t ? t.using.call(e, d) : u.css(d);
        }
      }),
      f.fn.extend({
        offset: function(e) {
          if (arguments.length)
            return void 0 === e
              ? this
              : this.each(function(t) {
                  f.offset.setOffset(this, e, t);
                });
          var t,
            n,
            i = { top: 0, left: 0 },
            o = this[0],
            r = o && o.ownerDocument;
          return r
            ? f.contains((t = r.documentElement), o)
              ? (void 0 !== o.getBoundingClientRect && (i = o.getBoundingClientRect()),
                (n = on(r)),
                {
                  top: i.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                  left: i.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
                })
              : i
            : void 0;
        },
        position: function() {
          if (this[0]) {
            var e,
              t,
              n = { top: 0, left: 0 },
              i = this[0];
            return (
              'fixed' === f.css(i, 'position')
                ? (t = i.getBoundingClientRect())
                : ((e = this.offsetParent()),
                  (t = this.offset()),
                  f.nodeName(e[0], 'html') || (n = e.offset()),
                  (n.top += f.css(e[0], 'borderTopWidth', !0)),
                  (n.left += f.css(e[0], 'borderLeftWidth', !0))),
              { top: t.top - n.top - f.css(i, 'marginTop', !0), left: t.left - n.left - f.css(i, 'marginLeft', !0) }
            );
          }
        },
        offsetParent: function() {
          return this.map(function() {
            for (var e = this.offsetParent; e && !f.nodeName(e, 'html') && 'static' === f.css(e, 'position'); )
              e = e.offsetParent;
            return e || Pe;
          });
        }
      }),
      f.each({ scrollLeft: 'pageXOffset', scrollTop: 'pageYOffset' }, function(e, t) {
        var n = /Y/.test(t);
        f.fn[e] = function(i) {
          return X(
            this,
            function(e, i, o) {
              var r = on(e);
              return void 0 === o
                ? r
                  ? t in r
                    ? r[t]
                    : r.document.documentElement[i]
                  : e[i]
                : void (r ? r.scrollTo(n ? f(r).scrollLeft() : o, n ? o : f(r).scrollTop()) : (e[i] = o));
            },
            e,
            i,
            arguments.length,
            null
          );
        };
      }),
      f.each(['top', 'left'], function(e, t) {
        f.cssHooks[t] = Be(d.pixelPosition, function(e, n) {
          return n ? ((n = He(e, t)), Fe.test(n) ? f(e).position()[t] + 'px' : n) : void 0;
        });
      }),
      f.each({ Height: 'height', Width: 'width' }, function(e, t) {
        f.each({ padding: 'inner' + e, content: t, '': 'outer' + e }, function(n, i) {
          f.fn[i] = function(i, o) {
            var r = arguments.length && (n || 'boolean' != typeof i),
              a = n || (!0 === i || !0 === o ? 'margin' : 'border');
            return X(
              this,
              function(t, n, i) {
                var o;
                return f.isWindow(t)
                  ? t.document.documentElement['client' + e]
                  : 9 === t.nodeType
                  ? ((o = t.documentElement),
                    Math.max(
                      t.body['scroll' + e],
                      o['scroll' + e],
                      t.body['offset' + e],
                      o['offset' + e],
                      o['client' + e]
                    ))
                  : void 0 === i
                  ? f.css(t, n, a)
                  : f.style(t, n, i, a);
              },
              t,
              r ? i : void 0,
              r,
              null
            );
          };
        });
      }),
      f.fn.extend({
        bind: function(e, t, n) {
          return this.on(e, null, t, n);
        },
        unbind: function(e, t) {
          return this.off(e, null, t);
        },
        delegate: function(e, t, n, i) {
          return this.on(t, e, n, i);
        },
        undelegate: function(e, t, n) {
          return 1 === arguments.length ? this.off(e, '**') : this.off(t, e || '**', n);
        }
      }),
      (f.fn.size = function() {
        return this.length;
      }),
      (f.fn.andSelf = f.fn.addBack),
      'function' == typeof define &&
        define.amd &&
        define('jquery', [], function() {
          return f;
        });
    var rn = e.jQuery,
      an = e.$;
    return (
      (f.noConflict = function(t) {
        return e.$ === f && (e.$ = an), t && e.jQuery === f && (e.jQuery = rn), f;
      }),
      t || (e.jQuery = e.$ = f),
      f
    );
  }),
  'undefined' == typeof jQuery)
)
  throw new Error("Bootstrap's JavaScript requires jQuery");
function hexToRgb(e) {
  e = e.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function(e, t, n, i) {
    return t + t + n + n + i + i;
  });
  var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);
  return t ? { r: parseInt(t[1], 16), g: parseInt(t[2], 16), b: parseInt(t[3], 16) } : null;
}
function clamp(e, t, n) {
  return Math.min(Math.max(e, t), n);
}
function isInArray(e, t) {
  return t.indexOf(e) > -1;
}
!(function(e) {
  'use strict';
  var t = jQuery.fn.jquery.split(' ')[0].split('.');
  if ((t[0] < 2 && t[1] < 9) || (1 == t[0] && 9 == t[1] && t[2] < 1) || t[0] > 2)
    throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3");
})(),
  (function(e) {
    'use strict';
    (e.fn.emulateTransitionEnd = function(t) {
      var n = !1,
        i = this;
      return (
        e(this).one('bsTransitionEnd', function() {
          n = !0;
        }),
        setTimeout(function() {
          n || e(i).trigger(e.support.transition.end);
        }, t),
        this
      );
    }),
      e(function() {
        (e.support.transition = (function() {
          var e = document.createElement('bootstrap'),
            t = {
              WebkitTransition: 'webkitTransitionEnd',
              MozTransition: 'transitionend',
              OTransition: 'oTransitionEnd otransitionend',
              transition: 'transitionend'
            };
          for (var n in t) if (void 0 !== e.style[n]) return { end: t[n] };
          return !1;
        })()),
          e.support.transition &&
            (e.event.special.bsTransitionEnd = {
              bindType: e.support.transition.end,
              delegateType: e.support.transition.end,
              handle: function(t) {
                return e(t.target).is(this) ? t.handleObj.handler.apply(this, arguments) : void 0;
              }
            });
      });
  })(jQuery),
  (function(e) {
    'use strict';
    var t = '[data-dismiss="alert"]',
      n = function(n) {
        e(n).on('click', t, this.close);
      };
    (n.VERSION = '3.3.6'),
      (n.TRANSITION_DURATION = 150),
      (n.prototype.close = function(t) {
        function i() {
          a.detach()
            .trigger('closed.bs.alert')
            .remove();
        }
        var o = e(this),
          r = o.attr('data-target');
        r || (r = (r = o.attr('href')) && r.replace(/.*(?=#[^\s]*$)/, ''));
        var a = e(r);
        t && t.preventDefault(),
          a.length || (a = o.closest('.alert')),
          a.trigger((t = e.Event('close.bs.alert'))),
          t.isDefaultPrevented() ||
            (a.removeClass('in'),
            e.support.transition && a.hasClass('fade')
              ? a.one('bsTransitionEnd', i).emulateTransitionEnd(n.TRANSITION_DURATION)
              : i());
      });
    var i = e.fn.alert;
    (e.fn.alert = function(t) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.alert');
        o || i.data('bs.alert', (o = new n(this))), 'string' == typeof t && o[t].call(i);
      });
    }),
      (e.fn.alert.Constructor = n),
      (e.fn.alert.noConflict = function() {
        return (e.fn.alert = i), this;
      }),
      e(document).on('click.bs.alert.data-api', t, n.prototype.close);
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.button');
        o || i.data('bs.button', (o = new n(this, 'object' == typeof t && t))),
          'toggle' == t ? o.toggle() : t && o.setState(t);
      });
    }
    var n = function(t, i) {
      (this.$element = e(t)), (this.options = e.extend({}, n.DEFAULTS, i)), (this.isLoading = !1);
    };
    (n.VERSION = '3.3.6'),
      (n.DEFAULTS = { loadingText: 'loading...' }),
      (n.prototype.setState = function(t) {
        var n = 'disabled',
          i = this.$element,
          o = i.is('input') ? 'val' : 'html',
          r = i.data();
        (t += 'Text'),
          null == r.resetText && i.data('resetText', i[o]()),
          setTimeout(
            e.proxy(function() {
              i[o](null == r[t] ? this.options[t] : r[t]),
                'loadingText' == t
                  ? ((this.isLoading = !0), i.addClass(n).attr(n, n))
                  : this.isLoading && ((this.isLoading = !1), i.removeClass(n).removeAttr(n));
            }, this),
            0
          );
      }),
      (n.prototype.toggle = function() {
        var e = !0,
          t = this.$element.closest('[data-toggle="buttons"]');
        if (t.length) {
          var n = this.$element.find('input');
          'radio' == n.prop('type')
            ? (n.prop('checked') && (e = !1), t.find('.active').removeClass('active'), this.$element.addClass('active'))
            : 'checkbox' == n.prop('type') &&
              (n.prop('checked') !== this.$element.hasClass('active') && (e = !1), this.$element.toggleClass('active')),
            n.prop('checked', this.$element.hasClass('active')),
            e && n.trigger('change');
        } else
          this.$element.attr('aria-pressed', !this.$element.hasClass('active')), this.$element.toggleClass('active');
      });
    var i = e.fn.button;
    (e.fn.button = t),
      (e.fn.button.Constructor = n),
      (e.fn.button.noConflict = function() {
        return (e.fn.button = i), this;
      }),
      e(document)
        .on('click.bs.button.data-api', '[data-toggle^="button"]', function(n) {
          var i = e(n.target);
          i.hasClass('btn') || (i = i.closest('.btn')),
            t.call(i, 'toggle'),
            e(n.target).is('input[type="radio"]') || e(n.target).is('input[type="checkbox"]') || n.preventDefault();
        })
        .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function(t) {
          e(t.target)
            .closest('.btn')
            .toggleClass('focus', /^focus(in)?$/.test(t.type));
        });
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.carousel'),
          r = e.extend({}, n.DEFAULTS, i.data(), 'object' == typeof t && t),
          a = 'string' == typeof t ? t : r.slide;
        o || i.data('bs.carousel', (o = new n(this, r))),
          'number' == typeof t ? o.to(t) : a ? o[a]() : r.interval && o.pause().cycle();
      });
    }
    var n = function(t, n) {
      (this.$element = e(t)),
        (this.$indicators = this.$element.find('.carousel-indicators')),
        (this.options = n),
        (this.paused = null),
        (this.sliding = null),
        (this.interval = null),
        (this.$active = null),
        (this.$items = null),
        this.options.keyboard && this.$element.on('keydown.bs.carousel', e.proxy(this.keydown, this)),
        'hover' == this.options.pause &&
          !('ontouchstart' in document.documentElement) &&
          this.$element
            .on('mouseenter.bs.carousel', e.proxy(this.pause, this))
            .on('mouseleave.bs.carousel', e.proxy(this.cycle, this));
    };
    (n.VERSION = '3.3.6'),
      (n.TRANSITION_DURATION = 600),
      (n.DEFAULTS = { interval: 5e3, pause: 'hover', wrap: !0, keyboard: !0 }),
      (n.prototype.keydown = function(e) {
        if (!/input|textarea/i.test(e.target.tagName)) {
          switch (e.which) {
            case 37:
              this.prev();
              break;
            case 39:
              this.next();
              break;
            default:
              return;
          }
          e.preventDefault();
        }
      }),
      (n.prototype.cycle = function(t) {
        return (
          t || (this.paused = !1),
          this.interval && clearInterval(this.interval),
          this.options.interval &&
            !this.paused &&
            (this.interval = setInterval(e.proxy(this.next, this), this.options.interval)),
          this
        );
      }),
      (n.prototype.getItemIndex = function(e) {
        return (this.$items = e.parent().children('.item')), this.$items.index(e || this.$active);
      }),
      (n.prototype.getItemForDirection = function(e, t) {
        var n = this.getItemIndex(t);
        return (('prev' == e && 0 === n) || ('next' == e && n == this.$items.length - 1)) && !this.options.wrap
          ? t
          : this.$items.eq((n + ('prev' == e ? -1 : 1)) % this.$items.length);
      }),
      (n.prototype.to = function(e) {
        var t = this,
          n = this.getItemIndex((this.$active = this.$element.find('.item.active')));
        return e > this.$items.length - 1 || 0 > e
          ? void 0
          : this.sliding
          ? this.$element.one('slid.bs.carousel', function() {
              t.to(e);
            })
          : n == e
          ? this.pause().cycle()
          : this.slide(e > n ? 'next' : 'prev', this.$items.eq(e));
      }),
      (n.prototype.pause = function(t) {
        return (
          t || (this.paused = !0),
          this.$element.find('.next, .prev').length &&
            e.support.transition &&
            (this.$element.trigger(e.support.transition.end), this.cycle(!0)),
          (this.interval = clearInterval(this.interval)),
          this
        );
      }),
      (n.prototype.next = function() {
        return this.sliding ? void 0 : this.slide('next');
      }),
      (n.prototype.prev = function() {
        return this.sliding ? void 0 : this.slide('prev');
      }),
      (n.prototype.slide = function(t, i) {
        var o = this.$element.find('.item.active'),
          r = i || this.getItemForDirection(t, o),
          a = this.interval,
          s = 'next' == t ? 'left' : 'right',
          l = this;
        if (r.hasClass('active')) return (this.sliding = !1);
        var c = r[0],
          u = e.Event('slide.bs.carousel', { relatedTarget: c, direction: s });
        if ((this.$element.trigger(u), !u.isDefaultPrevented())) {
          if (((this.sliding = !0), a && this.pause(), this.$indicators.length)) {
            this.$indicators.find('.active').removeClass('active');
            var d = e(this.$indicators.children()[this.getItemIndex(r)]);
            d && d.addClass('active');
          }
          var p = e.Event('slid.bs.carousel', { relatedTarget: c, direction: s });
          return (
            e.support.transition && this.$element.hasClass('slide')
              ? (r.addClass(t),
                o.addClass(s),
                r.addClass(s),
                o
                  .one('bsTransitionEnd', function() {
                    r.removeClass([t, s].join(' ')).addClass('active'),
                      o.removeClass(['active', s].join(' ')),
                      (l.sliding = !1),
                      setTimeout(function() {
                        l.$element.trigger(p);
                      }, 0);
                  })
                  .emulateTransitionEnd(n.TRANSITION_DURATION))
              : (o.removeClass('active'), r.addClass('active'), (this.sliding = !1), this.$element.trigger(p)),
            a && this.cycle(),
            this
          );
        }
      });
    var i = e.fn.carousel;
    (e.fn.carousel = t),
      (e.fn.carousel.Constructor = n),
      (e.fn.carousel.noConflict = function() {
        return (e.fn.carousel = i), this;
      });
    var o = function(n) {
      var i,
        o = e(this),
        r = e(o.attr('data-target') || ((i = o.attr('href')) && i.replace(/.*(?=#[^\s]+$)/, '')));
      if (r.hasClass('carousel')) {
        var a = e.extend({}, r.data(), o.data()),
          s = o.attr('data-slide-to');
        s && (a.interval = !1), t.call(r, a), s && r.data('bs.carousel').to(s), n.preventDefault();
      }
    };
    e(document)
      .on('click.bs.carousel.data-api', '[data-slide]', o)
      .on('click.bs.carousel.data-api', '[data-slide-to]', o),
      e(window).on('load', function() {
        e('[data-ride="carousel"]').each(function() {
          var n = e(this);
          t.call(n, n.data());
        });
      });
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      var n,
        i = t.attr('data-target') || ((n = t.attr('href')) && n.replace(/.*(?=#[^\s]+$)/, ''));
      return e(i);
    }
    function n(t) {
      return this.each(function() {
        var n = e(this),
          o = n.data('bs.collapse'),
          r = e.extend({}, i.DEFAULTS, n.data(), 'object' == typeof t && t);
        !o && r.toggle && /show|hide/.test(t) && (r.toggle = !1),
          o || n.data('bs.collapse', (o = new i(this, r))),
          'string' == typeof t && o[t]();
      });
    }
    var i = function(t, n) {
      (this.$element = e(t)),
        (this.options = e.extend({}, i.DEFAULTS, n)),
        (this.$trigger = e(
          '[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'
        )),
        (this.transitioning = null),
        this.options.parent
          ? (this.$parent = this.getParent())
          : this.addAriaAndCollapsedClass(this.$element, this.$trigger),
        this.options.toggle && this.toggle();
    };
    (i.VERSION = '3.3.6'),
      (i.TRANSITION_DURATION = 350),
      (i.DEFAULTS = { toggle: !0 }),
      (i.prototype.dimension = function() {
        return this.$element.hasClass('width') ? 'width' : 'height';
      }),
      (i.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass('in')) {
          var t,
            o = this.$parent && this.$parent.children('.panel').children('.in, .collapsing');
          if (!(o && o.length && ((t = o.data('bs.collapse')), t && t.transitioning))) {
            var r = e.Event('show.bs.collapse');
            if ((this.$element.trigger(r), !r.isDefaultPrevented())) {
              o && o.length && (n.call(o, 'hide'), t || o.data('bs.collapse', null));
              var a = this.dimension();
              this.$element
                .removeClass('collapse')
                .addClass('collapsing')
                [a](0)
                .attr('aria-expanded', !0),
                this.$trigger.removeClass('collapsed').attr('aria-expanded', !0),
                (this.transitioning = 1);
              var s = function() {
                this.$element
                  .removeClass('collapsing')
                  .addClass('collapse in')
                  [a](''),
                  (this.transitioning = 0),
                  this.$element.trigger('shown.bs.collapse');
              };
              if (!e.support.transition) return s.call(this);
              var l = e.camelCase(['scroll', a].join('-'));
              this.$element
                .one('bsTransitionEnd', e.proxy(s, this))
                .emulateTransitionEnd(i.TRANSITION_DURATION)
                [a](this.$element[0][l]);
            }
          }
        }
      }),
      (i.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass('in')) {
          var t = e.Event('hide.bs.collapse');
          if ((this.$element.trigger(t), !t.isDefaultPrevented())) {
            var n = this.dimension();
            this.$element[n](this.$element[n]()),
              this.$element
                .addClass('collapsing')
                .removeClass('collapse in')
                .attr('aria-expanded', !1),
              this.$trigger.addClass('collapsed').attr('aria-expanded', !1),
              (this.transitioning = 1);
            var o = function() {
              (this.transitioning = 0),
                this.$element
                  .removeClass('collapsing')
                  .addClass('collapse')
                  .trigger('hidden.bs.collapse');
            };
            return e.support.transition
              ? void this.$element[n](0)
                  .one('bsTransitionEnd', e.proxy(o, this))
                  .emulateTransitionEnd(i.TRANSITION_DURATION)
              : o.call(this);
          }
        }
      }),
      (i.prototype.toggle = function() {
        this[this.$element.hasClass('in') ? 'hide' : 'show']();
      }),
      (i.prototype.getParent = function() {
        return e(this.options.parent)
          .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
          .each(
            e.proxy(function(n, i) {
              var o = e(i);
              this.addAriaAndCollapsedClass(t(o), o);
            }, this)
          )
          .end();
      }),
      (i.prototype.addAriaAndCollapsedClass = function(e, t) {
        var n = e.hasClass('in');
        e.attr('aria-expanded', n), t.toggleClass('collapsed', !n).attr('aria-expanded', n);
      });
    var o = e.fn.collapse;
    (e.fn.collapse = n),
      (e.fn.collapse.Constructor = i),
      (e.fn.collapse.noConflict = function() {
        return (e.fn.collapse = o), this;
      }),
      e(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function(i) {
        var o = e(this);
        o.attr('data-target') || i.preventDefault();
        var r = t(o),
          a = r.data('bs.collapse') ? 'toggle' : o.data();
        n.call(r, a);
      });
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      var n = t.attr('data-target');
      n || (n = (n = t.attr('href')) && /#[A-Za-z]/.test(n) && n.replace(/.*(?=#[^\s]*$)/, ''));
      var i = n && e(n);
      return i && i.length ? i : t.parent();
    }
    function n(n) {
      (n && 3 === n.which) ||
        (e(i).remove(),
        e(o).each(function() {
          var i = e(this),
            o = t(i),
            r = { relatedTarget: this };
          o.hasClass('open') &&
            ((n && 'click' == n.type && /input|textarea/i.test(n.target.tagName) && e.contains(o[0], n.target)) ||
              (o.trigger((n = e.Event('hide.bs.dropdown', r))),
              n.isDefaultPrevented() ||
                (i.attr('aria-expanded', 'false'), o.removeClass('open').trigger(e.Event('hidden.bs.dropdown', r)))));
        }));
    }
    var i = '.dropdown-backdrop',
      o = '[data-toggle="dropdown"]',
      r = function(t) {
        e(t).on('click.bs.dropdown', this.toggle);
      };
    (r.VERSION = '3.3.6'),
      (r.prototype.toggle = function(i) {
        var o = e(this);
        if (!o.is('.disabled, :disabled')) {
          var r = t(o),
            a = r.hasClass('open');
          if ((n(), !a)) {
            'ontouchstart' in document.documentElement &&
              !r.closest('.navbar-nav').length &&
              e(document.createElement('div'))
                .addClass('dropdown-backdrop')
                .insertAfter(e(this))
                .on('click', n);
            var s = { relatedTarget: this };
            if ((r.trigger((i = e.Event('show.bs.dropdown', s))), i.isDefaultPrevented())) return;
            o.trigger('focus').attr('aria-expanded', 'true'),
              r.toggleClass('open').trigger(e.Event('shown.bs.dropdown', s));
          }
          return !1;
        }
      }),
      (r.prototype.keydown = function(n) {
        if (/(38|40|27|32)/.test(n.which) && !/input|textarea/i.test(n.target.tagName)) {
          var i = e(this);
          if ((n.preventDefault(), n.stopPropagation(), !i.is('.disabled, :disabled'))) {
            var r = t(i),
              a = r.hasClass('open');
            if ((!a && 27 != n.which) || (a && 27 == n.which))
              return 27 == n.which && r.find(o).trigger('focus'), i.trigger('click');
            var s = r.find('.dropdown-menu li:not(.disabled):visible a');
            if (s.length) {
              var l = s.index(n.target);
              38 == n.which && l > 0 && l--,
                40 == n.which && l < s.length - 1 && l++,
                ~l || (l = 0),
                s.eq(l).trigger('focus');
            }
          }
        }
      });
    var a = e.fn.dropdown;
    (e.fn.dropdown = function(t) {
      return this.each(function() {
        var n = e(this),
          i = n.data('bs.dropdown');
        i || n.data('bs.dropdown', (i = new r(this))), 'string' == typeof t && i[t].call(n);
      });
    }),
      (e.fn.dropdown.Constructor = r),
      (e.fn.dropdown.noConflict = function() {
        return (e.fn.dropdown = a), this;
      }),
      e(document)
        .on('click.bs.dropdown.data-api', n)
        .on('click.bs.dropdown.data-api', '.dropdown form', function(e) {
          e.stopPropagation();
        })
        .on('click.bs.dropdown.data-api', o, r.prototype.toggle)
        .on('keydown.bs.dropdown.data-api', o, r.prototype.keydown)
        .on('keydown.bs.dropdown.data-api', '.dropdown-menu', r.prototype.keydown);
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t, i) {
      return this.each(function() {
        var o = e(this),
          r = o.data('bs.modal'),
          a = e.extend({}, n.DEFAULTS, o.data(), 'object' == typeof t && t);
        r || o.data('bs.modal', (r = new n(this, a))), 'string' == typeof t ? r[t](i) : a.show && r.show(i);
      });
    }
    var n = function(t, n) {
      (this.options = n),
        (this.$body = e(document.body)),
        (this.$element = e(t)),
        (this.$dialog = this.$element.find('.modal-dialog')),
        (this.$backdrop = null),
        (this.isShown = null),
        (this.originalBodyPad = null),
        (this.scrollbarWidth = 0),
        (this.ignoreBackdropClick = !1),
        this.options.remote &&
          this.$element.find('.modal-content').load(
            this.options.remote,
            e.proxy(function() {
              this.$element.trigger('loaded.bs.modal');
            }, this)
          );
    };
    (n.VERSION = '3.3.6'),
      (n.TRANSITION_DURATION = 300),
      (n.BACKDROP_TRANSITION_DURATION = 150),
      (n.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }),
      (n.prototype.toggle = function(e) {
        return this.isShown ? this.hide() : this.show(e);
      }),
      (n.prototype.show = function(t) {
        var i = this,
          o = e.Event('show.bs.modal', { relatedTarget: t });
        this.$element.trigger(o),
          this.isShown ||
            o.isDefaultPrevented() ||
            ((this.isShown = !0),
            this.checkScrollbar(),
            this.setScrollbar(),
            this.$body.addClass('modal-open'),
            this.escape(),
            this.resize(),
            this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', e.proxy(this.hide, this)),
            this.$dialog.on('mousedown.dismiss.bs.modal', function() {
              i.$element.one('mouseup.dismiss.bs.modal', function(t) {
                e(t.target).is(i.$element) && (i.ignoreBackdropClick = !0);
              });
            }),
            this.backdrop(function() {
              var o = e.support.transition && i.$element.hasClass('fade');
              i.$element.parent().length || i.$element.appendTo(i.$body),
                i.$element.show().scrollTop(0),
                i.adjustDialog(),
                i.$element.addClass('in'),
                i.enforceFocus();
              var r = e.Event('shown.bs.modal', { relatedTarget: t });
              o
                ? i.$dialog
                    .one('bsTransitionEnd', function() {
                      i.$element.trigger('focus').trigger(r);
                    })
                    .emulateTransitionEnd(n.TRANSITION_DURATION)
                : i.$element.trigger('focus').trigger(r);
            }));
      }),
      (n.prototype.hide = function(t) {
        t && t.preventDefault(),
          (t = e.Event('hide.bs.modal')),
          this.$element.trigger(t),
          this.isShown &&
            !t.isDefaultPrevented() &&
            ((this.isShown = !1),
            this.escape(),
            this.resize(),
            e(document).off('focusin.bs.modal'),
            this.$element
              .removeClass('in')
              .off('click.dismiss.bs.modal')
              .off('mouseup.dismiss.bs.modal'),
            this.$dialog.off('mousedown.dismiss.bs.modal'),
            e.support.transition && this.$element.hasClass('fade')
              ? this.$element
                  .one('bsTransitionEnd', e.proxy(this.hideModal, this))
                  .emulateTransitionEnd(n.TRANSITION_DURATION)
              : this.hideModal());
      }),
      (n.prototype.enforceFocus = function() {
        e(document)
          .off('focusin.bs.modal')
          .on(
            'focusin.bs.modal',
            e.proxy(function(e) {
              this.$element[0] === e.target || this.$element.has(e.target).length || this.$element.trigger('focus');
            }, this)
          );
      }),
      (n.prototype.escape = function() {
        this.isShown && this.options.keyboard
          ? this.$element.on(
              'keydown.dismiss.bs.modal',
              e.proxy(function(e) {
                27 == e.which && this.hide();
              }, this)
            )
          : this.isShown || this.$element.off('keydown.dismiss.bs.modal');
      }),
      (n.prototype.resize = function() {
        this.isShown
          ? e(window).on('resize.bs.modal', e.proxy(this.handleUpdate, this))
          : e(window).off('resize.bs.modal');
      }),
      (n.prototype.hideModal = function() {
        var e = this;
        this.$element.hide(),
          this.backdrop(function() {
            e.$body.removeClass('modal-open'),
              e.resetAdjustments(),
              e.resetScrollbar(),
              e.$element.trigger('hidden.bs.modal');
          });
      }),
      (n.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), (this.$backdrop = null);
      }),
      (n.prototype.backdrop = function(t) {
        var i = this,
          o = this.$element.hasClass('fade') ? 'fade' : '';
        if (this.isShown && this.options.backdrop) {
          var r = e.support.transition && o;
          if (
            ((this.$backdrop = e(document.createElement('div'))
              .addClass('modal-backdrop ' + o)
              .appendTo(this.$body)),
            this.$element.on(
              'click.dismiss.bs.modal',
              e.proxy(function(e) {
                return this.ignoreBackdropClick
                  ? void (this.ignoreBackdropClick = !1)
                  : void (
                      e.target === e.currentTarget &&
                      ('static' == this.options.backdrop ? this.$element[0].focus() : this.hide())
                    );
              }, this)
            ),
            this.$backdrop.addClass('in'),
            !t)
          )
            return;
          r ? this.$backdrop.one('bsTransitionEnd', t).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : t();
        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass('in');
          var a = function() {
            i.removeBackdrop(), t && t();
          };
          e.support.transition && this.$element.hasClass('fade')
            ? this.$backdrop.one('bsTransitionEnd', a).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION)
            : a();
        } else t && t();
      }),
      (n.prototype.handleUpdate = function() {
        this.adjustDialog();
      }),
      (n.prototype.adjustDialog = function() {
        var e = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
          paddingLeft: !this.bodyIsOverflowing && e ? this.scrollbarWidth : '',
          paddingRight: this.bodyIsOverflowing && !e ? this.scrollbarWidth : ''
        });
      }),
      (n.prototype.resetAdjustments = function() {
        this.$element.css({ paddingLeft: '', paddingRight: '' });
      }),
      (n.prototype.checkScrollbar = function() {
        var e = window.innerWidth;
        if (!e) {
          var t = document.documentElement.getBoundingClientRect();
          e = t.right - Math.abs(t.left);
        }
        (this.bodyIsOverflowing = document.body.clientWidth < e), (this.scrollbarWidth = this.measureScrollbar());
      }),
      (n.prototype.setScrollbar = function() {
        var e = parseInt(this.$body.css('padding-right') || 0, 10);
        (this.originalBodyPad = document.body.style.paddingRight || ''),
          this.bodyIsOverflowing && this.$body.css('padding-right', e + this.scrollbarWidth);
      }),
      (n.prototype.resetScrollbar = function() {
        this.$body.css('padding-right', this.originalBodyPad);
      }),
      (n.prototype.measureScrollbar = function() {
        var e = document.createElement('div');
        (e.className = 'modal-scrollbar-measure'), this.$body.append(e);
        var t = e.offsetWidth - e.clientWidth;
        return this.$body[0].removeChild(e), t;
      });
    var i = e.fn.modal;
    (e.fn.modal = t),
      (e.fn.modal.Constructor = n),
      (e.fn.modal.noConflict = function() {
        return (e.fn.modal = i), this;
      }),
      e(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function(n) {
        var i = e(this),
          o = i.attr('href'),
          r = e(i.attr('data-target') || (o && o.replace(/.*(?=#[^\s]+$)/, ''))),
          a = r.data('bs.modal') ? 'toggle' : e.extend({ remote: !/#/.test(o) && o }, r.data(), i.data());
        i.is('a') && n.preventDefault(),
          r.one('show.bs.modal', function(e) {
            e.isDefaultPrevented() ||
              r.one('hidden.bs.modal', function() {
                i.is(':visible') && i.trigger('focus');
              });
          }),
          t.call(r, a, this);
      });
  })(jQuery),
  (function(e) {
    'use strict';
    var t = function(e, t) {
      (this.type = null),
        (this.options = null),
        (this.enabled = null),
        (this.timeout = null),
        (this.hoverState = null),
        (this.$element = null),
        (this.inState = null),
        this.init('tooltip', e, t);
    };
    (t.VERSION = '3.3.6'),
      (t.TRANSITION_DURATION = 150),
      (t.DEFAULTS = {
        animation: !0,
        placement: 'top',
        selector: !1,
        template:
          '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: 'hover focus',
        title: '',
        delay: 0,
        html: !1,
        container: !1,
        viewport: { selector: 'body', padding: 0 }
      }),
      (t.prototype.init = function(t, n, i) {
        if (
          ((this.enabled = !0),
          (this.type = t),
          (this.$element = e(n)),
          (this.options = this.getOptions(i)),
          (this.$viewport =
            this.options.viewport &&
            e(
              e.isFunction(this.options.viewport)
                ? this.options.viewport.call(this, this.$element)
                : this.options.viewport.selector || this.options.viewport
            )),
          (this.inState = { click: !1, hover: !1, focus: !1 }),
          this.$element[0] instanceof document.constructor && !this.options.selector)
        )
          throw new Error(
            '`selector` option must be specified when initializing ' + this.type + ' on the window.document object!'
          );
        for (var o = this.options.trigger.split(' '), r = o.length; r--; ) {
          var a = o[r];
          if ('click' == a) this.$element.on('click.' + this.type, this.options.selector, e.proxy(this.toggle, this));
          else if ('manual' != a) {
            var s = 'hover' == a ? 'mouseleave' : 'focusout';
            this.$element.on(
              ('hover' == a ? 'mouseenter' : 'focusin') + '.' + this.type,
              this.options.selector,
              e.proxy(this.enter, this)
            ),
              this.$element.on(s + '.' + this.type, this.options.selector, e.proxy(this.leave, this));
          }
        }
        this.options.selector
          ? (this._options = e.extend({}, this.options, { trigger: 'manual', selector: '' }))
          : this.fixTitle();
      }),
      (t.prototype.getDefaults = function() {
        return t.DEFAULTS;
      }),
      (t.prototype.getOptions = function(t) {
        return (
          (t = e.extend({}, this.getDefaults(), this.$element.data(), t)).delay &&
            'number' == typeof t.delay &&
            (t.delay = { show: t.delay, hide: t.delay }),
          t
        );
      }),
      (t.prototype.getDelegateOptions = function() {
        var t = {},
          n = this.getDefaults();
        return (
          this._options &&
            e.each(this._options, function(e, i) {
              n[e] != i && (t[e] = i);
            }),
          t
        );
      }),
      (t.prototype.enter = function(t) {
        var n = t instanceof this.constructor ? t : e(t.currentTarget).data('bs.' + this.type);
        return (
          n ||
            ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())),
            e(t.currentTarget).data('bs.' + this.type, n)),
          t instanceof e.Event && (n.inState['focusin' == t.type ? 'focus' : 'hover'] = !0),
          n.tip().hasClass('in') || 'in' == n.hoverState
            ? void (n.hoverState = 'in')
            : (clearTimeout(n.timeout),
              (n.hoverState = 'in'),
              n.options.delay && n.options.delay.show
                ? void (n.timeout = setTimeout(function() {
                    'in' == n.hoverState && n.show();
                  }, n.options.delay.show))
                : n.show())
        );
      }),
      (t.prototype.isInStateTrue = function() {
        for (var e in this.inState) if (this.inState[e]) return !0;
        return !1;
      }),
      (t.prototype.leave = function(t) {
        var n = t instanceof this.constructor ? t : e(t.currentTarget).data('bs.' + this.type);
        return (
          n ||
            ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())),
            e(t.currentTarget).data('bs.' + this.type, n)),
          t instanceof e.Event && (n.inState['focusout' == t.type ? 'focus' : 'hover'] = !1),
          n.isInStateTrue()
            ? void 0
            : (clearTimeout(n.timeout),
              (n.hoverState = 'out'),
              n.options.delay && n.options.delay.hide
                ? void (n.timeout = setTimeout(function() {
                    'out' == n.hoverState && n.hide();
                  }, n.options.delay.hide))
                : n.hide())
        );
      }),
      (t.prototype.show = function() {
        var n = e.Event('show.bs.' + this.type);
        if (this.hasContent() && this.enabled) {
          this.$element.trigger(n);
          var i = e.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
          if (n.isDefaultPrevented() || !i) return;
          var o = this,
            r = this.tip(),
            a = this.getUID(this.type);
          this.setContent(),
            r.attr('id', a),
            this.$element.attr('aria-describedby', a),
            this.options.animation && r.addClass('fade');
          var s =
              'function' == typeof this.options.placement
                ? this.options.placement.call(this, r[0], this.$element[0])
                : this.options.placement,
            l = /\s?auto?\s?/i,
            c = l.test(s);
          c && (s = s.replace(l, '') || 'top'),
            r
              .detach()
              .css({ top: 0, left: 0, display: 'block' })
              .addClass(s)
              .data('bs.' + this.type, this),
            this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element),
            this.$element.trigger('inserted.bs.' + this.type);
          var u = this.getPosition(),
            d = r[0].offsetWidth,
            p = r[0].offsetHeight;
          if (c) {
            var f = s,
              h = this.getPosition(this.$viewport);
            (s =
              'bottom' == s && u.bottom + p > h.bottom
                ? 'top'
                : 'top' == s && u.top - p < h.top
                ? 'bottom'
                : 'right' == s && u.right + d > h.width
                ? 'left'
                : 'left' == s && u.left - d < h.left
                ? 'right'
                : s),
              r.removeClass(f).addClass(s);
          }
          var m = this.getCalculatedOffset(s, u, d, p);
          this.applyPlacement(m, s);
          var v = function() {
            var e = o.hoverState;
            o.$element.trigger('shown.bs.' + o.type), (o.hoverState = null), 'out' == e && o.leave(o);
          };
          e.support.transition && this.$tip.hasClass('fade')
            ? r.one('bsTransitionEnd', v).emulateTransitionEnd(t.TRANSITION_DURATION)
            : v();
        }
      }),
      (t.prototype.applyPlacement = function(t, n) {
        var i = this.tip(),
          o = i[0].offsetWidth,
          r = i[0].offsetHeight,
          a = parseInt(i.css('margin-top'), 10),
          s = parseInt(i.css('margin-left'), 10);
        isNaN(a) && (a = 0),
          isNaN(s) && (s = 0),
          (t.top += a),
          (t.left += s),
          e.offset.setOffset(
            i[0],
            e.extend(
              {
                using: function(e) {
                  i.css({ top: Math.round(e.top), left: Math.round(e.left) });
                }
              },
              t
            ),
            0
          ),
          i.addClass('in');
        var l = i[0].offsetWidth,
          c = i[0].offsetHeight;
        'top' == n && c != r && (t.top = t.top + r - c);
        var u = this.getViewportAdjustedDelta(n, t, l, c);
        u.left ? (t.left += u.left) : (t.top += u.top);
        var d = /top|bottom/.test(n),
          p = d ? 2 * u.left - o + l : 2 * u.top - r + c,
          f = d ? 'offsetWidth' : 'offsetHeight';
        i.offset(t), this.replaceArrow(p, i[0][f], d);
      }),
      (t.prototype.replaceArrow = function(e, t, n) {
        this.arrow()
          .css(n ? 'left' : 'top', 50 * (1 - e / t) + '%')
          .css(n ? 'top' : 'left', '');
      }),
      (t.prototype.setContent = function() {
        var e = this.tip(),
          t = this.getTitle();
        e.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](t),
          e.removeClass('fade in top bottom left right');
      }),
      (t.prototype.hide = function(n) {
        function i() {
          'in' != o.hoverState && r.detach(),
            o.$element.removeAttr('aria-describedby').trigger('hidden.bs.' + o.type),
            n && n();
        }
        var o = this,
          r = e(this.$tip),
          a = e.Event('hide.bs.' + this.type);
        return (
          this.$element.trigger(a),
          a.isDefaultPrevented()
            ? void 0
            : (r.removeClass('in'),
              e.support.transition && r.hasClass('fade')
                ? r.one('bsTransitionEnd', i).emulateTransitionEnd(t.TRANSITION_DURATION)
                : i(),
              (this.hoverState = null),
              this)
        );
      }),
      (t.prototype.fixTitle = function() {
        var e = this.$element;
        (e.attr('title') || 'string' != typeof e.attr('data-original-title')) &&
          e.attr('data-original-title', e.attr('title') || '').attr('title', '');
      }),
      (t.prototype.hasContent = function() {
        return this.getTitle();
      }),
      (t.prototype.getPosition = function(t) {
        var n = (t = t || this.$element)[0],
          i = 'BODY' == n.tagName,
          o = n.getBoundingClientRect();
        null == o.width && (o = e.extend({}, o, { width: o.right - o.left, height: o.bottom - o.top }));
        var r = i ? { top: 0, left: 0 } : t.offset(),
          a = { scroll: i ? document.documentElement.scrollTop || document.body.scrollTop : t.scrollTop() },
          s = i ? { width: e(window).width(), height: e(window).height() } : null;
        return e.extend({}, o, a, s, r);
      }),
      (t.prototype.getCalculatedOffset = function(e, t, n, i) {
        return 'bottom' == e
          ? { top: t.top + t.height, left: t.left + t.width / 2 - n / 2 }
          : 'top' == e
          ? { top: t.top - i, left: t.left + t.width / 2 - n / 2 }
          : 'left' == e
          ? { top: t.top + t.height / 2 - i / 2, left: t.left - n }
          : { top: t.top + t.height / 2 - i / 2, left: t.left + t.width };
      }),
      (t.prototype.getViewportAdjustedDelta = function(e, t, n, i) {
        var o = { top: 0, left: 0 };
        if (!this.$viewport) return o;
        var r = (this.options.viewport && this.options.viewport.padding) || 0,
          a = this.getPosition(this.$viewport);
        if (/right|left/.test(e)) {
          var s = t.top - r - a.scroll,
            l = t.top + r - a.scroll + i;
          s < a.top ? (o.top = a.top - s) : l > a.top + a.height && (o.top = a.top + a.height - l);
        } else {
          var c = t.left - r,
            u = t.left + r + n;
          c < a.left ? (o.left = a.left - c) : u > a.right && (o.left = a.left + a.width - u);
        }
        return o;
      }),
      (t.prototype.getTitle = function() {
        var e = this.$element,
          t = this.options;
        return e.attr('data-original-title') || ('function' == typeof t.title ? t.title.call(e[0]) : t.title);
      }),
      (t.prototype.getUID = function(e) {
        do {
          e += ~~(1e6 * Math.random());
        } while (document.getElementById(e));
        return e;
      }),
      (t.prototype.tip = function() {
        if (!this.$tip && ((this.$tip = e(this.options.template)), 1 != this.$tip.length))
          throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!');
        return this.$tip;
      }),
      (t.prototype.arrow = function() {
        return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'));
      }),
      (t.prototype.enable = function() {
        this.enabled = !0;
      }),
      (t.prototype.disable = function() {
        this.enabled = !1;
      }),
      (t.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled;
      }),
      (t.prototype.toggle = function(t) {
        var n = this;
        t &&
          ((n = e(t.currentTarget).data('bs.' + this.type)) ||
            ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())),
            e(t.currentTarget).data('bs.' + this.type, n))),
          t
            ? ((n.inState.click = !n.inState.click), n.isInStateTrue() ? n.enter(n) : n.leave(n))
            : n.tip().hasClass('in')
            ? n.leave(n)
            : n.enter(n);
      }),
      (t.prototype.destroy = function() {
        var e = this;
        clearTimeout(this.timeout),
          this.hide(function() {
            e.$element.off('.' + e.type).removeData('bs.' + e.type),
              e.$tip && e.$tip.detach(),
              (e.$tip = null),
              (e.$arrow = null),
              (e.$viewport = null);
          });
      });
    var n = e.fn.tooltip;
    (e.fn.tooltip = function(n) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.tooltip'),
          r = 'object' == typeof n && n;
        (o || !/destroy|hide/.test(n)) &&
          (o || i.data('bs.tooltip', (o = new t(this, r))), 'string' == typeof n && o[n]());
      });
    }),
      (e.fn.tooltip.Constructor = t),
      (e.fn.tooltip.noConflict = function() {
        return (e.fn.tooltip = n), this;
      });
  })(jQuery),
  (function(e) {
    'use strict';
    var t = function(e, t) {
      this.init('popover', e, t);
    };
    if (!e.fn.tooltip) throw new Error('Popover requires tooltip.js');
    (t.VERSION = '3.3.6'),
      (t.DEFAULTS = e.extend({}, e.fn.tooltip.Constructor.DEFAULTS, {
        placement: 'right',
        trigger: 'click',
        content: '',
        template:
          '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
      })),
      ((t.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype)).constructor = t),
      (t.prototype.getDefaults = function() {
        return t.DEFAULTS;
      }),
      (t.prototype.setContent = function() {
        var e = this.tip(),
          t = this.getTitle(),
          n = this.getContent();
        e.find('.popover-title')[this.options.html ? 'html' : 'text'](t),
          e
            .find('.popover-content')
            .children()
            .detach()
            .end()
            [this.options.html ? ('string' == typeof n ? 'html' : 'append') : 'text'](n),
          e.removeClass('fade top bottom left right in'),
          e.find('.popover-title').html() || e.find('.popover-title').hide();
      }),
      (t.prototype.hasContent = function() {
        return this.getTitle() || this.getContent();
      }),
      (t.prototype.getContent = function() {
        var e = this.$element,
          t = this.options;
        return e.attr('data-content') || ('function' == typeof t.content ? t.content.call(e[0]) : t.content);
      }),
      (t.prototype.arrow = function() {
        return (this.$arrow = this.$arrow || this.tip().find('.arrow'));
      });
    var n = e.fn.popover;
    (e.fn.popover = function(n) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.popover'),
          r = 'object' == typeof n && n;
        (o || !/destroy|hide/.test(n)) &&
          (o || i.data('bs.popover', (o = new t(this, r))), 'string' == typeof n && o[n]());
      });
    }),
      (e.fn.popover.Constructor = t),
      (e.fn.popover.noConflict = function() {
        return (e.fn.popover = n), this;
      });
  })(jQuery),
  (function(e) {
    'use strict';
    function t(n, i) {
      (this.$body = e(document.body)),
        (this.$scrollElement = e(e(n).is(document.body) ? window : n)),
        (this.options = e.extend({}, t.DEFAULTS, i)),
        (this.selector = (this.options.target || '') + ' .nav li > a'),
        (this.offsets = []),
        (this.targets = []),
        (this.activeTarget = null),
        (this.scrollHeight = 0),
        this.$scrollElement.on('scroll.bs.scrollspy', e.proxy(this.process, this)),
        this.refresh(),
        this.process();
    }
    function n(n) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.scrollspy');
        o || i.data('bs.scrollspy', (o = new t(this, 'object' == typeof n && n))), 'string' == typeof n && o[n]();
      });
    }
    (t.VERSION = '3.3.6'),
      (t.DEFAULTS = { offset: 10 }),
      (t.prototype.getScrollHeight = function() {
        return (
          this.$scrollElement[0].scrollHeight ||
          Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
        );
      }),
      (t.prototype.refresh = function() {
        var t = this,
          n = 'offset',
          i = 0;
        (this.offsets = []),
          (this.targets = []),
          (this.scrollHeight = this.getScrollHeight()),
          e.isWindow(this.$scrollElement[0]) || ((n = 'position'), (i = this.$scrollElement.scrollTop())),
          this.$body
            .find(this.selector)
            .map(function() {
              var t = e(this),
                o = t.data('target') || t.attr('href'),
                r = /^#./.test(o) && e(o);
              return (r && r.length && r.is(':visible') && [[r[n]().top + i, o]]) || null;
            })
            .sort(function(e, t) {
              return e[0] - t[0];
            })
            .each(function() {
              t.offsets.push(this[0]), t.targets.push(this[1]);
            });
      }),
      (t.prototype.process = function() {
        var e,
          t = this.$scrollElement.scrollTop() + this.options.offset,
          n = this.getScrollHeight(),
          i = this.options.offset + n - this.$scrollElement.height(),
          o = this.offsets,
          r = this.targets,
          a = this.activeTarget;
        if ((this.scrollHeight != n && this.refresh(), t >= i)) return a != (e = r[r.length - 1]) && this.activate(e);
        if (a && t < o[0]) return (this.activeTarget = null), this.clear();
        for (e = o.length; e--; )
          a != r[e] && t >= o[e] && (void 0 === o[e + 1] || t < o[e + 1]) && this.activate(r[e]);
      }),
      (t.prototype.activate = function(t) {
        (this.activeTarget = t), this.clear();
        var n = e(this.selector + '[data-target="' + t + '"],' + this.selector + '[href="' + t + '"]')
          .parents('li')
          .addClass('active');
        n.parent('.dropdown-menu').length && (n = n.closest('li.dropdown').addClass('active')),
          n.trigger('activate.bs.scrollspy');
      }),
      (t.prototype.clear = function() {
        e(this.selector)
          .parentsUntil(this.options.target, '.active')
          .removeClass('active');
      });
    var i = e.fn.scrollspy;
    (e.fn.scrollspy = n),
      (e.fn.scrollspy.Constructor = t),
      (e.fn.scrollspy.noConflict = function() {
        return (e.fn.scrollspy = i), this;
      }),
      e(window).on('load.bs.scrollspy.data-api', function() {
        e('[data-spy="scroll"]').each(function() {
          var t = e(this);
          n.call(t, t.data());
        });
      });
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.tab');
        o || i.data('bs.tab', (o = new n(this))), 'string' == typeof t && o[t]();
      });
    }
    var n = function(t) {
      this.element = e(t);
    };
    (n.VERSION = '3.3.6'),
      (n.TRANSITION_DURATION = 150),
      (n.prototype.show = function() {
        var t = this.element,
          n = t.closest('ul:not(.dropdown-menu)'),
          i = t.data('target');
        if ((i || (i = (i = t.attr('href')) && i.replace(/.*(?=#[^\s]*$)/, '')), !t.parent('li').hasClass('active'))) {
          var o = n.find('.active:last a'),
            r = e.Event('hide.bs.tab', { relatedTarget: t[0] }),
            a = e.Event('show.bs.tab', { relatedTarget: o[0] });
          if ((o.trigger(r), t.trigger(a), !a.isDefaultPrevented() && !r.isDefaultPrevented())) {
            var s = e(i);
            this.activate(t.closest('li'), n),
              this.activate(s, s.parent(), function() {
                o.trigger({ type: 'hidden.bs.tab', relatedTarget: t[0] }),
                  t.trigger({ type: 'shown.bs.tab', relatedTarget: o[0] });
              });
          }
        }
      }),
      (n.prototype.activate = function(t, i, o) {
        function r() {
          a
            .removeClass('active')
            .find('> .dropdown-menu > .active')
            .removeClass('active')
            .end()
            .find('[data-toggle="tab"]')
            .attr('aria-expanded', !1),
            t
              .addClass('active')
              .find('[data-toggle="tab"]')
              .attr('aria-expanded', !0),
            s ? t.addClass('in') : t.removeClass('fade'),
            t.parent('.dropdown-menu').length &&
              t
                .closest('li.dropdown')
                .addClass('active')
                .end()
                .find('[data-toggle="tab"]')
                .attr('aria-expanded', !0),
            o && o();
        }
        var a = i.find('> .active'),
          s = o && e.support.transition && ((a.length && a.hasClass('fade')) || !!i.find('> .fade').length);
        a.length && s ? a.one('bsTransitionEnd', r).emulateTransitionEnd(n.TRANSITION_DURATION) : r(),
          a.removeClass('in');
      });
    var i = e.fn.tab;
    (e.fn.tab = t),
      (e.fn.tab.Constructor = n),
      (e.fn.tab.noConflict = function() {
        return (e.fn.tab = i), this;
      });
    var o = function(n) {
      n.preventDefault(), t.call(e(this), 'show');
    };
    e(document)
      .on('click.bs.tab.data-api', '[data-toggle="tab"]', o)
      .on('click.bs.tab.data-api', '[data-toggle="pill"]', o);
  })(jQuery),
  (function(e) {
    'use strict';
    function t(t) {
      return this.each(function() {
        var i = e(this),
          o = i.data('bs.affix');
        o || i.data('bs.affix', (o = new n(this, 'object' == typeof t && t))), 'string' == typeof t && o[t]();
      });
    }
    var n = function(t, i) {
      (this.options = e.extend({}, n.DEFAULTS, i)),
        (this.$target = e(this.options.target)
          .on('scroll.bs.affix.data-api', e.proxy(this.checkPosition, this))
          .on('click.bs.affix.data-api', e.proxy(this.checkPositionWithEventLoop, this))),
        (this.$element = e(t)),
        (this.affixed = null),
        (this.unpin = null),
        (this.pinnedOffset = null),
        this.checkPosition();
    };
    (n.VERSION = '3.3.6'),
      (n.RESET = 'affix affix-top affix-bottom'),
      (n.DEFAULTS = { offset: 0, target: window }),
      (n.prototype.getState = function(e, t, n, i) {
        var o = this.$target.scrollTop(),
          r = this.$element.offset(),
          a = this.$target.height();
        if (null != n && 'top' == this.affixed) return n > o && 'top';
        if ('bottom' == this.affixed)
          return null != n ? !(o + this.unpin <= r.top) && 'bottom' : !(e - i >= o + a) && 'bottom';
        var s = null == this.affixed;
        return null != n && n >= o ? 'top' : null != i && (s ? o : r.top) + (s ? a : t) >= e - i && 'bottom';
      }),
      (n.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(n.RESET).addClass('affix');
        var e = this.$target.scrollTop(),
          t = this.$element.offset();
        return (this.pinnedOffset = t.top - e);
      }),
      (n.prototype.checkPositionWithEventLoop = function() {
        setTimeout(e.proxy(this.checkPosition, this), 1);
      }),
      (n.prototype.checkPosition = function() {
        if (this.$element.is(':visible')) {
          var t = this.$element.height(),
            i = this.options.offset,
            o = i.top,
            r = i.bottom,
            a = Math.max(e(document).height(), e(document.body).height());
          'object' != typeof i && (r = o = i),
            'function' == typeof o && (o = i.top(this.$element)),
            'function' == typeof r && (r = i.bottom(this.$element));
          var s = this.getState(a, t, o, r);
          if (this.affixed != s) {
            null != this.unpin && this.$element.css('top', '');
            var l = 'affix' + (s ? '-' + s : ''),
              c = e.Event(l + '.bs.affix');
            if ((this.$element.trigger(c), c.isDefaultPrevented())) return;
            (this.affixed = s),
              (this.unpin = 'bottom' == s ? this.getPinnedOffset() : null),
              this.$element
                .removeClass(n.RESET)
                .addClass(l)
                .trigger(l.replace('affix', 'affixed') + '.bs.affix');
          }
          'bottom' == s && this.$element.offset({ top: a - t - r });
        }
      });
    var i = e.fn.affix;
    (e.fn.affix = t),
      (e.fn.affix.Constructor = n),
      (e.fn.affix.noConflict = function() {
        return (e.fn.affix = i), this;
      }),
      e(window).on('load', function() {
        e('[data-spy="affix"]').each(function() {
          var n = e(this),
            i = n.data();
          (i.offset = i.offset || {}),
            null != i.offsetBottom && (i.offset.bottom = i.offsetBottom),
            null != i.offsetTop && (i.offset.top = i.offsetTop),
            t.call(n, i);
        });
      });
  })(jQuery),
  (function(e, t) {
    'function' == typeof define && define.amd
      ? define(t)
      : 'object' == typeof exports
      ? (module.exports = t(require, exports, module))
      : (e.ScrollReveal = t());
  })(this, function(e, t, n) {
    return (
      function() {
        'use strict';
        var e, t, n;
        (this.ScrollReveal = (function() {
          function i(n) {
            return void 0 === this || Object.getPrototypeOf(this) !== i.prototype
              ? new i(n)
              : (((e = this).tools = new t()),
                e.isSupported()
                  ? (e.tools.extend(e.defaults, n || {}),
                    o(e.defaults),
                    (e.store = { elements: {}, containers: [] }),
                    (e.sequences = {}),
                    (e.history = []),
                    (e.uid = 0),
                    (e.initialized = !1))
                  : 'undefined' != typeof console && console,
                e);
          }
          function o(t) {
            var n = t.container;
            return n && 'string' == typeof n
              ? (t.container = window.document.querySelector(n))
              : (n && !e.tools.isNode(n) && (t.container = null),
                null == n && (t.container = window.document.documentElement),
                t.container);
          }
          function r() {
            return ++e.uid;
          }
          function a(t, n) {
            (t.config = e.tools.extendClone(t.config ? t.config : e.defaults, n)),
              (t.config.axis = 'top' === t.config.origin || 'bottom' === t.config.origin ? 'Y' : 'X'),
              ('top' !== t.config.origin && 'left' !== t.config.origin) ||
                (t.config.distance = '-' + t.config.distance);
          }
          function s(e) {
            var t = window.getComputedStyle(e.domEl);
            e.styles ||
              ((e.styles = { transition: {}, transform: {}, computed: {} }),
              (e.styles.inline = e.domEl.getAttribute('style') || ''),
              (e.styles.inline += '; visibility: visible; '),
              (e.styles.computed.opacity = t.opacity),
              (e.styles.computed.transition =
                t.transition && 'all 0s ease 0s' != t.transition ? t.transition + ', ' : '')),
              (e.styles.transition.instant = l(e, 0)),
              (e.styles.transition.delayed = l(e, e.config.delay)),
              (e.styles.transform.initial = ' -webkit-transform:'),
              (e.styles.transform.target = ' -webkit-transform:'),
              c(e),
              (e.styles.transform.initial += 'transform:'),
              (e.styles.transform.target += 'transform:'),
              c(e);
          }
          function l(e, t) {
            var n = e.config;
            return (
              '-webkit-transition: ' +
              e.styles.computed.transition +
              '-webkit-transform ' +
              n.duration / 1e3 +
              's ' +
              n.easing +
              ' ' +
              t / 1e3 +
              's, opacity ' +
              n.duration / 1e3 +
              's ' +
              n.easing +
              ' ' +
              t / 1e3 +
              's; transition: ' +
              e.styles.computed.transition +
              'transform ' +
              n.duration / 1e3 +
              's ' +
              n.easing +
              ' ' +
              t / 1e3 +
              's, opacity ' +
              n.duration / 1e3 +
              's ' +
              n.easing +
              ' ' +
              t / 1e3 +
              's; '
            );
          }
          function c(e) {
            var t = e.config,
              n = e.styles.transform;
            parseInt(t.distance) &&
              ((n.initial += ' translate' + t.axis + '(' + t.distance + ')'),
              (n.target += ' translate' + t.axis + '(0)')),
              t.scale && ((n.initial += ' scale(' + t.scale + ')'), (n.target += ' scale(1)')),
              t.rotate.x && ((n.initial += ' rotateX(' + t.rotate.x + 'deg)'), (n.target += ' rotateX(0)')),
              t.rotate.y && ((n.initial += ' rotateY(' + t.rotate.y + 'deg)'), (n.target += ' rotateY(0)')),
              t.rotate.z && ((n.initial += ' rotateZ(' + t.rotate.z + 'deg)'), (n.target += ' rotateZ(0)')),
              (n.initial += '; opacity: ' + t.opacity + ';'),
              (n.target += '; opacity: ' + e.styles.computed.opacity + ';');
          }
          function u(t) {
            var n = t.config.container;
            n && -1 == e.store.containers.indexOf(n) && e.store.containers.push(t.config.container),
              (e.store.elements[t.id] = t);
          }
          function d() {
            if (e.isSupported()) {
              f();
              for (var t = 0; t < e.store.containers.length; t++)
                e.store.containers[t].addEventListener('scroll', p),
                  e.store.containers[t].addEventListener('resize', p);
              e.initialized ||
                (window.addEventListener('scroll', p), window.addEventListener('resize', p), (e.initialized = !0));
            }
            return e;
          }
          function p() {
            n(f);
          }
          function f() {
            var t, n;
            (function() {
              var t, n;
              e.tools.forOwn(e.sequences, function(i) {
                (n = e.sequences[i]), (t = !1);
                for (var o = 0; o < n.elemIds.length; o++) v(e.store.elements[n.elemIds[o]]) && !t && (t = !0);
                n.active = t;
              });
            })(),
              e.tools.forOwn(e.store.elements, function(i) {
                (t = (function(t) {
                  var n = t.config.useDelay;
                  return 'always' === n || ('onload' === n && !e.initialized) || ('once' === n && !t.seen);
                })((n = e.store.elements[i]))),
                  (function(t) {
                    if (t.sequence) {
                      var n = e.sequences[t.sequence.id];
                      return n.active && !n.blocked && !t.revealing && !t.disabled;
                    }
                    return v(t) && !t.revealing && !t.disabled;
                  })(n)
                    ? (n.domEl.setAttribute(
                        'style',
                        t
                          ? n.styles.inline + n.styles.transform.target + n.styles.transition.delayed
                          : n.styles.inline + n.styles.transform.target + n.styles.transition.instant
                      ),
                      h('reveal', n, t),
                      (n.revealing = !0),
                      (n.seen = !0),
                      n.sequence &&
                        (function(t, n) {
                          var i = 0,
                            o = 0,
                            r = e.sequences[t.sequence.id];
                          (r.blocked = !0),
                            n && 'onload' == t.config.useDelay && (o = t.config.delay),
                            t.sequence.timer &&
                              ((i = Math.abs(t.sequence.timer.started - new Date())),
                              window.clearTimeout(t.sequence.timer)),
                            (t.sequence.timer = { started: new Date() }),
                            (t.sequence.timer.clock = window.setTimeout(function() {
                              (r.blocked = !1), (t.sequence.timer = null), p();
                            }, Math.abs(r.interval) + o - i));
                        })(n, t))
                    : (function(t) {
                        return t.sequence
                          ? !e.sequences[t.sequence.id].active && t.config.reset && t.revealing && !t.disabled
                          : !v(t) && t.config.reset && t.revealing && !t.disabled;
                      })(n) &&
                      (n.domEl.setAttribute(
                        'style',
                        n.styles.inline + n.styles.transform.initial + n.styles.transition.instant
                      ),
                      h('reset', n),
                      (n.revealing = !1));
              });
          }
          function h(e, t, n) {
            var i = 0,
              o = 0,
              r = 'after';
            switch (e) {
              case 'reveal':
                (o = t.config.duration), n && (o += t.config.delay), (r += 'Reveal');
                break;
              case 'reset':
                (o = t.config.duration), (r += 'Reset');
            }
            t.timer && ((i = Math.abs(t.timer.started - new Date())), window.clearTimeout(t.timer.clock)),
              (t.timer = { started: new Date() }),
              (t.timer.clock = window.setTimeout(function() {
                t.config[r](t.domEl), (t.timer = null);
              }, o - i));
          }
          function m(e) {
            var t = 0,
              n = 0,
              i = e.offsetHeight,
              o = e.offsetWidth;
            do {
              isNaN(e.offsetTop) || (t += e.offsetTop), isNaN(e.offsetLeft) || (n += e.offsetLeft);
            } while ((e = e.offsetParent));
            return { top: t, left: n, height: i, width: o };
          }
          function v(e) {
            var t = m(e.domEl),
              n = (function(e) {
                return { width: e.clientWidth, height: e.clientHeight };
              })(e.config.container),
              i = (function(e) {
                if (e && e !== window.document.documentElement) {
                  var t = m(e);
                  return { x: e.scrollLeft + t.left, y: e.scrollTop + t.top };
                }
                return { x: window.pageXOffset, y: window.pageYOffset };
              })(e.config.container),
              o = e.config.viewFactor,
              r = t.height,
              a = t.width,
              s = t.top,
              l = t.left;
            return (
              (i.y - e.config.viewOffset.bottom + n.height > s + r * o &&
                s + r - r * o > i.y + e.config.viewOffset.top &&
                l + a * o > i.x + e.config.viewOffset.left &&
                i.x - e.config.viewOffset.right + n.width > l + a - a * o) ||
              'fixed' === window.getComputedStyle(e.domEl).position
            );
          }
          return (
            (i.prototype.defaults = {
              origin: 'bottom',
              distance: '20px',
              duration: 500,
              delay: 0,
              rotate: { x: 0, y: 0, z: 0 },
              opacity: 0,
              scale: 0.9,
              easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
              container: null,
              mobile: !0,
              reset: !1,
              useDelay: 'always',
              viewFactor: 0.2,
              viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
              afterReveal: function(e) {},
              afterReset: function(e) {}
            }),
            (i.prototype.isSupported = function() {
              var e = document.documentElement.style;
              return ('WebkitTransition' in e && 'WebkitTransform' in e) || ('transition' in e && 'transform' in e);
            }),
            (i.prototype.reveal = function(t, n, i, l) {
              var c, p, f, h, m, v;
              if (
                ((c = n && n.container ? o(n) : e.defaults.container),
                !(p = e.tools.isNode(t) ? [t] : Array.prototype.slice.call(c.querySelectorAll(t))).length)
              )
                return e;
              n && 'number' == typeof n && ((i = n), (n = {})),
                i &&
                  'number' == typeof i &&
                  ((v = r()), (m = e.sequences[v] = { id: v, interval: i, elemIds: [], active: !1 }));
              for (var g = 0; g < p.length; g++)
                (h = p[g].getAttribute('data-sr-id'))
                  ? (f = e.store.elements[h])
                  : (f = { id: r(), domEl: p[g], seen: !1, revealing: !1 }).domEl.setAttribute('data-sr-id', f.id),
                  m && ((f.sequence = { id: m.id, index: m.elemIds.length }), m.elemIds.push(f.id)),
                  a(f, n || {}),
                  s(f),
                  u(f),
                  (e.tools.isMobile() && !f.config.mobile) || !e.isSupported()
                    ? (f.domEl.setAttribute('style', f.styles.inline), (f.disabled = !0))
                    : f.revealing || f.domEl.setAttribute('style', f.styles.inline + f.styles.transform.initial);
              return (
                !l &&
                  e.isSupported() &&
                  ((function(t, n, i) {
                    e.history.push({ selector: t, config: n, interval: void 0 });
                  })(t, n),
                  e.initTimeout && window.clearTimeout(e.initTimeout),
                  (e.initTimeout = window.setTimeout(d, 0))),
                e
              );
            }),
            (i.prototype.sync = function() {
              if (e.history.length && e.isSupported()) {
                for (var t = 0; t < e.history.length; t++) {
                  var n = e.history[t];
                  e.reveal(n.selector, n.config, n.interval, !0);
                }
                d();
              }
              return e;
            }),
            i
          );
        })()),
          (t = (function() {
            function e() {}
            return (
              (e.prototype.isObject = function(e) {
                return null !== e && 'object' == typeof e && e.constructor == Object;
              }),
              (e.prototype.isNode = function(e) {
                return 'object' == typeof Node
                  ? e instanceof Node
                  : e && 'object' == typeof e && 'number' == typeof e.nodeType && 'string' == typeof e.nodeName;
              }),
              (e.prototype.forOwn = function(e, t) {
                if (!this.isObject(e)) throw new TypeError('Expected "object", but received "' + typeof e + '".');
                for (var n in e) e.hasOwnProperty(n) && t(n);
              }),
              (e.prototype.extend = function(e, t) {
                return (
                  this.forOwn(
                    t,
                    function(n) {
                      this.isObject(t[n])
                        ? ((e[n] && this.isObject(e[n])) || (e[n] = {}), this.extend(e[n], t[n]))
                        : (e[n] = t[n]);
                    }.bind(this)
                  ),
                  e
                );
              }),
              (e.prototype.extendClone = function(e, t) {
                return this.extend(this.extend({}, e), t);
              }),
              (e.prototype.isMobile = function() {
                return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
              }),
              e
            );
          })()),
          (n = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame);
      }.call(this),
      this.ScrollReveal
    );
  }),
  (function(e, t, n, i) {
    'use strict';
    var o = e('#contact-form'),
      r = e('#message-contact'),
      a = e('#message-contact .message-text');
    o.submit(function(t) {
      o.find('.form-group').removeClass('error'), r.removeClass('error');
      var n = '',
        i = {
          name: e('input[name="form-name"]').val(),
          email: e('input[name="form-email"]').val(),
          message: e('textarea[name="form-message"]').val(),
          textfield: e('input[name="text-field"]').val()
        };
      e
        .ajax({ type: 'POST', url: 'php/contact-process.php', data: i, dataType: 'json', encode: !0 })
        .done(function(t) {
          t.success
            ? (a.html(t.confirmation),
              e('#contact-form .form-control').each(function() {
                e(this)
                  .fadeIn()
                  .val(e(this).attr('placeholder'));
              }))
            : (t.errors.name && (e('#name-field').addClass('error'), (n = t.errors.name)),
              t.errors.email && (e('#email-field').addClass('error'), (n = n + ' ' + t.errors.email)),
              t.errors.message && (e('#message-field').addClass('error'), (n = n + ' ' + t.errors.message)),
              r.addClass('error'),
              a.html(n)),
            r.slideDown('slow', 'swing'),
            setTimeout(function() {
              r.slideUp('slow', 'swing');
            }, 5e3);
        })
        .fail(function(e) {
          console.log(e);
        }),
        t.preventDefault();
    });
  })(jQuery, window, document),
  (function(e, t, n, i) {
    'use strict';
    var o = e('#newsletter-form'),
      r = e('#message-newsletter'),
      a = e('#message-newsletter .message-text');
    o.submit(function(t) {
      o.find('.input-group').removeClass('error'), r.removeClass('error');
      var n = '',
        i = { email: e('input[name="email"]').val(), 'textfield-nl': e('input[name="text-field-nl"]').val() };
      e
        .ajax({ type: 'POST', url: 'php/newsletter-process.php', data: i, dataType: 'json', encode: !0 })
        .done(function(e) {
          e.success
            ? (a.html(e.confirmation),
              o
                .find('.form-control')
                .fadeIn()
                .val(''))
            : (e.errors.email &&
                (o.find('.input-group').addClass('error'), r.addClass('error'), (n = n + ' ' + e.errors.email)),
              a.html(n)),
            r.slideDown('slow', 'swing'),
            setTimeout(function() {
              r.slideUp('slow', 'swing');
            }, 1e4);
        })
        .fail(function(e) {
          console.log(e);
        }),
        t.preventDefault();
    });
  })(jQuery, window, document),
  (function(e) {
    'use strict';
    (e.ajaxChimp = {
      responses: {
        'We have sent you a confirmation email': 0,
        'Please enter a value': 1,
        'An email address must contain a single @': 2,
        'The domain portion of the email address is invalid (the portion after the @: )': 3,
        'The username portion of the email address is invalid (the portion before the @: )': 4,
        'This email address looks fake or invalid. Please enter a real email address': 5
      },
      translations: { en: null },
      init: function(t, n) {
        e(t).ajaxChimp(n);
      }
    }),
      (e.fn.ajaxChimp = function(t) {
        return (
          e(this).each(function(n, i) {
            var o = e(i),
              r = o.find('input[type=email]'),
              a = o.find('label[for=' + r.attr('id') + ']'),
              s = e.extend({ url: o.attr('action'), language: 'en' }, t),
              l = s.url.replace('/post?', '/post-json?').concat('&c=?');
            o.attr('novalidate', 'true'),
              r.attr('name', 'EMAIL'),
              o.submit(function() {
                var t,
                  n = {},
                  i = o.serializeArray();
                e.each(i, function(e, t) {
                  n[t.name] = t.value;
                }),
                  e.ajax({
                    url: l,
                    data: n,
                    success: function(n) {
                      if ('success' === n.result)
                        (t = 'We have sent you a confirmation email'),
                          a.removeClass('error').addClass('valid'),
                          r.removeClass('error').addClass('valid');
                      else {
                        r.removeClass('valid').addClass('error'), a.removeClass('valid').addClass('error');
                        try {
                          var i = n.msg.split(' - ', 2);
                          t = void 0 === i[1] ? n.msg : parseInt(i[0], 10).toString() === i[0] ? i[1] : n.msg;
                        } catch (o) {
                          t = n.msg;
                        }
                      }
                      'en' !== s.language &&
                        void 0 !== e.ajaxChimp.responses[t] &&
                        e.ajaxChimp.translations &&
                        e.ajaxChimp.translations[s.language] &&
                        e.ajaxChimp.translations[s.language][e.ajaxChimp.responses[t]] &&
                        (t = e.ajaxChimp.translations[s.language][e.ajaxChimp.responses[t]]),
                        a.html(t),
                        a.show(2e3),
                        s.callback && s.callback(n);
                    },
                    dataType: 'jsonp',
                    error: function(e, t) {
                      console.log('mailchimp ajax submit error: ' + t);
                    }
                  });
                var c = 'Submitting...';
                return (
                  'en' !== s.language &&
                    e.ajaxChimp.translations &&
                    e.ajaxChimp.translations[s.language] &&
                    e.ajaxChimp.translations[s.language].submit &&
                    (c = e.ajaxChimp.translations[s.language].submit),
                  a.html(c).show(2e3),
                  !1
                );
              });
          }),
          this
        );
      });
  })(jQuery),
  (function(e, t) {
    'function' == typeof define && define.amd
      ? define(t)
      : 'object' == typeof exports
      ? (module.exports = t())
      : (e.PhotoSwipe = t());
  })(this, function() {
    'use strict';
    return function(e, t, n, i) {
      var o = {
        features: null,
        bind: function(e, t, n, i) {
          var o = (i ? 'remove' : 'add') + 'EventListener';
          t = t.split(' ');
          for (var r = 0; r < t.length; r++) t[r] && e[o](t[r], n, !1);
        },
        isArray: function(e) {
          return e instanceof Array;
        },
        createEl: function(e, t) {
          var n = document.createElement(t || 'div');
          return e && (n.className = e), n;
        },
        getScrollY: function() {
          var e = window.pageYOffset;
          return void 0 !== e ? e : document.documentElement.scrollTop;
        },
        unbind: function(e, t, n) {
          o.bind(e, t, n, !0);
        },
        removeClass: function(e, t) {
          var n = new RegExp('(\\s|^)' + t + '(\\s|$)');
          e.className = e.className
            .replace(n, ' ')
            .replace(/^\s\s*/, '')
            .replace(/\s\s*$/, '');
        },
        addClass: function(e, t) {
          o.hasClass(e, t) || (e.className += (e.className ? ' ' : '') + t);
        },
        hasClass: function(e, t) {
          return e.className && new RegExp('(^|\\s)' + t + '(\\s|$)').test(e.className);
        },
        getChildByClass: function(e, t) {
          for (var n = e.firstChild; n; ) {
            if (o.hasClass(n, t)) return n;
            n = n.nextSibling;
          }
        },
        arraySearch: function(e, t, n) {
          for (var i = e.length; i--; ) if (e[i][n] === t) return i;
          return -1;
        },
        extend: function(e, t, n) {
          for (var i in t)
            if (t.hasOwnProperty(i)) {
              if (n && e.hasOwnProperty(i)) continue;
              e[i] = t[i];
            }
        },
        easing: {
          sine: {
            out: function(e) {
              return Math.sin(e * (Math.PI / 2));
            },
            inOut: function(e) {
              return -(Math.cos(Math.PI * e) - 1) / 2;
            }
          },
          cubic: {
            out: function(e) {
              return --e * e * e + 1;
            }
          }
        },
        detectFeatures: function() {
          if (o.features) return o.features;
          var e = o.createEl().style,
            t = '',
            n = {};
          if (
            ((n.oldIE = document.all && !document.addEventListener),
            (n.touch = 'ontouchstart' in window),
            window.requestAnimationFrame &&
              ((n.raf = window.requestAnimationFrame), (n.caf = window.cancelAnimationFrame)),
            (n.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled),
            !n.pointerEvent)
          ) {
            var i = navigator.userAgent;
            if (/iP(hone|od)/.test(navigator.platform)) {
              var r = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
              r && r.length > 0 && (r = parseInt(r[1], 10)) >= 1 && 8 > r && (n.isOldIOSPhone = !0);
            }
            var a = i.match(/Android\s([0-9\.]*)/),
              s = a ? a[1] : 0;
            (s = parseFloat(s)) >= 1 && (4.4 > s && (n.isOldAndroid = !0), (n.androidVersion = s)),
              (n.isMobileOpera = /opera mini|opera mobi/i.test(i));
          }
          for (
            var l, c, u = ['transform', 'perspective', 'animationName'], d = ['', 'webkit', 'Moz', 'ms', 'O'], p = 0;
            4 > p;
            p++
          ) {
            t = d[p];
            for (var f = 0; 3 > f; f++)
              (l = u[f]), (c = t + (t ? l.charAt(0).toUpperCase() + l.slice(1) : l)), !n[l] && c in e && (n[l] = c);
            t &&
              !n.raf &&
              ((t = t.toLowerCase()),
              (n.raf = window[t + 'RequestAnimationFrame']),
              n.raf && (n.caf = window[t + 'CancelAnimationFrame'] || window[t + 'CancelRequestAnimationFrame']));
          }
          if (!n.raf) {
            var h = 0;
            (n.raf = function(e) {
              var t = new Date().getTime(),
                n = Math.max(0, 16 - (t - h)),
                i = window.setTimeout(function() {
                  e(t + n);
                }, n);
              return (h = t + n), i;
            }),
              (n.caf = function(e) {
                clearTimeout(e);
              });
          }
          return (
            (n.svg =
              !!document.createElementNS &&
              !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect),
            (o.features = n),
            n
          );
        }
      };
      o.detectFeatures(),
        o.features.oldIE &&
          (o.bind = function(e, t, n, i) {
            t = t.split(' ');
            for (
              var o,
                r = (i ? 'detach' : 'attach') + 'Event',
                a = function() {
                  n.handleEvent.call(n);
                },
                s = 0;
              s < t.length;
              s++
            )
              if ((o = t[s]))
                if ('object' == typeof n && n.handleEvent) {
                  if (i) {
                    if (!n['oldIE' + o]) return !1;
                  } else n['oldIE' + o] = a;
                  e[r]('on' + o, n['oldIE' + o]);
                } else e[r]('on' + o, n);
          });
      var r = this,
        a = {
          allowPanToNext: !0,
          spacing: 0.12,
          bgOpacity: 1,
          mouseUsed: !1,
          loop: !0,
          pinchToClose: !0,
          closeOnScroll: !0,
          closeOnVerticalDrag: !0,
          verticalDragRange: 0.75,
          hideAnimationDuration: 333,
          showAnimationDuration: 333,
          showHideOpacity: !1,
          focus: !0,
          escKey: !0,
          arrowKeys: !0,
          mainScrollEndFriction: 0.35,
          panEndFriction: 0.35,
          isClickableElement: function(e) {
            return 'A' === e.tagName;
          },
          getDoubleTapZoom: function(e, t) {
            return e || t.initialZoomLevel < 0.7 ? 1 : 1.33;
          },
          maxSpreadZoom: 1.33,
          modal: !0,
          scaleMode: 'fit'
        };
      o.extend(a, i);
      var s,
        l,
        c,
        u,
        d,
        p,
        f,
        h,
        m,
        v,
        g,
        y,
        b,
        x,
        w,
        C,
        T,
        S,
        E,
        k,
        _,
        A,
        D,
        I,
        N,
        $,
        O,
        R,
        M,
        L,
        F,
        j,
        P,
        q,
        H,
        z,
        B,
        W,
        U,
        Z,
        V,
        X,
        K,
        Y,
        G,
        J,
        Q,
        ee,
        te,
        ne,
        ie,
        oe,
        re,
        ae,
        se,
        le = { x: 0, y: 0 },
        ce = { x: 0, y: 0 },
        ue = { x: 0, y: 0 },
        de = {},
        pe = 0,
        fe = {},
        he = { x: 0, y: 0 },
        me = 0,
        ve = !0,
        ge = [],
        ye = {},
        be = !1,
        xe = function(e, t) {
          o.extend(r, t.publicMethods), ge.push(e);
        },
        we = function(e) {
          var t = qt();
          return e > t - 1 ? e - t : 0 > e ? t + e : e;
        },
        Ce = {},
        Te = function(e, t) {
          return Ce[e] || (Ce[e] = []), Ce[e].push(t);
        },
        Se = function(e) {
          var t = Ce[e];
          if (t) {
            var n = Array.prototype.slice.call(arguments);
            n.shift();
            for (var i = 0; i < t.length; i++) t[i].apply(r, n);
          }
        },
        Ee = function() {
          return new Date().getTime();
        },
        ke = function(e) {
          (re = e), (r.bg.style.opacity = e * a.bgOpacity);
        },
        _e = function(e, t, n, i, o) {
          (!be || (o && o !== r.currItem)) && (i /= o ? o.fitRatio : r.currItem.fitRatio),
            (e[A] = y + t + 'px, ' + n + 'px' + b + ' scale(' + i + ')');
        },
        Ae = function(e) {
          ee &&
            (e &&
              (v > r.currItem.fitRatio ? be || (Kt(r.currItem, !1, !0), (be = !0)) : be && (Kt(r.currItem), (be = !1))),
            _e(ee, ue.x, ue.y, v));
        },
        De = function(e) {
          e.container && _e(e.container.style, e.initialPosition.x, e.initialPosition.y, e.initialZoomLevel, e);
        },
        Ie = function(e, t) {
          t[A] = y + e + 'px, 0px' + b;
        },
        Ne = function(e, t) {
          if (!a.loop && t) {
            var n = u + (he.x * pe - e) / he.x,
              i = Math.round(e - ct.x);
            ((0 > n && i > 0) || (n >= qt() - 1 && 0 > i)) && (e = ct.x + i * a.mainScrollEndFriction);
          }
          (ct.x = e), Ie(e, d);
        },
        $e = function(e, t) {
          var n = ut[e] - fe[e];
          return ce[e] + le[e] + n - n * (t / g);
        },
        Oe = function(e, t) {
          (e.x = t.x), (e.y = t.y), t.id && (e.id = t.id);
        },
        Re = function(e) {
          (e.x = Math.round(e.x)), (e.y = Math.round(e.y));
        },
        Me = null,
        Le = function() {
          Me &&
            (o.unbind(document, 'mousemove', Le),
            o.addClass(e, 'pswp--has_mouse'),
            (a.mouseUsed = !0),
            Se('mouseUsed')),
            (Me = setTimeout(function() {
              Me = null;
            }, 100));
        },
        Fe = function(e, t) {
          var n = Ut(r.currItem, de, e);
          return t && (Q = n), n;
        },
        je = function(e) {
          return e || (e = r.currItem), e.initialZoomLevel;
        },
        Pe = function(e) {
          return e || (e = r.currItem), e.w > 0 ? a.maxSpreadZoom : 1;
        },
        qe = function(e, t, n, i) {
          return i === r.currItem.initialZoomLevel
            ? ((n[e] = r.currItem.initialPosition[e]), !0)
            : ((n[e] = $e(e, i)),
              n[e] > t.min[e] ? ((n[e] = t.min[e]), !0) : n[e] < t.max[e] && ((n[e] = t.max[e]), !0));
        },
        He = function(e) {
          var t = '';
          a.escKey && 27 === e.keyCode
            ? (t = 'close')
            : a.arrowKeys && (37 === e.keyCode ? (t = 'prev') : 39 === e.keyCode && (t = 'next')),
            t &&
              (e.ctrlKey ||
                e.altKey ||
                e.shiftKey ||
                e.metaKey ||
                (e.preventDefault ? e.preventDefault() : (e.returnValue = !1), r[t]()));
        },
        ze = function(e) {
          e && (V || Z || te || z) && (e.preventDefault(), e.stopPropagation());
        },
        Be = function() {
          r.setScrollOffset(0, o.getScrollY());
        },
        We = {},
        Ue = 0,
        Ze = function(e) {
          We[e] && (We[e].raf && $(We[e].raf), Ue--, delete We[e]);
        },
        Ve = function(e) {
          We[e] && Ze(e), We[e] || (Ue++, (We[e] = {}));
        },
        Xe = function() {
          for (var e in We) We.hasOwnProperty(e) && Ze(e);
        },
        Ke = function(e, t, n, i, o, r, a) {
          var s,
            l = Ee();
          Ve(e);
          var c = function() {
            if (We[e]) {
              if ((s = Ee() - l) >= i) return Ze(e), r(n), void (a && a());
              r((n - t) * o(s / i) + t), (We[e].raf = N(c));
            }
          };
          c();
        },
        Ye = {
          shout: Se,
          listen: Te,
          viewportSize: de,
          options: a,
          isMainScrollAnimating: function() {
            return te;
          },
          getZoomLevel: function() {
            return v;
          },
          getCurrentIndex: function() {
            return u;
          },
          isDragging: function() {
            return W;
          },
          isZooming: function() {
            return G;
          },
          setScrollOffset: function(e, t) {
            (fe.x = e), (L = fe.y = t), Se('updateScrollOffset', fe);
          },
          applyZoomPan: function(e, t, n, i) {
            (ue.x = t), (ue.y = n), (v = e), Ae(i);
          },
          init: function() {
            if (!s && !l) {
              var n;
              for (
                r.framework = o,
                  r.template = e,
                  r.bg = o.getChildByClass(e, 'pswp__bg'),
                  O = e.className,
                  s = !0,
                  F = o.detectFeatures(),
                  N = F.raf,
                  $ = F.caf,
                  A = F.transform,
                  M = F.oldIE,
                  r.scrollWrap = o.getChildByClass(e, 'pswp__scroll-wrap'),
                  r.container = o.getChildByClass(r.scrollWrap, 'pswp__container'),
                  d = r.container.style,
                  r.itemHolders = C = [
                    { el: r.container.children[0], wrap: 0, index: -1 },
                    { el: r.container.children[1], wrap: 0, index: -1 },
                    { el: r.container.children[2], wrap: 0, index: -1 }
                  ],
                  C[0].el.style.display = C[2].el.style.display = 'none',
                  (function() {
                    if (A)
                      return (
                        (y = 'translate' + (F.perspective && !I ? '3d(' : '(')),
                        void (b = F.perspective ? ', 0px)' : ')')
                      );
                    (A = 'left'),
                      o.addClass(e, 'pswp--ie'),
                      (Ie = function(e, t) {
                        t.left = e + 'px';
                      }),
                      (De = function(e) {
                        var t = e.fitRatio > 1 ? 1 : e.fitRatio,
                          n = e.container.style,
                          i = t * e.h;
                        (n.width = t * e.w + 'px'),
                          (n.height = i + 'px'),
                          (n.left = e.initialPosition.x + 'px'),
                          (n.top = e.initialPosition.y + 'px');
                      }),
                      (Ae = function() {
                        if (ee) {
                          var e = ee,
                            t = r.currItem,
                            n = t.fitRatio > 1 ? 1 : t.fitRatio,
                            i = n * t.h;
                          (e.width = n * t.w + 'px'),
                            (e.height = i + 'px'),
                            (e.left = ue.x + 'px'),
                            (e.top = ue.y + 'px');
                        }
                      });
                  })(),
                  m = { resize: r.updateSize, scroll: Be, keydown: He, click: ze },
                  (F.animationName && F.transform && !(F.isOldIOSPhone || F.isOldAndroid || F.isMobileOpera)) ||
                    (a.showAnimationDuration = a.hideAnimationDuration = 0),
                  n = 0;
                n < ge.length;
                n++
              )
                r['init' + ge[n]]();
              t && (r.ui = new t(r, o)).init(),
                Se('firstUpdate'),
                (u = u || a.index || 0),
                (isNaN(u) || 0 > u || u >= qt()) && (u = 0),
                (r.currItem = Pt(u)),
                (F.isOldIOSPhone || F.isOldAndroid) && (ve = !1),
                e.setAttribute('aria-hidden', 'false'),
                a.modal &&
                  (ve
                    ? (e.style.position = 'fixed')
                    : ((e.style.position = 'absolute'), (e.style.top = o.getScrollY() + 'px'))),
                void 0 === L && (Se('initialLayout'), (L = R = o.getScrollY()));
              var i = 'pswp--open ';
              for (
                a.mainClass && (i += a.mainClass + ' '),
                  a.showHideOpacity && (i += 'pswp--animate_opacity '),
                  i += I ? 'pswp--touch' : 'pswp--notouch',
                  i += F.animationName ? ' pswp--css_animation' : '',
                  o.addClass(e, (i += F.svg ? ' pswp--svg' : '')),
                  r.updateSize(),
                  p = -1,
                  me = null,
                  n = 0;
                3 > n;
                n++
              )
                Ie((n + p) * he.x, C[n].el.style);
              M || o.bind(r.scrollWrap, h, r),
                Te('initialZoomInEnd', function() {
                  r.setContent(C[0], u - 1),
                    r.setContent(C[2], u + 1),
                    (C[0].el.style.display = C[2].el.style.display = 'block'),
                    a.focus && e.focus(),
                    o.bind(document, 'keydown', r),
                    F.transform && o.bind(r.scrollWrap, 'click', r),
                    a.mouseUsed || o.bind(document, 'mousemove', Le),
                    o.bind(window, 'resize scroll', r),
                    Se('bindEvents');
                }),
                r.setContent(C[1], u),
                r.updateCurrItem(),
                Se('afterInit'),
                ve ||
                  (x = setInterval(function() {
                    Ue || W || G || v !== r.currItem.initialZoomLevel || r.updateSize();
                  }, 1e3)),
                o.addClass(e, 'pswp--visible');
            }
          },
          close: function() {
            s &&
              ((s = !1),
              (l = !0),
              Se('close'),
              o.unbind(window, 'resize', r),
              o.unbind(window, 'scroll', m.scroll),
              o.unbind(document, 'keydown', r),
              o.unbind(document, 'mousemove', Le),
              F.transform && o.unbind(r.scrollWrap, 'click', r),
              W && o.unbind(window, f, r),
              Se('unbindEvents'),
              Ht(r.currItem, null, !0, r.destroy));
          },
          destroy: function() {
            Se('destroy'),
              Mt && clearTimeout(Mt),
              e.setAttribute('aria-hidden', 'true'),
              (e.className = O),
              x && clearInterval(x),
              o.unbind(r.scrollWrap, h, r),
              o.unbind(window, 'scroll', r),
              ft(),
              Xe(),
              (Ce = null);
          },
          panTo: function(e, t, n) {
            n ||
              (e > Q.min.x ? (e = Q.min.x) : e < Q.max.x && (e = Q.max.x),
              t > Q.min.y ? (t = Q.min.y) : t < Q.max.y && (t = Q.max.y)),
              (ue.x = e),
              (ue.y = t),
              Ae();
          },
          handleEvent: function(e) {
            (e = e || window.event), m[e.type] && m[e.type](e);
          },
          goTo: function(e) {
            var t = (e = we(e)) - u;
            (me = t), (r.currItem = Pt((u = e))), Ne(he.x * (pe -= t)), Xe(), (te = !1), r.updateCurrItem();
          },
          next: function() {
            r.goTo(u + 1);
          },
          prev: function() {
            r.goTo(u - 1);
          },
          updateCurrZoomItem: function(e) {
            if ((e && Se('beforeChange', 0), C[1].el.children.length)) {
              var t = C[1].el.children[0];
              ee = o.hasClass(t, 'pswp__zoom-wrap') ? t.style : null;
            } else ee = null;
            (g = v = r.currItem.initialZoomLevel),
              (ue.x = (Q = r.currItem.bounds).center.x),
              (ue.y = Q.center.y),
              e && Se('afterChange');
          },
          invalidateCurrItems: function() {
            w = !0;
            for (var e = 0; 3 > e; e++) C[e].item && (C[e].item.needsUpdate = !0);
          },
          updateCurrItem: function(e) {
            if (0 !== me) {
              var t,
                n = Math.abs(me);
              if (!(e && 2 > n)) {
                (r.currItem = Pt(u)),
                  (be = !1),
                  Se('beforeChange', me),
                  n >= 3 && ((p += me + (me > 0 ? -3 : 3)), (n = 3));
                for (var i = 0; n > i; i++)
                  me > 0
                    ? ((t = C.shift()),
                      (C[2] = t),
                      p++,
                      Ie((p + 2) * he.x, t.el.style),
                      r.setContent(t, u - n + i + 1 + 1))
                    : ((t = C.pop()), C.unshift(t), p--, Ie(p * he.x, t.el.style), r.setContent(t, u + n - i - 1 - 1));
                if (ee && 1 === Math.abs(me)) {
                  var o = Pt(T);
                  o.initialZoomLevel !== v && (Ut(o, de), Kt(o), De(o));
                }
                (me = 0), r.updateCurrZoomItem(), (T = u), Se('afterChange');
              }
            }
          },
          updateSize: function(t) {
            if (!ve && a.modal) {
              var n = o.getScrollY();
              if (
                (L !== n && ((e.style.top = n + 'px'), (L = n)),
                !t && ye.x === window.innerWidth && ye.y === window.innerHeight)
              )
                return;
              (ye.x = window.innerWidth), (ye.y = window.innerHeight), (e.style.height = ye.y + 'px');
            }
            if (
              ((de.x = r.scrollWrap.clientWidth),
              (de.y = r.scrollWrap.clientHeight),
              Be(),
              (he.x = de.x + Math.round(de.x * a.spacing)),
              (he.y = de.y),
              Ne(he.x * pe),
              Se('beforeResize'),
              void 0 !== p)
            ) {
              for (var i, s, l, c = 0; 3 > c; c++)
                Ie((c + p) * he.x, (i = C[c]).el.style),
                  (l = u + c - 1),
                  a.loop && qt() > 2 && (l = we(l)),
                  (s = Pt(l)) && (w || s.needsUpdate || !s.bounds)
                    ? (r.cleanSlide(s),
                      r.setContent(i, l),
                      1 === c && ((r.currItem = s), r.updateCurrZoomItem(!0)),
                      (s.needsUpdate = !1))
                    : -1 === i.index && l >= 0 && r.setContent(i, l),
                  s && s.container && (Ut(s, de), Kt(s), De(s));
              w = !1;
            }
            (g = v = r.currItem.initialZoomLevel),
              (Q = r.currItem.bounds) && ((ue.x = Q.center.x), (ue.y = Q.center.y), Ae(!0)),
              Se('resize');
          },
          zoomTo: function(e, t, n, i, r) {
            t && ((g = v), (ut.x = Math.abs(t.x) - ue.x), (ut.y = Math.abs(t.y) - ue.y), Oe(ce, ue));
            var a = Fe(e, !1),
              s = {};
            qe('x', a, s, e), qe('y', a, s, e);
            var l = v,
              c = ue.x,
              u = ue.y;
            Re(s);
            var d = function(t) {
              1 === t
                ? ((v = e), (ue.x = s.x), (ue.y = s.y))
                : ((v = (e - l) * t + l), (ue.x = (s.x - c) * t + c), (ue.y = (s.y - u) * t + u)),
                r && r(t),
                Ae(1 === t);
            };
            n ? Ke('customZoomTo', 0, 1, n, i || o.easing.sine.inOut, d) : d(1);
          }
        },
        Ge = {},
        Je = {},
        Qe = {},
        et = {},
        tt = {},
        nt = [],
        it = {},
        ot = [],
        rt = {},
        at = 0,
        st = { x: 0, y: 0 },
        lt = 0,
        ct = { x: 0, y: 0 },
        ut = { x: 0, y: 0 },
        dt = { x: 0, y: 0 },
        pt = function(e, t) {
          return (rt.x = Math.abs(e.x - t.x)), (rt.y = Math.abs(e.y - t.y)), Math.sqrt(rt.x * rt.x + rt.y * rt.y);
        },
        ft = function() {
          X && ($(X), (X = null));
        },
        ht = function() {
          W && ((X = N(ht)), At());
        },
        mt = function(e, t) {
          return (
            !(!e || e === document) &&
            !(e.getAttribute('class') && e.getAttribute('class').indexOf('pswp__scroll-wrap') > -1) &&
            (t(e) ? e : mt(e.parentNode, t))
          );
        },
        vt = {},
        gt = function(e, t) {
          return (vt.prevent = !mt(e.target, a.isClickableElement)), Se('preventDragEvent', e, t, vt), vt.prevent;
        },
        yt = function(e, t) {
          return (t.x = e.pageX), (t.y = e.pageY), (t.id = e.identifier), t;
        },
        bt = function(e, t, n) {
          (n.x = 0.5 * (e.x + t.x)), (n.y = 0.5 * (e.y + t.y));
        },
        xt = function() {
          return 1 - Math.abs((ue.y - r.currItem.initialPosition.y) / (de.y / 2));
        },
        wt = {},
        Ct = {},
        Tt = [],
        St = function(e) {
          for (; Tt.length > 0; ) Tt.pop();
          return (
            D
              ? ((se = 0),
                nt.forEach(function(e) {
                  0 === se ? (Tt[0] = e) : 1 === se && (Tt[1] = e), se++;
                }))
              : e.type.indexOf('touch') > -1
              ? e.touches &&
                e.touches.length > 0 &&
                ((Tt[0] = yt(e.touches[0], wt)), e.touches.length > 1 && (Tt[1] = yt(e.touches[1], Ct)))
              : ((wt.x = e.pageX), (wt.y = e.pageY), (wt.id = ''), (Tt[0] = wt)),
            Tt
          );
        },
        Et = function(e, t) {
          var n,
            i,
            o,
            s,
            l = ue[e] + t[e],
            c = ct.x + t.x,
            u = ct.x - it.x;
          return (
            (l = ue[e] + t[e] * (n = l > Q.min[e] || l < Q.max[e] ? a.panEndFriction : 1)),
            (!a.allowPanToNext && v !== r.currItem.initialZoomLevel) ||
            (ee
              ? 'h' !== ne ||
                'x' !== e ||
                Z ||
                (t[e] > 0
                  ? (l > Q.min[e] && ((n = a.panEndFriction), (i = Q.min[e] - ce[e])),
                    (0 >= i || 0 > u) && qt() > 1
                      ? ((s = c), 0 > u && c > it.x && (s = it.x))
                      : Q.min.x !== Q.max.x && (o = l))
                  : (l < Q.max[e] && ((n = a.panEndFriction), (i = ce[e] - Q.max[e])),
                    (0 >= i || u > 0) && qt() > 1
                      ? ((s = c), u > 0 && c < it.x && (s = it.x))
                      : Q.min.x !== Q.max.x && (o = l)))
              : (s = c),
            'x' !== e)
              ? void (te || K || (v > r.currItem.fitRatio && (ue[e] += t[e] * n)))
              : (void 0 !== s && (Ne(s, !0), (K = s !== it.x)),
                Q.min.x !== Q.max.x && (void 0 !== o ? (ue.x = o) : K || (ue.x += t.x * n)),
                void 0 !== s)
          );
        },
        kt = function(e) {
          if (!('mousedown' === e.type && e.button > 0)) {
            if (jt) return void e.preventDefault();
            if (!B || 'mousedown' !== e.type) {
              if ((gt(e, !0) && e.preventDefault(), Se('pointerDown'), D)) {
                var t = o.arraySearch(nt, e.pointerId, 'id');
                0 > t && (t = nt.length), (nt[t] = { x: e.pageX, y: e.pageY, id: e.pointerId });
              }
              var n = St(e),
                i = n.length;
              (Y = null),
                Xe(),
                (W && 1 !== i) ||
                  ((W = ie = !0),
                  o.bind(window, f, r),
                  (H = ae = oe = z = K = V = U = Z = !1),
                  (ne = null),
                  Se('firstTouchStart', n),
                  Oe(ce, ue),
                  (le.x = le.y = 0),
                  Oe(et, n[0]),
                  Oe(tt, et),
                  (it.x = he.x * pe),
                  (ot = [{ x: et.x, y: et.y }]),
                  (P = j = Ee()),
                  Fe(v, !0),
                  ft(),
                  ht()),
                !G &&
                  i > 1 &&
                  !te &&
                  !K &&
                  ((g = v),
                  (Z = !1),
                  (G = U = !0),
                  (le.y = le.x = 0),
                  Oe(ce, ue),
                  Oe(Ge, n[0]),
                  Oe(Je, n[1]),
                  bt(Ge, Je, dt),
                  (ut.x = Math.abs(dt.x) - ue.x),
                  (ut.y = Math.abs(dt.y) - ue.y),
                  (J = pt(Ge, Je)));
            }
          }
        },
        _t = function(e) {
          if ((e.preventDefault(), D)) {
            var t = o.arraySearch(nt, e.pointerId, 'id');
            if (t > -1) {
              var n = nt[t];
              (n.x = e.pageX), (n.y = e.pageY);
            }
          }
          if (W) {
            var i = St(e);
            if (ne || V || G) Y = i;
            else if (ct.x !== he.x * pe) ne = 'h';
            else {
              var r = Math.abs(i[0].x - et.x) - Math.abs(i[0].y - et.y);
              Math.abs(r) >= 10 && ((ne = r > 0 ? 'h' : 'v'), (Y = i));
            }
          }
        },
        At = function() {
          if (Y) {
            var e = Y.length;
            if (0 !== e)
              if ((Oe(Ge, Y[0]), (Qe.x = Ge.x - et.x), (Qe.y = Ge.y - et.y), G && e > 1)) {
                if (
                  ((et.x = Ge.x),
                  (et.y = Ge.y),
                  !Qe.x &&
                    !Qe.y &&
                    (function(e, t) {
                      return e.x === t.x && e.y === t.y;
                    })(Y[1], Je))
                )
                  return;
                Oe(Je, Y[1]), Z || ((Z = !0), Se('zoomGestureStarted'));
                var t = pt(Ge, Je),
                  n = Ot(t);
                n > r.currItem.initialZoomLevel + r.currItem.initialZoomLevel / 15 && (ae = !0);
                var i = 1,
                  o = je(),
                  s = Pe();
                if (o > n)
                  if (a.pinchToClose && !ae && g <= r.currItem.initialZoomLevel) {
                    var l = 1 - (o - n) / (o / 1.2);
                    ke(l), Se('onPinchClose', l), (oe = !0);
                  } else (i = (o - n) / o) > 1 && (i = 1), (n = o - i * (o / 3));
                else n > s && ((i = (n - s) / (6 * o)) > 1 && (i = 1), (n = s + i * o));
                0 > i && (i = 0),
                  bt(Ge, Je, st),
                  (le.x += st.x - dt.x),
                  (le.y += st.y - dt.y),
                  Oe(dt, st),
                  (ue.x = $e('x', n)),
                  (ue.y = $e('y', n)),
                  (H = n > v),
                  (v = n),
                  Ae();
              } else {
                if (!ne) return;
                if (
                  (ie &&
                    ((ie = !1),
                    Math.abs(Qe.x) >= 10 && (Qe.x -= Y[0].x - tt.x),
                    Math.abs(Qe.y) >= 10 && (Qe.y -= Y[0].y - tt.y)),
                  (et.x = Ge.x),
                  (et.y = Ge.y),
                  0 === Qe.x && 0 === Qe.y)
                )
                  return;
                if ('v' === ne && a.closeOnVerticalDrag && 'fit' === a.scaleMode && v === r.currItem.initialZoomLevel) {
                  (le.y += Qe.y), (ue.y += Qe.y);
                  var c = xt();
                  return (z = !0), Se('onVerticalDrag', c), ke(c), void Ae();
                }
                (function(e, t, n) {
                  if (e - P > 50) {
                    var i = ot.length > 2 ? ot.shift() : {};
                    (i.x = t), (i.y = n), ot.push(i), (P = e);
                  }
                })(Ee(), Ge.x, Ge.y),
                  (V = !0),
                  (Q = r.currItem.bounds),
                  Et('x', Qe) || (Et('y', Qe), Re(ue), Ae());
              }
          }
        },
        Dt = function(e) {
          if (F.isOldAndroid) {
            if (B && 'mouseup' === e.type) return;
            e.type.indexOf('touch') > -1 &&
              (clearTimeout(B),
              (B = setTimeout(function() {
                B = 0;
              }, 600)));
          }
          var t;
          if ((Se('pointerUp'), gt(e, !1) && e.preventDefault(), D)) {
            var n = o.arraySearch(nt, e.pointerId, 'id');
            n > -1 &&
              ((t = nt.splice(n, 1)[0]),
              navigator.pointerEnabled
                ? (t.type = e.pointerType || 'mouse')
                : ((t.type = { 4: 'mouse', 2: 'touch', 3: 'pen' }[e.pointerType]),
                  t.type || (t.type = e.pointerType || 'mouse')));
          }
          var i,
            s = St(e),
            l = s.length;
          if (('mouseup' === e.type && (l = 0), 2 === l)) return (Y = null), !0;
          1 === l && Oe(tt, s[0]),
            0 !== l ||
              ne ||
              te ||
              (t ||
                ('mouseup' === e.type
                  ? (t = { x: e.pageX, y: e.pageY, type: 'mouse' })
                  : e.changedTouches &&
                    e.changedTouches[0] &&
                    (t = { x: e.changedTouches[0].pageX, y: e.changedTouches[0].pageY, type: 'touch' })),
              Se('touchRelease', e, t));
          var c = -1;
          if (
            (0 === l && ((W = !1), o.unbind(window, f, r), ft(), G ? (c = 0) : -1 !== lt && (c = Ee() - lt)),
            (lt = 1 === l ? Ee() : -1),
            (i = -1 !== c && 150 > c ? 'zoom' : 'swipe'),
            G && 2 > l && ((G = !1), 1 === l && (i = 'zoomPointerUp'), Se('zoomGestureEnded')),
            (Y = null),
            V || Z || te || z)
          )
            if ((Xe(), q || (q = It()), q.calculateSwipeSpeed('x'), z))
              if (xt() < a.verticalDragRange) r.close();
              else {
                var u = ue.y,
                  d = re;
                Ke('verticalDrag', 0, 1, 300, o.easing.cubic.out, function(e) {
                  (ue.y = (r.currItem.initialPosition.y - u) * e + u), ke((1 - d) * e + d), Ae();
                }),
                  Se('onVerticalDrag', 1);
              }
            else {
              if ((K || te) && 0 === l) {
                if ($t(i, q)) return;
                i = 'zoomPointerUp';
              }
              if (!te) return 'swipe' !== i ? void Rt() : void (!K && v > r.currItem.fitRatio && Nt(q));
            }
        },
        It = function() {
          var e,
            t,
            n = {
              lastFlickOffset: {},
              lastFlickDist: {},
              lastFlickSpeed: {},
              slowDownRatio: {},
              slowDownRatioReverse: {},
              speedDecelerationRatio: {},
              speedDecelerationRatioAbs: {},
              distanceOffset: {},
              backAnimDestination: {},
              backAnimStarted: {},
              calculateSwipeSpeed: function(i) {
                ot.length > 1 ? ((e = Ee() - P + 50), (t = ot[ot.length - 2][i])) : ((e = Ee() - j), (t = tt[i])),
                  (n.lastFlickOffset[i] = et[i] - t),
                  (n.lastFlickDist[i] = Math.abs(n.lastFlickOffset[i])),
                  (n.lastFlickSpeed[i] = n.lastFlickDist[i] > 20 ? n.lastFlickOffset[i] / e : 0),
                  Math.abs(n.lastFlickSpeed[i]) < 0.1 && (n.lastFlickSpeed[i] = 0),
                  (n.slowDownRatio[i] = 0.95),
                  (n.slowDownRatioReverse[i] = 1 - n.slowDownRatio[i]),
                  (n.speedDecelerationRatio[i] = 1);
              },
              calculateOverBoundsAnimOffset: function(e, t) {
                n.backAnimStarted[e] ||
                  (ue[e] > Q.min[e]
                    ? (n.backAnimDestination[e] = Q.min[e])
                    : ue[e] < Q.max[e] && (n.backAnimDestination[e] = Q.max[e]),
                  void 0 !== n.backAnimDestination[e] &&
                    ((n.slowDownRatio[e] = 0.7),
                    (n.slowDownRatioReverse[e] = 1 - n.slowDownRatio[e]),
                    n.speedDecelerationRatioAbs[e] < 0.05 &&
                      ((n.lastFlickSpeed[e] = 0),
                      (n.backAnimStarted[e] = !0),
                      Ke('bounceZoomPan' + e, ue[e], n.backAnimDestination[e], t || 300, o.easing.sine.out, function(
                        t
                      ) {
                        (ue[e] = t), Ae();
                      }))));
              },
              calculateAnimOffset: function(e) {
                n.backAnimStarted[e] ||
                  ((n.speedDecelerationRatio[e] =
                    n.speedDecelerationRatio[e] *
                    (n.slowDownRatio[e] + n.slowDownRatioReverse[e] - (n.slowDownRatioReverse[e] * n.timeDiff) / 10)),
                  (n.speedDecelerationRatioAbs[e] = Math.abs(n.lastFlickSpeed[e] * n.speedDecelerationRatio[e])),
                  (n.distanceOffset[e] = n.lastFlickSpeed[e] * n.speedDecelerationRatio[e] * n.timeDiff),
                  (ue[e] += n.distanceOffset[e]));
              },
              panAnimLoop: function() {
                return We.zoomPan &&
                  ((We.zoomPan.raf = N(n.panAnimLoop)),
                  (n.now = Ee()),
                  (n.timeDiff = n.now - n.lastNow),
                  (n.lastNow = n.now),
                  n.calculateAnimOffset('x'),
                  n.calculateAnimOffset('y'),
                  Ae(),
                  n.calculateOverBoundsAnimOffset('x'),
                  n.calculateOverBoundsAnimOffset('y'),
                  n.speedDecelerationRatioAbs.x < 0.05 && n.speedDecelerationRatioAbs.y < 0.05)
                  ? ((ue.x = Math.round(ue.x)), (ue.y = Math.round(ue.y)), Ae(), void Ze('zoomPan'))
                  : void 0;
              }
            };
          return n;
        },
        Nt = function(e) {
          return (
            e.calculateSwipeSpeed('y'),
            (Q = r.currItem.bounds),
            (e.backAnimDestination = {}),
            (e.backAnimStarted = {}),
            Math.abs(e.lastFlickSpeed.x) <= 0.05 && Math.abs(e.lastFlickSpeed.y) <= 0.05
              ? ((e.speedDecelerationRatioAbs.x = e.speedDecelerationRatioAbs.y = 0),
                e.calculateOverBoundsAnimOffset('x'),
                e.calculateOverBoundsAnimOffset('y'),
                !0)
              : (Ve('zoomPan'), (e.lastNow = Ee()), void e.panAnimLoop())
          );
        },
        $t = function(e, t) {
          var n, i, s;
          if ((te || (at = u), 'swipe' === e)) {
            var l = et.x - tt.x,
              c = t.lastFlickDist.x < 10;
            l > 30 && (c || t.lastFlickOffset.x > 20)
              ? (i = -1)
              : -30 > l && (c || t.lastFlickOffset.x < -20) && (i = 1);
          }
          i &&
            (0 > (u += i)
              ? ((u = a.loop ? qt() - 1 : 0), (s = !0))
              : u >= qt() && ((u = a.loop ? 0 : qt() - 1), (s = !0)),
            (!s || a.loop) && ((me += i), (pe -= i), (n = !0)));
          var d,
            p = he.x * pe,
            f = Math.abs(p - ct.x);
          return (
            n || p > ct.x == t.lastFlickSpeed.x > 0
              ? ((d = Math.abs(t.lastFlickSpeed.x) > 0 ? f / Math.abs(t.lastFlickSpeed.x) : 333),
                (d = Math.min(d, 400)),
                (d = Math.max(d, 250)))
              : (d = 333),
            at === u && (n = !1),
            (te = !0),
            Se('mainScrollAnimStart'),
            Ke('mainScroll', ct.x, p, d, o.easing.cubic.out, Ne, function() {
              Xe(), (te = !1), (at = -1), (n || at !== u) && r.updateCurrItem(), Se('mainScrollAnimComplete');
            }),
            n && r.updateCurrItem(!0),
            n
          );
        },
        Ot = function(e) {
          return (1 / J) * e * g;
        },
        Rt = function() {
          var e = v,
            t = je(),
            n = Pe();
          t > v ? (e = t) : v > n && (e = n);
          var i,
            a = re;
          return oe && !H && !ae && t > v
            ? (r.close(), !0)
            : (oe &&
                (i = function(e) {
                  ke((1 - a) * e + a);
                }),
              r.zoomTo(e, 0, 200, o.easing.cubic.out, i),
              !0);
        };
      xe('Gestures', {
        publicMethods: {
          initGestures: function() {
            var e = function(e, t, n, i, o) {
              (S = e + t), (E = e + n), (k = e + i), (_ = o ? e + o : '');
            };
            (D = F.pointerEvent) && F.touch && (F.touch = !1),
              D
                ? navigator.pointerEnabled
                  ? e('pointer', 'down', 'move', 'up', 'cancel')
                  : e('MSPointer', 'Down', 'Move', 'Up', 'Cancel')
                : F.touch
                ? (e('touch', 'start', 'move', 'end', 'cancel'), (I = !0))
                : e('mouse', 'down', 'move', 'up'),
              (f = E + ' ' + k + ' ' + _),
              (h = S),
              D && !I && (I = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1),
              (r.likelyTouchDevice = I),
              (m[S] = kt),
              (m[E] = _t),
              (m[k] = Dt),
              _ && (m[_] = m[k]),
              F.touch &&
                ((h += ' mousedown'),
                (f += ' mousemove mouseup'),
                (m.mousedown = m[S]),
                (m.mousemove = m[E]),
                (m.mouseup = m[k])),
              I || (a.allowPanToNext = !1);
          }
        }
      });
      var Mt,
        Lt,
        Ft,
        jt,
        Pt,
        qt,
        Ht = function(t, n, i, s) {
          var l;
          Mt && clearTimeout(Mt),
            (jt = !0),
            (Ft = !0),
            t.initialLayout
              ? ((l = t.initialLayout), (t.initialLayout = null))
              : (l = a.getThumbBoundsFn && a.getThumbBoundsFn(u));
          var d = i ? a.hideAnimationDuration : a.showAnimationDuration,
            p = function() {
              Ze('initialZoom'),
                i
                  ? (r.template.removeAttribute('style'), r.bg.removeAttribute('style'))
                  : (ke(1),
                    n && (n.style.display = 'block'),
                    o.addClass(e, 'pswp--animated-in'),
                    Se('initialZoom' + (i ? 'OutEnd' : 'InEnd'))),
                s && s(),
                (jt = !1);
            };
          if (!d || !l || void 0 === l.x)
            return (
              Se('initialZoom' + (i ? 'Out' : 'In')),
              (v = t.initialZoomLevel),
              Oe(ue, t.initialPosition),
              Ae(),
              (e.style.opacity = i ? 0 : 1),
              ke(1),
              void (d
                ? setTimeout(function() {
                    p();
                  }, d)
                : p())
            );
          !(function() {
            var n = c,
              s = !r.currItem.src || r.currItem.loadError || a.showHideOpacity;
            t.miniImg && (t.miniImg.style.webkitBackfaceVisibility = 'hidden'),
              i ||
                ((v = l.w / t.w),
                (ue.x = l.x),
                (ue.y = l.y - R),
                (r[s ? 'template' : 'bg'].style.opacity = 0.001),
                Ae()),
              Ve('initialZoom'),
              i && !n && o.removeClass(e, 'pswp--animated-in'),
              s &&
                (i
                  ? o[(n ? 'remove' : 'add') + 'Class'](e, 'pswp--animate_opacity')
                  : setTimeout(function() {
                      o.addClass(e, 'pswp--animate_opacity');
                    }, 30)),
              (Mt = setTimeout(
                function() {
                  if ((Se('initialZoom' + (i ? 'Out' : 'In')), i)) {
                    var r = l.w / t.w,
                      a = { x: ue.x, y: ue.y },
                      c = v,
                      u = re,
                      f = function(t) {
                        1 === t
                          ? ((v = r), (ue.x = l.x), (ue.y = l.y - L))
                          : ((v = (r - c) * t + c), (ue.x = (l.x - a.x) * t + a.x), (ue.y = (l.y - L - a.y) * t + a.y)),
                          Ae(),
                          s ? (e.style.opacity = 1 - t) : ke(u - t * u);
                      };
                    n ? Ke('initialZoom', 0, 1, d, o.easing.cubic.out, f, p) : (f(1), (Mt = setTimeout(p, d + 20)));
                  } else
                    (v = t.initialZoomLevel),
                      Oe(ue, t.initialPosition),
                      Ae(),
                      ke(1),
                      s ? (e.style.opacity = 1) : ke(1),
                      (Mt = setTimeout(p, d + 20));
                },
                i ? 25 : 90
              ));
          })();
        },
        zt = {},
        Bt = [],
        Wt = {
          index: 0,
          errorMsg:
            '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
          forceProgressiveLoading: !1,
          preload: [1, 1],
          getNumItemsFn: function() {
            return Lt.length;
          }
        },
        Ut = function(e, t, n) {
          if (e.src && !e.loadError) {
            var i = !n;
            if (
              (i && (e.vGap || (e.vGap = { top: 0, bottom: 0 }), Se('parseVerticalMargin', e)),
              (zt.x = t.x),
              (zt.y = t.y - e.vGap.top - e.vGap.bottom),
              i)
            ) {
              var o = zt.x / e.w,
                r = zt.y / e.h;
              e.fitRatio = r > o ? o : r;
              var s = a.scaleMode;
              'orig' === s ? (n = 1) : 'fit' === s && (n = e.fitRatio),
                n > 1 && (n = 1),
                (e.initialZoomLevel = n),
                e.bounds || (e.bounds = { center: { x: 0, y: 0 }, max: { x: 0, y: 0 }, min: { x: 0, y: 0 } });
            }
            if (!n) return;
            return (
              (function(e, t, n) {
                var i = e.bounds;
                (i.center.x = Math.round((zt.x - t) / 2)),
                  (i.center.y = Math.round((zt.y - n) / 2) + e.vGap.top),
                  (i.max.x = t > zt.x ? Math.round(zt.x - t) : i.center.x),
                  (i.max.y = n > zt.y ? Math.round(zt.y - n) + e.vGap.top : i.center.y),
                  (i.min.x = t > zt.x ? 0 : i.center.x),
                  (i.min.y = n > zt.y ? e.vGap.top : i.center.y);
              })(e, e.w * n, e.h * n),
              i && n === e.initialZoomLevel && (e.initialPosition = e.bounds.center),
              e.bounds
            );
          }
          return (
            (e.w = e.h = 0),
            (e.initialZoomLevel = e.fitRatio = 1),
            (e.bounds = { center: { x: 0, y: 0 }, max: { x: 0, y: 0 }, min: { x: 0, y: 0 } }),
            (e.initialPosition = e.bounds.center),
            e.bounds
          );
        },
        Zt = function(e, t, n, i, o, a) {
          t.loadError ||
            (i &&
              ((t.imageAppended = !0),
              Kt(t, i, t === r.currItem && be),
              n.appendChild(i),
              a &&
                setTimeout(function() {
                  t && t.loaded && t.placeholder && ((t.placeholder.style.display = 'none'), (t.placeholder = null));
                }, 500)));
        },
        Vt = function(e) {
          (e.loading = !0), (e.loaded = !1);
          var t = (e.img = o.createEl('pswp__img', 'img')),
            n = function() {
              (e.loading = !1),
                (e.loaded = !0),
                e.loadComplete ? e.loadComplete(e) : (e.img = null),
                (t.onload = t.onerror = null),
                (t = null);
            };
          return (
            (t.onload = n),
            (t.onerror = function() {
              (e.loadError = !0), n();
            }),
            (t.src = e.src),
            t
          );
        },
        Xt = function(e, t) {
          return e.src && e.loadError && e.container
            ? (t && (e.container.innerHTML = ''), (e.container.innerHTML = a.errorMsg.replace('%url%', e.src)), !0)
            : void 0;
        },
        Kt = function(e, t, n) {
          if (e.src) {
            t || (t = e.container.lastChild);
            var i = n ? e.w : Math.round(e.w * e.fitRatio),
              o = n ? e.h : Math.round(e.h * e.fitRatio);
            e.placeholder &&
              !e.loaded &&
              ((e.placeholder.style.width = i + 'px'), (e.placeholder.style.height = o + 'px')),
              (t.style.width = i + 'px'),
              (t.style.height = o + 'px');
          }
        },
        Yt = function() {
          if (Bt.length) {
            for (var e, t = 0; t < Bt.length; t++)
              (e = Bt[t]).holder.index === e.index && Zt(0, e.item, e.baseDiv, e.img, 0, e.clearPlaceholder);
            Bt = [];
          }
        };
      xe('Controller', {
        publicMethods: {
          lazyLoadItem: function(e) {
            e = we(e);
            var t = Pt(e);
            t && ((!t.loaded && !t.loading) || w) && (Se('gettingData', e, t), t.src && Vt(t));
          },
          initController: function() {
            o.extend(a, Wt, !0),
              (r.items = Lt = n),
              (Pt = r.getItemAt),
              (qt = a.getNumItemsFn)() < 3 && (a.loop = !1),
              Te('beforeChange', function(e) {
                var t,
                  n = a.preload,
                  i = null === e || e >= 0,
                  o = Math.min(n[0], qt()),
                  s = Math.min(n[1], qt());
                for (t = 1; (i ? s : o) >= t; t++) r.lazyLoadItem(u + t);
                for (t = 1; (i ? o : s) >= t; t++) r.lazyLoadItem(u - t);
              }),
              Te('initialLayout', function() {
                r.currItem.initialLayout = a.getThumbBoundsFn && a.getThumbBoundsFn(u);
              }),
              Te('mainScrollAnimComplete', Yt),
              Te('initialZoomInEnd', Yt),
              Te('destroy', function() {
                for (var e, t = 0; t < Lt.length; t++)
                  (e = Lt[t]).container && (e.container = null),
                    e.placeholder && (e.placeholder = null),
                    e.img && (e.img = null),
                    e.preloader && (e.preloader = null),
                    e.loadError && (e.loaded = e.loadError = !1);
                Bt = null;
              });
          },
          getItemAt: function(e) {
            return e >= 0 && void 0 !== Lt[e] && Lt[e];
          },
          allowProgressiveImg: function() {
            return a.forceProgressiveLoading || !I || a.mouseUsed || screen.width > 1200;
          },
          setContent: function(e, t) {
            a.loop && (t = we(t));
            var n = r.getItemAt(e.index);
            n && (n.container = null);
            var i,
              l = r.getItemAt(t);
            if (l) {
              Se('gettingData', t, l), (e.index = t), (e.item = l);
              var c = (l.container = o.createEl('pswp__zoom-wrap'));
              if (
                (!l.src && l.html && (l.html.tagName ? c.appendChild(l.html) : (c.innerHTML = l.html)),
                Xt(l),
                Ut(l, de),
                !l.src || l.loadError || l.loaded)
              )
                l.src &&
                  !l.loadError &&
                  (((i = o.createEl('pswp__img', 'img')).style.opacity = 1), (i.src = l.src), Kt(l, i), Zt(0, l, c, i));
              else {
                if (
                  ((l.loadComplete = function(n) {
                    if (s) {
                      if (e && e.index === t) {
                        if (Xt(n, !0))
                          return (
                            (n.loadComplete = n.img = null),
                            Ut(n, de),
                            De(n),
                            void (e.index === u && r.updateCurrZoomItem())
                          );
                        n.imageAppended
                          ? !jt && n.placeholder && ((n.placeholder.style.display = 'none'), (n.placeholder = null))
                          : F.transform && (te || jt)
                          ? Bt.push({ item: n, baseDiv: c, img: n.img, index: t, holder: e, clearPlaceholder: !0 })
                          : Zt(0, n, c, n.img, 0, !0);
                      }
                      (n.loadComplete = null), (n.img = null), Se('imageLoadComplete', t, n);
                    }
                  }),
                  o.features.transform)
                ) {
                  var d = 'pswp__img pswp__img--placeholder',
                    p = o.createEl((d += l.msrc ? '' : ' pswp__img--placeholder--blank'), l.msrc ? 'img' : '');
                  l.msrc && (p.src = l.msrc), Kt(l, p), c.appendChild(p), (l.placeholder = p);
                }
                l.loading || Vt(l),
                  r.allowProgressiveImg() &&
                    (!Ft && F.transform
                      ? Bt.push({ item: l, baseDiv: c, img: l.img, index: t, holder: e })
                      : Zt(0, l, c, l.img, 0, !0));
              }
              Ft || t !== u ? De(l) : ((ee = c.style), Ht(l, i || l.img)), (e.el.innerHTML = ''), e.el.appendChild(c);
            } else e.el.innerHTML = '';
          },
          cleanSlide: function(e) {
            e.img && (e.img.onload = e.img.onerror = null), (e.loaded = e.loading = e.img = e.imageAppended = !1);
          }
        }
      });
      var Gt,
        Jt,
        Qt = {},
        en = function(e, t, n) {
          var i = document.createEvent('CustomEvent');
          i.initCustomEvent('pswpTap', !0, !0, {
            origEvent: e,
            target: e.target,
            releasePoint: t,
            pointerType: n || 'touch'
          }),
            e.target.dispatchEvent(i);
        };
      xe('Tap', {
        publicMethods: {
          initTap: function() {
            Te('firstTouchStart', r.onTapStart),
              Te('touchRelease', r.onTapRelease),
              Te('destroy', function() {
                (Qt = {}), (Gt = null);
              });
          },
          onTapStart: function(e) {
            e.length > 1 && (clearTimeout(Gt), (Gt = null));
          },
          onTapRelease: function(e, t) {
            if (t && !V && !U && !Ue) {
              var n = t;
              if (
                Gt &&
                (clearTimeout(Gt),
                (Gt = null),
                (function(e, t) {
                  return Math.abs(e.x - t.x) < 25 && Math.abs(e.y - t.y) < 25;
                })(n, Qt))
              )
                return void Se('doubleTap', n);
              if ('mouse' === t.type) return void en(e, t, 'mouse');
              if ('BUTTON' === e.target.tagName.toUpperCase() || o.hasClass(e.target, 'pswp__single-tap'))
                return void en(e, t);
              Oe(Qt, n),
                (Gt = setTimeout(function() {
                  en(e, t), (Gt = null);
                }, 300));
            }
          }
        }
      }),
        xe('DesktopZoom', {
          publicMethods: {
            initDesktopZoom: function() {
              M ||
                (I
                  ? Te('mouseUsed', function() {
                      r.setupDesktopZoom();
                    })
                  : r.setupDesktopZoom(!0));
            },
            setupDesktopZoom: function(t) {
              Jt = {};
              var n = 'wheel mousewheel DOMMouseScroll';
              Te('bindEvents', function() {
                o.bind(e, n, r.handleMouseWheel);
              }),
                Te('unbindEvents', function() {
                  Jt && o.unbind(e, n, r.handleMouseWheel);
                }),
                (r.mouseZoomedIn = !1);
              var i,
                a = function() {
                  r.mouseZoomedIn && (o.removeClass(e, 'pswp--zoomed-in'), (r.mouseZoomedIn = !1)),
                    1 > v ? o.addClass(e, 'pswp--zoom-allowed') : o.removeClass(e, 'pswp--zoom-allowed'),
                    s();
                },
                s = function() {
                  i && (o.removeClass(e, 'pswp--dragging'), (i = !1));
                };
              Te('resize', a),
                Te('afterChange', a),
                Te('pointerDown', function() {
                  r.mouseZoomedIn && ((i = !0), o.addClass(e, 'pswp--dragging'));
                }),
                Te('pointerUp', s),
                t || a();
            },
            handleMouseWheel: function(e) {
              if (v <= r.currItem.fitRatio)
                return (
                  a.modal &&
                    (!a.closeOnScroll || Ue || W
                      ? e.preventDefault()
                      : A && Math.abs(e.deltaY) > 2 && ((c = !0), r.close())),
                  !0
                );
              if ((e.stopPropagation(), (Jt.x = 0), 'deltaX' in e))
                1 === e.deltaMode
                  ? ((Jt.x = 18 * e.deltaX), (Jt.y = 18 * e.deltaY))
                  : ((Jt.x = e.deltaX), (Jt.y = e.deltaY));
              else if ('wheelDelta' in e)
                e.wheelDeltaX && (Jt.x = -0.16 * e.wheelDeltaX),
                  (Jt.y = e.wheelDeltaY ? -0.16 * e.wheelDeltaY : -0.16 * e.wheelDelta);
              else {
                if (!('detail' in e)) return;
                Jt.y = e.detail;
              }
              Fe(v, !0);
              var t = ue.x - Jt.x,
                n = ue.y - Jt.y;
              (a.modal || (t <= Q.min.x && t >= Q.max.x && n <= Q.min.y && n >= Q.max.y)) && e.preventDefault(),
                r.panTo(t, n);
            },
            toggleDesktopZoom: function(t) {
              t = t || { x: de.x / 2 + fe.x, y: de.y / 2 + fe.y };
              var n = a.getDoubleTapZoom(!0, r.currItem),
                i = v === n;
              (r.mouseZoomedIn = !i),
                r.zoomTo(i ? r.currItem.initialZoomLevel : n, t, 333),
                o[(i ? 'remove' : 'add') + 'Class'](e, 'pswp--zoomed-in');
            }
          }
        });
      var tn,
        nn,
        on,
        rn,
        an,
        sn,
        ln,
        cn,
        un,
        dn,
        pn,
        fn,
        hn = { history: !0, galleryUID: 1 },
        mn = function() {
          return pn.hash.substring(1);
        },
        vn = function() {
          tn && clearTimeout(tn), on && clearTimeout(on);
        },
        gn = function() {
          var e = mn(),
            t = {};
          if (e.length < 5) return t;
          var n,
            i = e.split('&');
          for (n = 0; n < i.length; n++)
            if (i[n]) {
              var o = i[n].split('=');
              o.length < 2 || (t[o[0]] = o[1]);
            }
          if (a.galleryPIDs) {
            var r = t.pid;
            for (t.pid = 0, n = 0; n < Lt.length; n++)
              if (Lt[n].pid === r) {
                t.pid = n;
                break;
              }
          } else t.pid = parseInt(t.pid, 10) - 1;
          return t.pid < 0 && (t.pid = 0), t;
        },
        yn = function() {
          if ((on && clearTimeout(on), Ue || W)) on = setTimeout(yn, 500);
          else {
            rn ? clearTimeout(nn) : (rn = !0);
            var e = u + 1,
              t = Pt(u);
            t.hasOwnProperty('pid') && (e = t.pid);
            var n = ln + '&gid=' + a.galleryUID + '&pid=' + e;
            cn || (-1 === pn.hash.indexOf(n) && (dn = !0));
            var i = pn.href.split('#')[0] + '#' + n;
            fn
              ? '#' + n !== window.location.hash && history[cn ? 'replaceState' : 'pushState']('', document.title, i)
              : cn
              ? pn.replace(i)
              : (pn.hash = n),
              (cn = !0),
              (nn = setTimeout(function() {
                rn = !1;
              }, 60));
          }
        };
      xe('History', {
        publicMethods: {
          initHistory: function() {
            if ((o.extend(a, hn, !0), a.history)) {
              (pn = window.location),
                (dn = !1),
                (un = !1),
                (cn = !1),
                (ln = mn()),
                (fn = 'pushState' in history),
                ln.indexOf('gid=') > -1 && (ln = (ln = ln.split('&gid=')[0]).split('?gid=')[0]),
                Te('afterChange', r.updateURL),
                Te('unbindEvents', function() {
                  o.unbind(window, 'hashchange', r.onHashChange);
                });
              var e = function() {
                (sn = !0),
                  un ||
                    (dn
                      ? history.back()
                      : ln
                      ? (pn.hash = ln)
                      : fn
                      ? history.pushState('', document.title, pn.pathname + pn.search)
                      : (pn.hash = '')),
                  vn();
              };
              Te('unbindEvents', function() {
                c && e();
              }),
                Te('destroy', function() {
                  sn || e();
                }),
                Te('firstUpdate', function() {
                  u = gn().pid;
                });
              var t = ln.indexOf('pid=');
              t > -1 && '&' === (ln = ln.substring(0, t)).slice(-1) && (ln = ln.slice(0, -1)),
                setTimeout(function() {
                  s && o.bind(window, 'hashchange', r.onHashChange);
                }, 40);
            }
          },
          onHashChange: function() {
            return mn() === ln ? ((un = !0), void r.close()) : void (rn || ((an = !0), r.goTo(gn().pid), (an = !1)));
          },
          updateURL: function() {
            vn(), an || (cn ? (tn = setTimeout(yn, 800)) : yn());
          }
        }
      }),
        o.extend(r, Ye);
    };
  }),
  (function(e, t) {
    'function' == typeof define && define.amd
      ? define(t)
      : 'object' == typeof exports
      ? (module.exports = t())
      : (e.PhotoSwipeUI_Default = t());
  })(this, function() {
    'use strict';
    return function(e, t) {
      var n,
        i,
        o,
        r,
        a,
        s,
        l,
        c,
        u,
        d,
        p,
        f,
        h,
        m,
        v,
        g,
        y,
        b,
        x = this,
        w = !1,
        C = !0,
        T = !0,
        S = {
          barsSize: { top: 44, bottom: 'auto' },
          closeElClasses: ['item', 'caption', 'zoom-wrap', 'ui', 'top-bar'],
          timeToIdle: 4e3,
          timeToIdleOutside: 1e3,
          loadingIndicatorDelay: 1e3,
          addCaptionHTMLFn: function(e, t) {
            return e.title ? ((t.children[0].innerHTML = e.title), !0) : ((t.children[0].innerHTML = ''), !1);
          },
          closeEl: !0,
          captionEl: !0,
          fullscreenEl: !0,
          zoomEl: !0,
          shareEl: !0,
          counterEl: !0,
          arrowEl: !0,
          preloaderEl: !0,
          tapToClose: !1,
          tapToToggleControls: !0,
          clickToCloseNonZoomable: !0,
          shareButtons: [
            { id: 'facebook', label: 'Share on Facebook', url: 'https://www.facebook.com/sharer/sharer.php?u={{url}}' },
            { id: 'twitter', label: 'Tweet', url: 'https://twitter.com/intent/tweet?text={{text}}&url={{url}}' },
            {
              id: 'pinterest',
              label: 'Pin it',
              url: 'http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}'
            },
            { id: 'download', label: 'Download image', url: '{{raw_image_url}}', download: !0 }
          ],
          getImageURLForShare: function() {
            return e.currItem.src || '';
          },
          getPageURLForShare: function() {
            return window.location.href;
          },
          getTextForShare: function() {
            return e.currItem.title || '';
          },
          indexIndicatorSep: ' / ',
          fitControlsWidth: 1200
        },
        E = function(e) {
          if (g) return !0;
          (e = e || window.event), v.timeToIdle && v.mouseUsed && !u && M();
          for (var n, i, o = (e.target || e.srcElement).getAttribute('class') || '', r = 0; r < P.length; r++)
            (n = P[r]).onTap && o.indexOf('pswp__' + n.name) > -1 && (n.onTap(), (i = !0));
          i &&
            (e.stopPropagation && e.stopPropagation(),
            (g = !0),
            setTimeout(
              function() {
                g = !1;
              },
              t.features.isOldAndroid ? 600 : 30
            ));
        },
        k = function() {
          return !e.likelyTouchDevice || v.mouseUsed || screen.width > v.fitControlsWidth;
        },
        _ = function(e, n, i) {
          t[(i ? 'add' : 'remove') + 'Class'](e, 'pswp__' + n);
        },
        A = function() {
          var e = 1 === v.getNumItemsFn();
          e !== m && (_(i, 'ui--one-slide', e), (m = e));
        },
        D = function() {
          _(l, 'share-modal--hidden', T);
        },
        I = function() {
          return (
            (T = !T)
              ? (t.removeClass(l, 'pswp__share-modal--fade-in'),
                setTimeout(function() {
                  T && D();
                }, 300))
              : (D(),
                setTimeout(function() {
                  T || t.addClass(l, 'pswp__share-modal--fade-in');
                }, 30)),
            T || $(),
            !1
          );
        },
        N = function(t) {
          var n = (t = t || window.event).target || t.srcElement;
          return (
            e.shout('shareLinkClick', t, n),
            !(
              !n.href ||
              (!n.hasAttribute('download') &&
                (window.open(
                  n.href,
                  'pswp_share',
                  'scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=' +
                    (window.screen ? Math.round(screen.width / 2 - 275) : 100)
                ),
                T || I(),
                1))
            )
          );
        },
        $ = function() {
          for (var e, t, n, i, o = '', r = 0; r < v.shareButtons.length; r++)
            (t = v.getImageURLForShare((e = v.shareButtons[r]))),
              (n = v.getPageURLForShare(e)),
              (i = v.getTextForShare(e)),
              (o +=
                '<a href="' +
                e.url
                  .replace('{{url}}', encodeURIComponent(n))
                  .replace('{{image_url}}', encodeURIComponent(t))
                  .replace('{{raw_image_url}}', t)
                  .replace('{{text}}', encodeURIComponent(i)) +
                '" target="_blank" class="pswp__share--' +
                e.id +
                '"' +
                (e.download ? 'download' : '') +
                '>' +
                e.label +
                '</a>'),
              v.parseShareButtonOut && (o = v.parseShareButtonOut(e, o));
          (l.children[0].innerHTML = o), (l.children[0].onclick = N);
        },
        O = function(e) {
          for (var n = 0; n < v.closeElClasses.length; n++)
            if (t.hasClass(e, 'pswp__' + v.closeElClasses[n])) return !0;
        },
        R = 0,
        M = function() {
          clearTimeout(b), (R = 0), u && x.setIdle(!1);
        },
        L = function(e) {
          var t = (e = e || window.event).relatedTarget || e.toElement;
          (t && 'HTML' !== t.nodeName) ||
            (clearTimeout(b),
            (b = setTimeout(function() {
              x.setIdle(!0);
            }, v.timeToIdleOutside)));
        },
        F = function(e) {
          f !== e && (_(p, 'preloader--active', !e), (f = e));
        },
        j = function(e) {
          var n = e.vGap;
          if (k()) {
            var a = v.barsSize;
            v.captionEl && 'auto' === a.bottom
              ? (r ||
                  ((r = t.createEl('pswp__caption pswp__caption--fake')).appendChild(
                    t.createEl('pswp__caption__center')
                  ),
                  i.insertBefore(r, o),
                  t.addClass(i, 'pswp__ui--fit')),
                (n.bottom = v.addCaptionHTMLFn(e, r, !0) ? parseInt(r.clientHeight, 10) || 44 : a.top))
              : (n.bottom = 'auto' === a.bottom ? 0 : a.bottom),
              (n.top = a.top);
          } else n.top = n.bottom = 0;
        },
        P = [
          {
            name: 'caption',
            option: 'captionEl',
            onInit: function(e) {
              o = e;
            }
          },
          {
            name: 'share-modal',
            option: 'shareEl',
            onInit: function(e) {
              l = e;
            },
            onTap: function() {
              I();
            }
          },
          {
            name: 'button--share',
            option: 'shareEl',
            onInit: function(e) {
              s = e;
            },
            onTap: function() {
              I();
            }
          },
          { name: 'button--zoom', option: 'zoomEl', onTap: e.toggleDesktopZoom },
          {
            name: 'counter',
            option: 'counterEl',
            onInit: function(e) {
              a = e;
            }
          },
          { name: 'button--close', option: 'closeEl', onTap: e.close },
          { name: 'button--arrow--left', option: 'arrowEl', onTap: e.prev },
          { name: 'button--arrow--right', option: 'arrowEl', onTap: e.next },
          {
            name: 'button--fs',
            option: 'fullscreenEl',
            onTap: function() {
              n.isFullscreen() ? n.exit() : n.enter();
            }
          },
          {
            name: 'preloader',
            option: 'preloaderEl',
            onInit: function(e) {
              p = e;
            }
          }
        ];
      (x.init = function() {
        t.extend(e.options, S, !0),
          (v = e.options),
          (i = t.getChildByClass(e.scrollWrap, 'pswp__ui')),
          (d = e.listen),
          (function() {
            var e;
            d('onVerticalDrag', function(e) {
              C && 0.95 > e ? x.hideControls() : !C && e >= 0.95 && x.showControls();
            }),
              d('onPinchClose', function(t) {
                C && 0.9 > t ? (x.hideControls(), (e = !0)) : e && !C && t > 0.9 && x.showControls();
              }),
              d('zoomGestureEnded', function() {
                (e = !1) && !C && x.showControls();
              });
          })(),
          d('beforeChange', x.update),
          d('doubleTap', function(t) {
            var n = e.currItem.initialZoomLevel;
            e.getZoomLevel() !== n ? e.zoomTo(n, t, 333) : e.zoomTo(v.getDoubleTapZoom(!1, e.currItem), t, 333);
          }),
          d('preventDragEvent', function(e, t, n) {
            var i = e.target || e.srcElement;
            i &&
              i.getAttribute('class') &&
              e.type.indexOf('mouse') > -1 &&
              (i.getAttribute('class').indexOf('__caption') > 0 || /(SMALL|STRONG|EM)/i.test(i.tagName)) &&
              (n.prevent = !1);
          }),
          d('bindEvents', function() {
            t.bind(i, 'pswpTap click', E),
              t.bind(e.scrollWrap, 'pswpTap', x.onGlobalTap),
              e.likelyTouchDevice || t.bind(e.scrollWrap, 'mouseover', x.onMouseOver);
          }),
          d('unbindEvents', function() {
            T || I(),
              y && clearInterval(y),
              t.unbind(document, 'mouseout', L),
              t.unbind(document, 'mousemove', M),
              t.unbind(i, 'pswpTap click', E),
              t.unbind(e.scrollWrap, 'pswpTap', x.onGlobalTap),
              t.unbind(e.scrollWrap, 'mouseover', x.onMouseOver),
              n &&
                (t.unbind(document, n.eventK, x.updateFullscreen),
                n.isFullscreen() && ((v.hideAnimationDuration = 0), n.exit()),
                (n = null));
          }),
          d('destroy', function() {
            v.captionEl && (r && i.removeChild(r), t.removeClass(o, 'pswp__caption--empty')),
              l && (l.children[0].onclick = null),
              t.removeClass(i, 'pswp__ui--over-close'),
              t.addClass(i, 'pswp__ui--hidden'),
              x.setIdle(!1);
          }),
          v.showAnimationDuration || t.removeClass(i, 'pswp__ui--hidden'),
          d('initialZoomIn', function() {
            v.showAnimationDuration && t.removeClass(i, 'pswp__ui--hidden');
          }),
          d('initialZoomOut', function() {
            t.addClass(i, 'pswp__ui--hidden');
          }),
          d('parseVerticalMargin', j),
          (function() {
            var e,
              n,
              o,
              r = function(i) {
                if (i)
                  for (var r = i.length, a = 0; r > a; a++) {
                    n = (e = i[a]).className;
                    for (var s = 0; s < P.length; s++)
                      n.indexOf('pswp__' + (o = P[s]).name) > -1 &&
                        (v[o.option]
                          ? (t.removeClass(e, 'pswp__element--disabled'), o.onInit && o.onInit(e))
                          : t.addClass(e, 'pswp__element--disabled'));
                  }
              };
            r(i.children);
            var a = t.getChildByClass(i, 'pswp__top-bar');
            a && r(a.children);
          })(),
          v.shareEl && s && l && (T = !0),
          A(),
          v.timeToIdle &&
            d('mouseUsed', function() {
              t.bind(document, 'mousemove', M),
                t.bind(document, 'mouseout', L),
                (y = setInterval(function() {
                  2 == ++R && x.setIdle(!0);
                }, v.timeToIdle / 2));
            }),
          v.fullscreenEl &&
            !t.features.isOldAndroid &&
            (n || (n = x.getFullscreenAPI()),
            n
              ? (t.bind(document, n.eventK, x.updateFullscreen),
                x.updateFullscreen(),
                t.addClass(e.template, 'pswp--supports-fs'))
              : t.removeClass(e.template, 'pswp--supports-fs')),
          v.preloaderEl &&
            (F(!0),
            d('beforeChange', function() {
              clearTimeout(h),
                (h = setTimeout(function() {
                  e.currItem && e.currItem.loading
                    ? (!e.allowProgressiveImg() || (e.currItem.img && !e.currItem.img.naturalWidth)) && F(!1)
                    : F(!0);
                }, v.loadingIndicatorDelay));
            }),
            d('imageLoadComplete', function(t, n) {
              e.currItem === n && F(!0);
            }));
      }),
        (x.setIdle = function(e) {
          (u = e), _(i, 'ui--idle', e);
        }),
        (x.update = function() {
          C && e.currItem
            ? (x.updateIndexIndicator(),
              v.captionEl && (v.addCaptionHTMLFn(e.currItem, o), _(o, 'caption--empty', !e.currItem.title)),
              (w = !0))
            : (w = !1),
            T || I(),
            A();
        }),
        (x.updateFullscreen = function(i) {
          i &&
            setTimeout(function() {
              e.setScrollOffset(0, t.getScrollY());
            }, 50),
            t[(n.isFullscreen() ? 'add' : 'remove') + 'Class'](e.template, 'pswp--fs');
        }),
        (x.updateIndexIndicator = function() {
          v.counterEl && (a.innerHTML = e.getCurrentIndex() + 1 + v.indexIndicatorSep + v.getNumItemsFn());
        }),
        (x.onGlobalTap = function(n) {
          var i = (n = n || window.event).target || n.srcElement;
          if (!g)
            if (n.detail && 'mouse' === n.detail.pointerType) {
              if (O(i)) return void e.close();
              t.hasClass(i, 'pswp__img') &&
                (1 === e.getZoomLevel() && e.getZoomLevel() <= e.currItem.fitRatio
                  ? v.clickToCloseNonZoomable && e.close()
                  : e.toggleDesktopZoom(n.detail.releasePoint));
            } else if (
              (v.tapToToggleControls && (C ? x.hideControls() : x.showControls()),
              v.tapToClose && (t.hasClass(i, 'pswp__img') || O(i)))
            )
              return void e.close();
        }),
        (x.onMouseOver = function(e) {
          (e = e || window.event), _(i, 'ui--over-close', O(e.target || e.srcElement));
        }),
        (x.hideControls = function() {
          t.addClass(i, 'pswp__ui--hidden'), (C = !1);
        }),
        (x.showControls = function() {
          (C = !0), w || x.update(), t.removeClass(i, 'pswp__ui--hidden');
        }),
        (x.supportsFullscreen = function() {
          var e = document;
          return !!(e.exitFullscreen || e.mozCancelFullScreen || e.webkitExitFullscreen || e.msExitFullscreen);
        }),
        (x.getFullscreenAPI = function() {
          var t,
            n = document.documentElement,
            i = 'fullscreenchange';
          return (
            n.requestFullscreen
              ? (t = { enterK: 'requestFullscreen', exitK: 'exitFullscreen', elementK: 'fullscreenElement', eventK: i })
              : n.mozRequestFullScreen
              ? (t = {
                  enterK: 'mozRequestFullScreen',
                  exitK: 'mozCancelFullScreen',
                  elementK: 'mozFullScreenElement',
                  eventK: 'moz' + i
                })
              : n.webkitRequestFullscreen
              ? (t = {
                  enterK: 'webkitRequestFullscreen',
                  exitK: 'webkitExitFullscreen',
                  elementK: 'webkitFullscreenElement',
                  eventK: 'webkit' + i
                })
              : n.msRequestFullscreen &&
                (t = {
                  enterK: 'msRequestFullscreen',
                  exitK: 'msExitFullscreen',
                  elementK: 'msFullscreenElement',
                  eventK: 'MSFullscreenChange'
                }),
            t &&
              ((t.enter = function() {
                return (
                  (c = v.closeOnScroll),
                  (v.closeOnScroll = !1),
                  'webkitRequestFullscreen' !== this.enterK
                    ? e.template[this.enterK]()
                    : void e.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
                );
              }),
              (t.exit = function() {
                return (v.closeOnScroll = c), document[this.exitK]();
              }),
              (t.isFullscreen = function() {
                return document[this.elementK];
              })),
            t
          );
        });
    };
  }),
  (function(e) {
    if ('object' == typeof exports && 'undefined' != typeof module) module.exports = e();
    else if ('function' == typeof define && define.amd) define([], e);
    else {
      var t;
      'undefined' != typeof window
        ? (t = window)
        : 'undefined' != typeof global
        ? (t = global)
        : 'undefined' != typeof self && (t = self),
        (t.Countdown = e());
    }
  })(function() {
    return (function e(t, n, i) {
      function o(a, s) {
        if (!n[a]) {
          if (!t[a]) {
            var l = 'function' == typeof require && require;
            if (!s && l) return l(a, !0);
            if (r) return r(a, !0);
            var c = new Error("Cannot find module '" + a + "'");
            throw ((c.code = 'MODULE_NOT_FOUND'), c);
          }
          var u = (n[a] = { exports: {} });
          t[a][0].call(
            u.exports,
            function(e) {
              return o(t[a][1][e] || e);
            },
            u,
            u.exports,
            e,
            t,
            n,
            i
          );
        }
        return n[a].exports;
      }
      for (var r = 'function' == typeof require && require, a = 0; a < i.length; a++) o(i[a]);
      return o;
    })(
      {
        1: [
          function(e, t, n) {
            var i = {
              date: 'June 7, 2087 15:03:25',
              refresh: 1e3,
              offset: 0,
              onEnd: function() {},
              render: function(e) {
                this.el.innerHTML =
                  e.years +
                  ' years, ' +
                  e.days +
                  ' days, ' +
                  this.leadingZeros(e.hours) +
                  ' hours, ' +
                  this.leadingZeros(e.min) +
                  ' min and ' +
                  this.leadingZeros(e.sec) +
                  ' sec';
              }
            };
            t.exports = function(e, t) {
              (this.el = e),
                (this.options = {}),
                (this.interval = !1),
                (this.mergeOptions = function(e) {
                  for (var t in i)
                    i.hasOwnProperty(t) &&
                      ((this.options[t] = void 0 !== e[t] ? e[t] : i[t]),
                      'date' === t &&
                        'object' != typeof this.options.date &&
                        (this.options.date = new Date(this.options.date)),
                      'function' == typeof this.options[t] && (this.options[t] = this.options[t].bind(this)));
                  'object' != typeof this.options.date && (this.options.date = new Date(this.options.date));
                }.bind(this)),
                this.mergeOptions(t),
                (this.getDiffDate = function() {
                  var e = (this.options.date.getTime() - Date.now() + this.options.offset) / 1e3,
                    t = { years: 0, days: 0, hours: 0, min: 0, sec: 0, millisec: 0 };
                  return e <= 0
                    ? (this.interval && (this.stop(), this.options.onEnd()), t)
                    : (e >= 31557600 && ((t.years = Math.floor(e / 31557600)), (e -= 365.25 * t.years * 86400)),
                      e >= 86400 && ((t.days = Math.floor(e / 86400)), (e -= 86400 * t.days)),
                      e >= 3600 && ((t.hours = Math.floor(e / 3600)), (e -= 3600 * t.hours)),
                      e >= 60 && ((t.min = Math.floor(e / 60)), (e -= 60 * t.min)),
                      (t.sec = Math.round(e)),
                      (t.millisec = (e % 1) * 1e3),
                      t);
                }.bind(this)),
                (this.leadingZeros = function(e, t) {
                  return (t = t || 2), (e = String(e)).length > t ? e : (Array(t + 1).join('0') + e).substr(-t);
                }),
                (this.update = function(e) {
                  return 'object' != typeof e && (e = new Date(e)), (this.options.date = e), this.render(), this;
                }.bind(this)),
                (this.stop = function() {
                  return this.interval && (clearInterval(this.interval), (this.interval = !1)), this;
                }.bind(this)),
                (this.render = function() {
                  return this.options.render(this.getDiffDate()), this;
                }.bind(this)),
                (this.start = function() {
                  if (!this.interval)
                    return (
                      this.render(),
                      this.options.refresh && (this.interval = setInterval(this.render, this.options.refresh)),
                      this
                    );
                }.bind(this)),
                (this.updateOffset = function(e) {
                  return (this.options.offset = e), this;
                }.bind(this)),
                (this.restart = function(e) {
                  return this.mergeOptions(e), (this.interval = !1), this.start(), this;
                }.bind(this)),
                this.start();
            };
          },
          {}
        ],
        2: [
          function(e, t, n) {
            var i = e('./countdown.js');
            (jQuery.fn.countdown = function(e) {
              return $.each(this, function(t, n) {
                var o = $(n);
                o.data('countdown') || (o.data('date') && (e.date = o.data('date')), o.data('countdown', new i(n, e)));
              });
            }),
              (t.exports = i);
          },
          { './countdown.js': 1 }
        ]
      },
      {},
      [2]
    )(2);
  });
var pJS = function(e, t) {
  var n = document.querySelector('#' + e + ' > .particles-js-canvas-el');
  this.pJS = {
    canvas: { el: n, w: n.offsetWidth, h: n.offsetHeight },
    particles: {
      number: { value: 400, density: { enable: !0, value_area: 800 } },
      color: { value: '#fff' },
      shape: {
        type: 'circle',
        stroke: { width: 0, color: '#ff0000' },
        polygon: { nb_sides: 5 },
        image: { src: '', width: 100, height: 100 }
      },
      opacity: { value: 1, random: !1, anim: { enable: !1, speed: 2, opacity_min: 0, sync: !1 } },
      size: { value: 20, random: !1, anim: { enable: !1, speed: 20, size_min: 0, sync: !1 } },
      line_linked: { enable: !0, distance: 100, color: '#fff', opacity: 1, width: 1 },
      move: {
        enable: !0,
        speed: 2,
        direction: 'none',
        random: !1,
        straight: !1,
        out_mode: 'out',
        bounce: !1,
        attract: { enable: !1, rotateX: 3e3, rotateY: 3e3 }
      },
      array: []
    },
    interactivity: {
      detect_on: 'canvas',
      events: { onhover: { enable: !0, mode: 'grab' }, onclick: { enable: !0, mode: 'push' }, resize: !0 },
      modes: {
        grab: { distance: 100, line_linked: { opacity: 1 } },
        bubble: { distance: 200, size: 80, duration: 0.4 },
        repulse: { distance: 200, duration: 0.4 },
        push: { particles_nb: 4 },
        remove: { particles_nb: 2 }
      },
      mouse: {}
    },
    retina_detect: !1,
    fn: { interact: {}, modes: {}, vendors: {} },
    tmp: {}
  };
  var i = this.pJS;
  t && Object.deepExtend(i, t),
    (i.tmp.obj = {
      size_value: i.particles.size.value,
      size_anim_speed: i.particles.size.anim.speed,
      move_speed: i.particles.move.speed,
      line_linked_distance: i.particles.line_linked.distance,
      line_linked_width: i.particles.line_linked.width,
      mode_grab_distance: i.interactivity.modes.grab.distance,
      mode_bubble_distance: i.interactivity.modes.bubble.distance,
      mode_bubble_size: i.interactivity.modes.bubble.size,
      mode_repulse_distance: i.interactivity.modes.repulse.distance
    }),
    (i.fn.retinaInit = function() {
      i.retina_detect && window.devicePixelRatio > 1
        ? ((i.canvas.pxratio = window.devicePixelRatio), (i.tmp.retina = !0))
        : ((i.canvas.pxratio = 1), (i.tmp.retina = !1)),
        (i.canvas.w = i.canvas.el.offsetWidth * i.canvas.pxratio),
        (i.canvas.h = i.canvas.el.offsetHeight * i.canvas.pxratio),
        (i.particles.size.value = i.tmp.obj.size_value * i.canvas.pxratio),
        (i.particles.size.anim.speed = i.tmp.obj.size_anim_speed * i.canvas.pxratio),
        (i.particles.move.speed = i.tmp.obj.move_speed * i.canvas.pxratio),
        (i.particles.line_linked.distance = i.tmp.obj.line_linked_distance * i.canvas.pxratio),
        (i.interactivity.modes.grab.distance = i.tmp.obj.mode_grab_distance * i.canvas.pxratio),
        (i.interactivity.modes.bubble.distance = i.tmp.obj.mode_bubble_distance * i.canvas.pxratio),
        (i.particles.line_linked.width = i.tmp.obj.line_linked_width * i.canvas.pxratio),
        (i.interactivity.modes.bubble.size = i.tmp.obj.mode_bubble_size * i.canvas.pxratio),
        (i.interactivity.modes.repulse.distance = i.tmp.obj.mode_repulse_distance * i.canvas.pxratio);
    }),
    (i.fn.canvasInit = function() {
      i.canvas.ctx = i.canvas.el.getContext('2d');
    }),
    (i.fn.canvasSize = function() {
      (i.canvas.el.width = i.canvas.w),
        (i.canvas.el.height = i.canvas.h),
        i &&
          i.interactivity.events.resize &&
          window.addEventListener('resize', function() {
            (i.canvas.w = i.canvas.el.offsetWidth),
              (i.canvas.h = i.canvas.el.offsetHeight),
              i.tmp.retina && ((i.canvas.w *= i.canvas.pxratio), (i.canvas.h *= i.canvas.pxratio)),
              (i.canvas.el.width = i.canvas.w),
              (i.canvas.el.height = i.canvas.h),
              i.particles.move.enable ||
                (i.fn.particlesEmpty(),
                i.fn.particlesCreate(),
                i.fn.particlesDraw(),
                i.fn.vendors.densityAutoParticles()),
              i.fn.vendors.densityAutoParticles();
          });
    }),
    (i.fn.canvasPaint = function() {
      i.canvas.ctx.fillRect(0, 0, i.canvas.w, i.canvas.h);
    }),
    (i.fn.canvasClear = function() {
      i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h);
    }),
    (i.fn.particle = function(e, t, n) {
      if (
        ((this.radius = (i.particles.size.random ? Math.random() : 1) * i.particles.size.value),
        i.particles.size.anim.enable &&
          ((this.size_status = !1),
          (this.vs = i.particles.size.anim.speed / 100),
          i.particles.size.anim.sync || (this.vs = this.vs * Math.random())),
        (this.x = n ? n.x : Math.random() * i.canvas.w),
        (this.y = n ? n.y : Math.random() * i.canvas.h),
        this.x > i.canvas.w - 2 * this.radius
          ? (this.x = this.x - this.radius)
          : this.x < 2 * this.radius && (this.x = this.x + this.radius),
        this.y > i.canvas.h - 2 * this.radius
          ? (this.y = this.y - this.radius)
          : this.y < 2 * this.radius && (this.y = this.y + this.radius),
        i.particles.move.bounce && i.fn.vendors.checkOverlap(this, n),
        (this.color = {}),
        'object' == typeof e.value)
      )
        if (e.value instanceof Array) {
          var o = e.value[Math.floor(Math.random() * i.particles.color.value.length)];
          this.color.rgb = hexToRgb(o);
        } else
          null != e.value.r &&
            null != e.value.g &&
            null != e.value.b &&
            (this.color.rgb = { r: e.value.r, g: e.value.g, b: e.value.b }),
            null != e.value.h &&
              null != e.value.s &&
              null != e.value.l &&
              (this.color.hsl = { h: e.value.h, s: e.value.s, l: e.value.l });
      else
        'random' == e.value
          ? (this.color.rgb = {
              r: Math.floor(256 * Math.random()) + 0,
              g: Math.floor(256 * Math.random()) + 0,
              b: Math.floor(256 * Math.random()) + 0
            })
          : 'string' == typeof e.value && ((this.color = e), (this.color.rgb = hexToRgb(this.color.value)));
      (this.opacity = (i.particles.opacity.random ? Math.random() : 1) * i.particles.opacity.value),
        i.particles.opacity.anim.enable &&
          ((this.opacity_status = !1),
          (this.vo = i.particles.opacity.anim.speed / 100),
          i.particles.opacity.anim.sync || (this.vo = this.vo * Math.random()));
      var r = {};
      switch (i.particles.move.direction) {
        case 'top':
          r = { x: 0, y: -1 };
          break;
        case 'top-right':
          r = { x: 0.5, y: -0.5 };
          break;
        case 'right':
          r = { x: 1, y: -0 };
          break;
        case 'bottom-right':
          r = { x: 0.5, y: 0.5 };
          break;
        case 'bottom':
          r = { x: 0, y: 1 };
          break;
        case 'bottom-left':
          r = { x: -0.5, y: 1 };
          break;
        case 'left':
          r = { x: -1, y: 0 };
          break;
        case 'top-left':
          r = { x: -0.5, y: -0.5 };
          break;
        default:
          r = { x: 0, y: 0 };
      }
      i.particles.move.straight
        ? ((this.vx = r.x),
          (this.vy = r.y),
          i.particles.move.random && ((this.vx = this.vx * Math.random()), (this.vy = this.vy * Math.random())))
        : ((this.vx = r.x + Math.random() - 0.5), (this.vy = r.y + Math.random() - 0.5)),
        (this.vx_i = this.vx),
        (this.vy_i = this.vy);
      var a = i.particles.shape.type;
      if ('object' == typeof a) {
        if (a instanceof Array) {
          var s = a[Math.floor(Math.random() * a.length)];
          this.shape = s;
        }
      } else this.shape = a;
      if ('image' == this.shape) {
        var l = i.particles.shape;
        (this.img = { src: l.image.src, ratio: l.image.width / l.image.height }),
          this.img.ratio || (this.img.ratio = 1),
          'svg' == i.tmp.img_type &&
            null != i.tmp.source_svg &&
            (i.fn.vendors.createSvgImg(this), i.tmp.pushing && (this.img.loaded = !1));
      }
    }),
    (i.fn.particle.prototype.draw = function() {
      var e = this;
      if (null != e.radius_bubble) var t = e.radius_bubble;
      else t = e.radius;
      if (null != e.opacity_bubble) var n = e.opacity_bubble;
      else n = e.opacity;
      if (e.color.rgb) var o = 'rgba(' + e.color.rgb.r + ',' + e.color.rgb.g + ',' + e.color.rgb.b + ',' + n + ')';
      else o = 'hsla(' + e.color.hsl.h + ',' + e.color.hsl.s + '%,' + e.color.hsl.l + '%,' + n + ')';
      switch (((i.canvas.ctx.fillStyle = o), i.canvas.ctx.beginPath(), e.shape)) {
        case 'circle':
          i.canvas.ctx.arc(e.x, e.y, t, 0, 2 * Math.PI, !1);
          break;
        case 'edge':
          i.canvas.ctx.rect(e.x - t, e.y - t, 2 * t, 2 * t);
          break;
        case 'triangle':
          i.fn.vendors.drawShape(i.canvas.ctx, e.x - t, e.y + t / 1.66, 2 * t, 3, 2);
          break;
        case 'polygon':
          i.fn.vendors.drawShape(
            i.canvas.ctx,
            e.x - t / (i.particles.shape.polygon.nb_sides / 3.5),
            e.y - t / 0.76,
            (2.66 * t) / (i.particles.shape.polygon.nb_sides / 3),
            i.particles.shape.polygon.nb_sides,
            1
          );
          break;
        case 'star':
          i.fn.vendors.drawShape(
            i.canvas.ctx,
            e.x - (2 * t) / (i.particles.shape.polygon.nb_sides / 4),
            e.y - t / 1.52,
            (2 * t * 2.66) / (i.particles.shape.polygon.nb_sides / 3),
            i.particles.shape.polygon.nb_sides,
            2
          );
          break;
        case 'image':
          if ('svg' == i.tmp.img_type) var r = e.img.obj;
          else r = i.tmp.img_obj;
          r && i.canvas.ctx.drawImage(r, e.x - t, e.y - t, 2 * t, (2 * t) / e.img.ratio);
      }
      i.canvas.ctx.closePath(),
        i.particles.shape.stroke.width > 0 &&
          ((i.canvas.ctx.strokeStyle = i.particles.shape.stroke.color),
          (i.canvas.ctx.lineWidth = i.particles.shape.stroke.width),
          i.canvas.ctx.stroke()),
        i.canvas.ctx.fill();
    }),
    (i.fn.particlesCreate = function() {
      for (var e = 0; e < i.particles.number.value; e++)
        i.particles.array.push(new i.fn.particle(i.particles.color, i.particles.opacity.value));
    }),
    (i.fn.particlesUpdate = function() {
      for (var e = 0; e < i.particles.array.length; e++) {
        var t = i.particles.array[e];
        if (i.particles.move.enable) {
          var n = i.particles.move.speed / 2;
          (t.x += t.vx * n), (t.y += t.vy * n);
        }
        if (
          (i.particles.opacity.anim.enable &&
            (1 == t.opacity_status
              ? (t.opacity >= i.particles.opacity.value && (t.opacity_status = !1), (t.opacity += t.vo))
              : (t.opacity <= i.particles.opacity.anim.opacity_min && (t.opacity_status = !0), (t.opacity -= t.vo)),
            t.opacity < 0 && (t.opacity = 0)),
          i.particles.size.anim.enable &&
            (1 == t.size_status
              ? (t.radius >= i.particles.size.value && (t.size_status = !1), (t.radius += t.vs))
              : (t.radius <= i.particles.size.anim.size_min && (t.size_status = !0), (t.radius -= t.vs)),
            t.radius < 0 && (t.radius = 0)),
          'bounce' == i.particles.move.out_mode)
        )
          var o = { x_left: t.radius, x_right: i.canvas.w, y_top: t.radius, y_bottom: i.canvas.h };
        else
          o = { x_left: -t.radius, x_right: i.canvas.w + t.radius, y_top: -t.radius, y_bottom: i.canvas.h + t.radius };
        switch (
          (t.x - t.radius > i.canvas.w
            ? ((t.x = o.x_left), (t.y = Math.random() * i.canvas.h))
            : t.x + t.radius < 0 && ((t.x = o.x_right), (t.y = Math.random() * i.canvas.h)),
          t.y - t.radius > i.canvas.h
            ? ((t.y = o.y_top), (t.x = Math.random() * i.canvas.w))
            : t.y + t.radius < 0 && ((t.y = o.y_bottom), (t.x = Math.random() * i.canvas.w)),
          i.particles.move.out_mode)
        ) {
          case 'bounce':
            (t.x + t.radius > i.canvas.w || t.x - t.radius < 0) && (t.vx = -t.vx),
              (t.y + t.radius > i.canvas.h || t.y - t.radius < 0) && (t.vy = -t.vy);
        }
        if (
          (isInArray('grab', i.interactivity.events.onhover.mode) && i.fn.modes.grabParticle(t),
          (isInArray('bubble', i.interactivity.events.onhover.mode) ||
            isInArray('bubble', i.interactivity.events.onclick.mode)) &&
            i.fn.modes.bubbleParticle(t),
          (isInArray('repulse', i.interactivity.events.onhover.mode) ||
            isInArray('repulse', i.interactivity.events.onclick.mode)) &&
            i.fn.modes.repulseParticle(t),
          i.particles.line_linked.enable || i.particles.move.attract.enable)
        )
          for (var r = e + 1; r < i.particles.array.length; r++) {
            var a = i.particles.array[r];
            i.particles.line_linked.enable && i.fn.interact.linkParticles(t, a),
              i.particles.move.attract.enable && i.fn.interact.attractParticles(t, a),
              i.particles.move.bounce && i.fn.interact.bounceParticles(t, a);
          }
      }
    }),
    (i.fn.particlesDraw = function() {
      i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h), i.fn.particlesUpdate();
      for (var e = 0; e < i.particles.array.length; e++) i.particles.array[e].draw();
    }),
    (i.fn.particlesEmpty = function() {
      i.particles.array = [];
    }),
    (i.fn.particlesRefresh = function() {
      cancelRequestAnimFrame(i.fn.checkAnimFrame),
        cancelRequestAnimFrame(i.fn.drawAnimFrame),
        (i.tmp.source_svg = void 0),
        (i.tmp.img_obj = void 0),
        (i.tmp.count_svg = 0),
        i.fn.particlesEmpty(),
        i.fn.canvasClear(),
        i.fn.vendors.start();
    }),
    (i.fn.interact.linkParticles = function(e, t) {
      var n = e.x - t.x,
        o = e.y - t.y,
        r = Math.sqrt(n * n + o * o);
      if (r <= i.particles.line_linked.distance) {
        var a =
          i.particles.line_linked.opacity -
          r / (1 / i.particles.line_linked.opacity) / i.particles.line_linked.distance;
        if (a > 0) {
          var s = i.particles.line_linked.color_rgb_line;
          (i.canvas.ctx.strokeStyle = 'rgba(' + s.r + ',' + s.g + ',' + s.b + ',' + a + ')'),
            (i.canvas.ctx.lineWidth = i.particles.line_linked.width),
            i.canvas.ctx.beginPath(),
            i.canvas.ctx.moveTo(e.x, e.y),
            i.canvas.ctx.lineTo(t.x, t.y),
            i.canvas.ctx.stroke(),
            i.canvas.ctx.closePath();
        }
      }
    }),
    (i.fn.interact.attractParticles = function(e, t) {
      var n = e.x - t.x,
        o = e.y - t.y;
      if (Math.sqrt(n * n + o * o) <= i.particles.line_linked.distance) {
        var r = n / (1e3 * i.particles.move.attract.rotateX),
          a = o / (1e3 * i.particles.move.attract.rotateY);
        (e.vx -= r), (e.vy -= a), (t.vx += r), (t.vy += a);
      }
    }),
    (i.fn.interact.bounceParticles = function(e, t) {
      var n = e.x - t.x,
        i = e.y - t.y,
        o = Math.sqrt(n * n + i * i);
      e.radius + t.radius >= o && ((e.vx = -e.vx), (e.vy = -e.vy), (t.vx = -t.vx), (t.vy = -t.vy));
    }),
    (i.fn.modes.pushParticles = function(e, t) {
      i.tmp.pushing = !0;
      for (var n = 0; e > n; n++)
        i.particles.array.push(
          new i.fn.particle(i.particles.color, i.particles.opacity.value, {
            x: t ? t.pos_x : Math.random() * i.canvas.w,
            y: t ? t.pos_y : Math.random() * i.canvas.h
          })
        ),
          n == e - 1 && (i.particles.move.enable || i.fn.particlesDraw(), (i.tmp.pushing = !1));
    }),
    (i.fn.modes.removeParticles = function(e) {
      i.particles.array.splice(0, e), i.particles.move.enable || i.fn.particlesDraw();
    }),
    (i.fn.modes.bubbleParticle = function(e) {
      function t() {
        (e.opacity_bubble = e.opacity), (e.radius_bubble = e.radius);
      }
      function n(t, n, o, r, a) {
        if (t != n)
          if (i.tmp.bubble_duration_end)
            null != o &&
              ((l = t + (t - (r - (u * (r - t)) / i.interactivity.modes.bubble.duration))),
              'size' == a && (e.radius_bubble = l),
              'opacity' == a && (e.opacity_bubble = l));
          else if (c <= i.interactivity.modes.bubble.distance) {
            if (null != o) var s = o;
            else s = r;
            if (s != t) {
              var l = r - (u * (r - t)) / i.interactivity.modes.bubble.duration;
              'size' == a && (e.radius_bubble = l), 'opacity' == a && (e.opacity_bubble = l);
            }
          } else 'size' == a && (e.radius_bubble = void 0), 'opacity' == a && (e.opacity_bubble = void 0);
      }
      if (i.interactivity.events.onhover.enable && isInArray('bubble', i.interactivity.events.onhover.mode)) {
        var o = e.x - i.interactivity.mouse.pos_x,
          r = e.y - i.interactivity.mouse.pos_y,
          a = 1 - (c = Math.sqrt(o * o + r * r)) / i.interactivity.modes.bubble.distance;
        if (c <= i.interactivity.modes.bubble.distance) {
          if (a >= 0 && 'mousemove' == i.interactivity.status) {
            if (i.interactivity.modes.bubble.size != i.particles.size.value)
              if (i.interactivity.modes.bubble.size > i.particles.size.value)
                (s = e.radius + i.interactivity.modes.bubble.size * a) >= 0 && (e.radius_bubble = s);
              else {
                var s;
                e.radius_bubble = (s = e.radius - (e.radius - i.interactivity.modes.bubble.size) * a) > 0 ? s : 0;
              }
            if (i.interactivity.modes.bubble.opacity != i.particles.opacity.value)
              if (i.interactivity.modes.bubble.opacity > i.particles.opacity.value)
                (l = i.interactivity.modes.bubble.opacity * a) > e.opacity &&
                  l <= i.interactivity.modes.bubble.opacity &&
                  (e.opacity_bubble = l);
              else {
                var l;
                (l = e.opacity - (i.particles.opacity.value - i.interactivity.modes.bubble.opacity) * a) < e.opacity &&
                  l >= i.interactivity.modes.bubble.opacity &&
                  (e.opacity_bubble = l);
              }
          }
        } else t();
        'mouseleave' == i.interactivity.status && t();
      } else if (i.interactivity.events.onclick.enable && isInArray('bubble', i.interactivity.events.onclick.mode)) {
        if (i.tmp.bubble_clicking) {
          (o = e.x - i.interactivity.mouse.click_pos_x), (r = e.y - i.interactivity.mouse.click_pos_y);
          var c = Math.sqrt(o * o + r * r),
            u = (new Date().getTime() - i.interactivity.mouse.click_time) / 1e3;
          u > i.interactivity.modes.bubble.duration && (i.tmp.bubble_duration_end = !0),
            u > 2 * i.interactivity.modes.bubble.duration &&
              ((i.tmp.bubble_clicking = !1), (i.tmp.bubble_duration_end = !1));
        }
        i.tmp.bubble_clicking &&
          (n(i.interactivity.modes.bubble.size, i.particles.size.value, e.radius_bubble, e.radius, 'size'),
          n(i.interactivity.modes.bubble.opacity, i.particles.opacity.value, e.opacity_bubble, e.opacity, 'opacity'));
      }
    }),
    (i.fn.modes.repulseParticle = function(e) {
      if (
        i.interactivity.events.onhover.enable &&
        isInArray('repulse', i.interactivity.events.onhover.mode) &&
        'mousemove' == i.interactivity.status
      ) {
        var t = e.x - i.interactivity.mouse.pos_x,
          n = e.y - i.interactivity.mouse.pos_y,
          o = Math.sqrt(t * t + n * n),
          r = { x: t / o, y: n / o },
          a = clamp(
            (1 / (l = i.interactivity.modes.repulse.distance)) * (-1 * Math.pow(o / l, 2) + 1) * l * 100,
            0,
            50
          ),
          s = { x: e.x + r.x * a, y: e.y + r.y * a };
        'bounce' == i.particles.move.out_mode
          ? (s.x - e.radius > 0 && s.x + e.radius < i.canvas.w && (e.x = s.x),
            s.y - e.radius > 0 && s.y + e.radius < i.canvas.h && (e.y = s.y))
          : ((e.x = s.x), (e.y = s.y));
      } else if (i.interactivity.events.onclick.enable && isInArray('repulse', i.interactivity.events.onclick.mode))
        if (
          (i.tmp.repulse_finish ||
            (i.tmp.repulse_count++, i.tmp.repulse_count == i.particles.array.length && (i.tmp.repulse_finish = !0)),
          i.tmp.repulse_clicking)
        ) {
          var l = Math.pow(i.interactivity.modes.repulse.distance / 6, 3),
            c = i.interactivity.mouse.click_pos_x - e.x,
            u = i.interactivity.mouse.click_pos_y - e.y,
            d = c * c + u * u,
            p = (-l / d) * 1;
          l >= d &&
            (function() {
              var t = Math.atan2(u, c);
              if (((e.vx = p * Math.cos(t)), (e.vy = p * Math.sin(t)), 'bounce' == i.particles.move.out_mode)) {
                var n = { x: e.x + e.vx, y: e.y + e.vy };
                (n.x + e.radius > i.canvas.w || n.x - e.radius < 0) && (e.vx = -e.vx),
                  (n.y + e.radius > i.canvas.h || n.y - e.radius < 0) && (e.vy = -e.vy);
              }
            })();
        } else 0 == i.tmp.repulse_clicking && ((e.vx = e.vx_i), (e.vy = e.vy_i));
    }),
    (i.fn.modes.grabParticle = function(e) {
      if (i.interactivity.events.onhover.enable && 'mousemove' == i.interactivity.status) {
        var t = e.x - i.interactivity.mouse.pos_x,
          n = e.y - i.interactivity.mouse.pos_y,
          o = Math.sqrt(t * t + n * n);
        if (o <= i.interactivity.modes.grab.distance) {
          var r =
            i.interactivity.modes.grab.line_linked.opacity -
            o / (1 / i.interactivity.modes.grab.line_linked.opacity) / i.interactivity.modes.grab.distance;
          if (r > 0) {
            var a = i.particles.line_linked.color_rgb_line;
            (i.canvas.ctx.strokeStyle = 'rgba(' + a.r + ',' + a.g + ',' + a.b + ',' + r + ')'),
              (i.canvas.ctx.lineWidth = i.particles.line_linked.width),
              i.canvas.ctx.beginPath(),
              i.canvas.ctx.moveTo(e.x, e.y),
              i.canvas.ctx.lineTo(i.interactivity.mouse.pos_x, i.interactivity.mouse.pos_y),
              i.canvas.ctx.stroke(),
              i.canvas.ctx.closePath();
          }
        }
      }
    }),
    (i.fn.vendors.eventsListeners = function() {
      (i.interactivity.el = 'window' == i.interactivity.detect_on ? window : i.canvas.el),
        (i.interactivity.events.onhover.enable || i.interactivity.events.onclick.enable) &&
          (i.interactivity.el.addEventListener('mousemove', function(e) {
            if (i.interactivity.el == window)
              var t = e.clientX,
                n = e.clientY;
            else (t = e.offsetX || e.clientX), (n = e.offsetY || e.clientY);
            (i.interactivity.mouse.pos_x = t),
              (i.interactivity.mouse.pos_y = n),
              i.tmp.retina &&
                ((i.interactivity.mouse.pos_x *= i.canvas.pxratio), (i.interactivity.mouse.pos_y *= i.canvas.pxratio)),
              (i.interactivity.status = 'mousemove');
          }),
          i.interactivity.el.addEventListener('mouseleave', function(e) {
            (i.interactivity.mouse.pos_x = null),
              (i.interactivity.mouse.pos_y = null),
              (i.interactivity.status = 'mouseleave');
          })),
        i.interactivity.events.onclick.enable &&
          i.interactivity.el.addEventListener('click', function() {
            if (
              ((i.interactivity.mouse.click_pos_x = i.interactivity.mouse.pos_x),
              (i.interactivity.mouse.click_pos_y = i.interactivity.mouse.pos_y),
              (i.interactivity.mouse.click_time = new Date().getTime()),
              i.interactivity.events.onclick.enable)
            )
              switch (i.interactivity.events.onclick.mode) {
                case 'push':
                  i.particles.move.enable || 1 == i.interactivity.modes.push.particles_nb
                    ? i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb, i.interactivity.mouse)
                    : i.interactivity.modes.push.particles_nb > 1 &&
                      i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb);
                  break;
                case 'remove':
                  i.fn.modes.removeParticles(i.interactivity.modes.remove.particles_nb);
                  break;
                case 'bubble':
                  i.tmp.bubble_clicking = !0;
                  break;
                case 'repulse':
                  (i.tmp.repulse_clicking = !0),
                    (i.tmp.repulse_count = 0),
                    (i.tmp.repulse_finish = !1),
                    setTimeout(function() {
                      i.tmp.repulse_clicking = !1;
                    }, 1e3 * i.interactivity.modes.repulse.duration);
              }
          });
    }),
    (i.fn.vendors.densityAutoParticles = function() {
      if (i.particles.number.density.enable) {
        var e = (i.canvas.el.width * i.canvas.el.height) / 1e3;
        i.tmp.retina && (e /= 2 * i.canvas.pxratio);
        var t = i.particles.array.length - (e * i.particles.number.value) / i.particles.number.density.value_area;
        0 > t ? i.fn.modes.pushParticles(Math.abs(t)) : i.fn.modes.removeParticles(t);
      }
    }),
    (i.fn.vendors.checkOverlap = function(e, t) {
      for (var n = 0; n < i.particles.array.length; n++) {
        var o = i.particles.array[n],
          r = e.x - o.x,
          a = e.y - o.y;
        Math.sqrt(r * r + a * a) <= e.radius + o.radius &&
          ((e.x = t ? t.x : Math.random() * i.canvas.w),
          (e.y = t ? t.y : Math.random() * i.canvas.h),
          i.fn.vendors.checkOverlap(e));
      }
    }),
    (i.fn.vendors.createSvgImg = function(e) {
      var t = i.tmp.source_svg.replace(/#([0-9A-F]{3,6})/gi, function(t, n, i, o) {
          if (e.color.rgb)
            var r = 'rgba(' + e.color.rgb.r + ',' + e.color.rgb.g + ',' + e.color.rgb.b + ',' + e.opacity + ')';
          else r = 'hsla(' + e.color.hsl.h + ',' + e.color.hsl.s + '%,' + e.color.hsl.l + '%,' + e.opacity + ')';
          return r;
        }),
        n = new Blob([t], { type: 'image/svg+xml;charset=utf-8' }),
        o = window.URL || window.webkitURL || window,
        r = o.createObjectURL(n),
        a = new Image();
      a.addEventListener('load', function() {
        (e.img.obj = a), (e.img.loaded = !0), o.revokeObjectURL(r), i.tmp.count_svg++;
      }),
        (a.src = r);
    }),
    (i.fn.vendors.destroypJS = function() {
      cancelAnimationFrame(i.fn.drawAnimFrame), n.remove(), (pJSDom = null);
    }),
    (i.fn.vendors.drawShape = function(e, t, n, i, o, r) {
      var a = o * r,
        s = o / r,
        l = Math.PI - (Math.PI * ((180 * (s - 2)) / s)) / 180;
      e.save(), e.beginPath(), e.translate(t, n), e.moveTo(0, 0);
      for (var c = 0; a > c; c++) e.lineTo(i, 0), e.translate(i, 0), e.rotate(l);
      e.fill(), e.restore();
    }),
    (i.fn.vendors.exportImg = function() {
      window.open(i.canvas.el.toDataURL('image/png'), '_blank');
    }),
    (i.fn.vendors.loadImg = function(e) {
      if (((i.tmp.img_error = void 0), '' != i.particles.shape.image.src))
        if ('svg' == e) {
          var t = new XMLHttpRequest();
          t.open('GET', i.particles.shape.image.src),
            (t.onreadystatechange = function(e) {
              4 == t.readyState &&
                (200 == t.status
                  ? ((i.tmp.source_svg = e.currentTarget.response), i.fn.vendors.checkBeforeDraw())
                  : (console.log('Error pJS - Image not found'), (i.tmp.img_error = !0)));
            }),
            t.send();
        } else {
          var n = new Image();
          n.addEventListener('load', function() {
            (i.tmp.img_obj = n), i.fn.vendors.checkBeforeDraw();
          }),
            (n.src = i.particles.shape.image.src);
        }
      else console.log('Error pJS - No image.src'), (i.tmp.img_error = !0);
    }),
    (i.fn.vendors.draw = function() {
      'image' == i.particles.shape.type
        ? 'svg' == i.tmp.img_type
          ? i.tmp.count_svg >= i.particles.number.value
            ? (i.fn.particlesDraw(),
              i.particles.move.enable
                ? (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw))
                : cancelRequestAnimFrame(i.fn.drawAnimFrame))
            : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw))
          : null != i.tmp.img_obj
          ? (i.fn.particlesDraw(),
            i.particles.move.enable
              ? (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw))
              : cancelRequestAnimFrame(i.fn.drawAnimFrame))
          : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw))
        : (i.fn.particlesDraw(),
          i.particles.move.enable
            ? (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw))
            : cancelRequestAnimFrame(i.fn.drawAnimFrame));
    }),
    (i.fn.vendors.checkBeforeDraw = function() {
      'image' == i.particles.shape.type
        ? 'svg' == i.tmp.img_type && null == i.tmp.source_svg
          ? (i.tmp.checkAnimFrame = requestAnimFrame(check))
          : (cancelRequestAnimFrame(i.tmp.checkAnimFrame),
            i.tmp.img_error || (i.fn.vendors.init(), i.fn.vendors.draw()))
        : (i.fn.vendors.init(), i.fn.vendors.draw());
    }),
    (i.fn.vendors.init = function() {
      i.fn.retinaInit(),
        i.fn.canvasInit(),
        i.fn.canvasSize(),
        i.fn.canvasPaint(),
        i.fn.particlesCreate(),
        i.fn.vendors.densityAutoParticles(),
        (i.particles.line_linked.color_rgb_line = hexToRgb(i.particles.line_linked.color));
    }),
    (i.fn.vendors.start = function() {
      isInArray('image', i.particles.shape.type)
        ? ((i.tmp.img_type = i.particles.shape.image.src.substr(i.particles.shape.image.src.length - 3)),
          i.fn.vendors.loadImg(i.tmp.img_type))
        : i.fn.vendors.checkBeforeDraw();
    }),
    i.fn.vendors.eventsListeners(),
    i.fn.vendors.start();
};
(Object.deepExtend = function(e, t) {
  for (var n in t)
    t[n] && t[n].constructor && t[n].constructor === Object
      ? ((e[n] = e[n] || {}), arguments.callee(e[n], t[n]))
      : (e[n] = t[n]);
  return e;
}),
  (window.requestAnimFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(e) {
      window.setTimeout(e, 1e3 / 60);
    }),
  (window.cancelRequestAnimFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    clearTimeout),
  (window.pJSDom = []),
  (window.particlesJS = function(e, t) {
    'string' != typeof e && ((t = e), (e = 'particles-js')), e || (e = 'particles-js');
    var n = document.getElementById(e),
      i = 'particles-js-canvas-el',
      o = n.getElementsByClassName(i);
    if (o.length) for (; o.length > 0; ) n.removeChild(o[0]);
    var r = document.createElement('canvas');
    (r.className = i),
      (r.style.width = '100%'),
      (r.style.height = '100%'),
      null != document.getElementById(e).appendChild(r) && pJSDom.push(new pJS(e, t));
  }),
  (window.particlesJS.load = function(e, t, n) {
    var i = new XMLHttpRequest();
    i.open('GET', t),
      (i.onreadystatechange = function(t) {
        if (4 == i.readyState)
          if (200 == i.status) {
            var o = JSON.parse(t.currentTarget.response);
            window.particlesJS(e, o), n && n();
          } else
            console.log('Error pJS - XMLHttpRequest status: ' + i.status),
              console.log('Error pJS - File config not found');
      }),
      i.send();
  }),
  (function() {
    function e(e, t) {
      return [].slice.call((t || document).querySelectorAll(e));
    }
    if (window.addEventListener) {
      var t = (window.StyleFix = {
        link: function(e) {
          var n = e.href || e.getAttribute('data-href');
          try {
            if (!n || 'stylesheet' !== e.rel || e.hasAttribute('data-noprefix')) return;
          } catch (u) {
            return;
          }
          var i,
            o = n.replace(/[^\/]+$/, ''),
            r = (/^[a-z]{3,10}:/.exec(o) || [''])[0],
            a = (/^[a-z]{3,10}:\/\/[^\/]+/.exec(o) || [''])[0],
            s = /^([^?]*)\??/.exec(n)[1],
            l = e.parentNode,
            c = new XMLHttpRequest();
          (c.onreadystatechange = function() {
            4 === c.readyState && i();
          }),
            (i = function() {
              if ((n = c.responseText) && e.parentNode && (!c.status || 400 > c.status || 600 < c.status)) {
                if (((n = t.fix(n, !0, e)), o)) {
                  var n = n.replace(/url\(\s*?((?:"|')?)(.+?)\1\s*?\)/gi, function(e, t, n) {
                      return /^([a-z]{3,10}:|#)/i.test(n)
                        ? e
                        : /^\/\//.test(n)
                        ? 'url("' + r + n + '")'
                        : /^\//.test(n)
                        ? 'url("' + a + n + '")'
                        : /^\?/.test(n)
                        ? 'url("' + s + n + '")'
                        : 'url("' + o + n + '")';
                    }),
                    i = o.replace(/([\\\^\$*+[\]?{}.=!:(|)])/g, '\\$1');
                  n = n.replace(RegExp('\\b(behavior:\\s*?url\\(\'?"?)' + i, 'gi'), '$1');
                }
                ((i = document.createElement('style')).textContent =
                  '/*# sourceURL=' +
                  e.getAttribute('href') +
                  ' */\n/*@ sourceURL=' +
                  e.getAttribute('href') +
                  ' */\n' +
                  n),
                  (i.media = e.media),
                  (i.disabled = e.disabled),
                  i.setAttribute('data-href', e.getAttribute('href')),
                  l.insertBefore(i, e),
                  l.removeChild(e),
                  (i.media = e.media);
              }
            });
          try {
            c.open('GET', n), c.send(null);
          } catch (d) {
            'undefined' != typeof XDomainRequest &&
              (((c = new XDomainRequest()).onerror = c.onprogress = function() {}),
              (c.onload = i),
              c.open('GET', n),
              c.send(null));
          }
          e.setAttribute('data-inprogress', '');
        },
        styleElement: function(e) {
          if (!e.hasAttribute('data-noprefix')) {
            var n = e.disabled;
            (e.textContent = t.fix(e.textContent, !0, e)), (e.disabled = n);
          }
        },
        styleAttribute: function(e) {
          var n = e.getAttribute('style');
          (n = t.fix(n, !1, e)), e.setAttribute('style', n);
        },
        process: function() {
          e('link[rel="stylesheet"]:not([data-inprogress])').forEach(StyleFix.link),
            e('style').forEach(StyleFix.styleElement),
            e('[style]').forEach(StyleFix.styleAttribute);
        },
        register: function(e, n) {
          (t.fixers = t.fixers || []).splice(void 0 === n ? t.fixers.length : n, 0, e);
        },
        fix: function(e, n, i) {
          for (var o = 0; o < t.fixers.length; o++) e = t.fixers[o](e, n, i) || e;
          return e;
        },
        camelCase: function(e) {
          return e
            .replace(/-([a-z])/g, function(e, t) {
              return t.toUpperCase();
            })
            .replace('-', '');
        },
        deCamelCase: function(e) {
          return e.replace(/[A-Z]/g, function(e) {
            return '-' + e.toLowerCase();
          });
        }
      });
      setTimeout(function() {
        e('link[rel="stylesheet"]').forEach(StyleFix.link);
      }, 10),
        document.addEventListener('DOMContentLoaded', StyleFix.process, !1);
    }
  })(),
  (function(e) {
    function t(e, t, i, o, r) {
      return (e = n[e]).length && ((e = RegExp(t + '(' + e.join('|') + ')' + i, 'gi')), (r = r.replace(e, o))), r;
    }
    if (window.StyleFix && window.getComputedStyle) {
      var n = (window.PrefixFree = {
        prefixCSS: function(e, i, o) {
          var r = n.prefix;
          if (
            (-1 < n.functions.indexOf('linear-gradient') &&
              (e = e.replace(/(\s|:|,)(repeating-)?linear-gradient\(\s*(-?\d*\.?\d*)deg/gi, function(e, t, n, i) {
                return t + (n || '') + 'linear-gradient(' + (90 - i) + 'deg';
              })),
            (e = t('functions', '(\\s|:|,)', '\\s*\\(', '$1' + r + '$2(', e)),
            (e = t('keywords', '(\\s|:)', '(\\s|;|\\}|$)', '$1' + r + '$2$3', e)),
            (e = t('properties', '(^|\\{|\\s|;)', '\\s*:', '$1' + r + '$2:', e)),
            n.properties.length)
          ) {
            var a = RegExp('\\b(' + n.properties.join('|') + ')(?!:)', 'gi');
            e = t(
              'valueProperties',
              '\\b',
              ':(.+?);',
              function(e) {
                return e.replace(a, r + '$1');
              },
              e
            );
          }
          return (
            i &&
              ((e = t('selectors', '', '\\b', n.prefixSelector, e)), (e = t('atrules', '@', '\\b', '@' + r + '$1', e))),
            (e = e.replace(RegExp('-' + r, 'g'), '-')).replace(/-\*-(?=[a-z]+)/gi, n.prefix)
          );
        },
        property: function(e) {
          return (0 <= n.properties.indexOf(e) ? n.prefix : '') + e;
        },
        value: function(e, i) {
          return (
            (e = t('functions', '(^|\\s|,)', '\\s*\\(', '$1' + n.prefix + '$2(', e)),
            (e = t('keywords', '(^|\\s)', '(\\s|$)', '$1' + n.prefix + '$2$3', e)),
            0 <= n.valueProperties.indexOf(i) &&
              (e = t('properties', '(^|\\s|,)', '($|\\s|,)', '$1' + n.prefix + '$2$3', e)),
            e
          );
        },
        prefixSelector: function(e) {
          return e.replace(/^:{1,2}/, function(e) {
            return e + n.prefix;
          });
        },
        prefixProperty: function(e, t) {
          var i = n.prefix + e;
          return t ? StyleFix.camelCase(i) : i;
        }
      });
      !(function() {
        var e,
          t,
          i = {},
          o = [],
          r = getComputedStyle(document.documentElement, null),
          a = document.createElement('div').style,
          s = function(e) {
            if ('-' === e.charAt(0)) {
              o.push(e);
              var t = (e = e.split('-'))[1];
              for (i[t] = ++i[t] || 1; 3 < e.length; )
                e.pop(), (t = e.join('-')), StyleFix.camelCase(t) in a && -1 === o.indexOf(t) && o.push(t);
            }
          };
        if (0 < r.length) for (var l = 0; l < r.length; l++) s(r[l]);
        else for (var c in r) s(StyleFix.deCamelCase(c));
        for (t in ((l = 0), i)) l < (r = i[t]) && ((e = t), (l = r));
        for (
          n.prefix = '-' + e + '-', n.Prefix = StyleFix.camelCase(n.prefix), n.properties = [], l = 0;
          l < o.length;
          l++
        )
          0 === (c = o[l]).indexOf(n.prefix) &&
            ((e = c.slice(n.prefix.length)), StyleFix.camelCase(e) in a || n.properties.push(e));
        'Ms' == n.Prefix &&
          !('transform' in a) &&
          !('MsTransform' in a) &&
          'msTransform' in a &&
          n.properties.push('transform', 'transform-origin'),
          n.properties.sort();
      })(),
        (function() {
          function e(e, t) {
            return (r[t] = ''), (r[t] = e), !!r[t];
          }
          var t = {
            'linear-gradient': { property: 'backgroundImage', params: 'red, teal' },
            calc: { property: 'width', params: '1px + 5%' },
            element: { property: 'backgroundImage', params: '#foo' },
            'cross-fade': { property: 'backgroundImage', params: 'url(a.png), url(b.png), 50%' }
          };
          t['repeating-linear-gradient'] = t['repeating-radial-gradient'] = t['radial-gradient'] = t['linear-gradient'];
          var i = {
            initial: 'color',
            'zoom-in': 'cursor',
            'zoom-out': 'cursor',
            box: 'display',
            flexbox: 'display',
            'inline-flexbox': 'display',
            flex: 'display',
            'inline-flex': 'display',
            grid: 'display',
            'inline-grid': 'display',
            'max-content': 'width',
            'min-content': 'width',
            'fit-content': 'width',
            'fill-available': 'width'
          };
          (n.functions = []), (n.keywords = []);
          var o,
            r = document.createElement('div').style;
          for (o in t) {
            var a,
              s = (a = t[o]).property;
            !e((a = o + '(' + a.params + ')'), s) && e(n.prefix + a, s) && n.functions.push(o);
          }
          for (var l in i) !e(l, (s = i[l])) && e(n.prefix + l, s) && n.keywords.push(l);
        })(),
        (function() {
          function t(e) {
            return (a.textContent = e + '{}'), !!a.sheet.cssRules.length;
          }
          var i = { ':read-only': null, ':read-write': null, ':any-link': null, '::selection': null },
            o = { keyframes: 'name', viewport: null, document: 'regexp(".")' };
          (n.selectors = []), (n.atrules = []);
          var r,
            a = e.appendChild(document.createElement('style'));
          for (r in i) {
            var s = r + (i[r] ? '(' + i[r] + ')' : '');
            !t(s) && t(n.prefixSelector(s)) && n.selectors.push(r);
          }
          for (var l in o) !t('@' + (s = l + ' ' + (o[l] || ''))) && t('@' + n.prefix + s) && n.atrules.push(l);
          e.removeChild(a);
        })(),
        (n.valueProperties = ['transition', 'transition-property']),
        (e.className += ' ' + n.prefix),
        StyleFix.register(n.prefixCSS);
    }
  })(document.documentElement),
  $(document).ready(function() {
    'use strict';
    var e = /iPhone|iPad|iPod|Android|BlackBerry|BB10|Silk|Mobi/i.test(self._navigator && self._navigator.userAgent),
      t = !!(
        'ontouchend' in window ||
        (self._navigator && self._navigator.maxTouchPoints > 0) ||
        (self._navigator && self._navigator.msMaxTouchPoints > 0)
      ),
      n = $('.loader');
    n.length &&
      $(window).on('load', function() {
        n.addClass('fade-out'),
          setTimeout(function() {
            n.hide();
          }, 1e3);
      });
    var i = $('.count-down');
    if (i.length) {
      var o = new Date(i.data('end-date'));
      i.countdown({
        date: o,
        render: function(e) {
          $(this.el).html(
            '<div class="cd-row"><div><h1>' +
              this.leadingZeros(e.days, 3) +
              '</h1><p>days</p></div><div><h1>' +
              this.leadingZeros(e.hours, 2) +
              '</h1><p>hrs</p></div></div><div class="cd-row"><div><h1>' +
              this.leadingZeros(e.min, 2) +
              '</h1><p>min</p></div><div><h1>' +
              this.leadingZeros(e.sec, 2) +
              '</h1><p>sec</p></div></div>'
          );
        }
      });
    }
    function r(e) {
      $('html, body')
        .stop()
        .animate(
          { scrollTop: $(e).offset().top },
          { duration: 1e3, specialEasing: { width: 'linear', height: 'easeInOutCubic' } }
        );
    }
    $('a.smooth-scroll').on('click', function(e) {
      r($(this).attr('href')), e.preventDefault();
    }),
      $('.reveal').length &&
        ((window.sr = ScrollReveal()),
        sr.isSupported() && document.documentElement.classList.add('sr'),
        sr.reveal('.reveal.scale-in', {
          origin: 'bottom',
          distance: '20px',
          duration: 1500,
          delay: 400,
          opacity: 1,
          scale: 1.1,
          easing: 'linear',
          reset: !1
        }),
        sr.reveal('.reveal.scale-out', {
          origin: 'bottom',
          distance: '20px',
          duration: 1500,
          delay: 400,
          opacity: 1,
          scale: 0.9,
          easing: 'linear',
          reset: !1
        }));
    var a = $('.collapse');
    a.on('show.bs.collapse', function() {
      $('.navbar-button').addClass('open');
    }),
      a.on('hide.bs.collapse', function() {
        $('.navbar-button').removeClass('open');
      });
    var s = $('.navbar-links li'),
      l = $('body'),
      c = $('.navbar'),
      u = $('.col-transform');
    function d(e, t, n, i, o) {
      $(e).each(function() {
        var e = this;
        window.setTimeout(function() {
          $(e).css({ '-webkit-transform': 'translate(' + t + ',0)', opacity: n, visibility: i });
        }, o),
          (o += 300);
      });
    }
    $('.show-info').click(function() {
      window.innerWidth > 990 &&
        (l.hasClass('show-content')
          ? (u.removeClass('col-md-6').addClass('col-md-12'),
            l.removeClass('show-content'),
            setTimeout(function() {
              c.removeClass('navbar-open');
            }, 900),
            d(s, '100%', '0', 'hidden', 600))
          : (u.removeClass('col-md-12').addClass('col-md-6'),
            l.addClass('show-content'),
            setTimeout(function() {
              c.addClass('navbar-open');
            }, 300),
            d(s.get().reverse(), '0', '1', 'visible', 600))),
        r($(this).data('href'));
    });
    var p = $('#page-slider');
    p.length &&
      p.owlCarousel({
        loop: !0,
        items: 1,
        autoplay: !0,
        animateOut: 'fadeOut',
        slideTransition: 'linear',
        autoplaySpeed: 8e3,
        mouseDrag: !1
      });
    var f = $('#page-youtube');
    !f.length ||
      e ||
      t ||
      f.YTPlayer({
        fitToBackground: !0,
        videoId: f.data('video-id'),
        playerVars: {
          modestbranding: 0,
          autoplay: 1,
          controls: 0,
          showinfo: 0,
          branding: 0,
          rel: 0,
          autohide: 0,
          start: 0
        }
      });
    var h = $('#page-video');
    !h.length ||
      e ||
      t ||
      new $.backgroundVideo($('body'), {
        align: 'centerXY',
        width: 960,
        height: 540,
        path: h.data('path'),
        filename: h.data('file'),
        types: ['mp4', 'ogg', 'webm']
      });
    var m = $('#kenburn-slider, body');
    $('#kenburn-slider').length &&
      m.vegas({
        slides: [
          { src: 'images/placeholder-2400-1600.jpg' },
          { src: 'images/placeholder-2400-1600.jpg' },
          { src: 'images/placeholder-2400-1600.jpg' }
        ],
        transition: 'fade',
        animation: 'random',
        transitionDuration: 4e3,
        delay: 8e3,
        timer: !1
      });
    var v = $('#page-particle');
    v.length && v.particleground({ dotColor: v.data('dot-color'), lineColor: v.data('line-color') });
    var g = $('#page-gradient');
    g.length &&
      g.gradientify({
        gradients: [
          { start: [35, 72, 200], stop: [192, 33, 114] },
          { start: [10, 155, 102], stop: [82, 98, 176] }
        ],
        fps: 80
      });
    var y = $('#page-rain');
    if (y.length && !e && !t) {
      var b = y[0];
      (b.onload = function() {
        var e = new RainyDay({ image: this, parentElement: $('.section-overlay')[0] });
        e.rain([[1, 2, 4e3]]),
          e.rain(
            [
              [3, 3, 1],
              [5, 5, 1],
              [6, 2, 1]
            ],
            100
          );
      }),
        (b.crossOrigin = 'anonymous'),
        (b.src = y.attr('src'));
    }
    var x = $('#page-amplitude'),
      w = $('#volume'),
      C = $('#playback');
    if (x.length) {
      var T = new SiriWave({
        speed: 0.05,
        amplitude: 1,
        container: x[0],
        autostart: !0,
        frequency: 4,
        height: 300,
        style: 'ios9'
      });
      if (e || t) w.hide(), C.hide();
      else {
        var S = new Howl({ src: ['audio/egotype.webm', 'audio/egotype.mp3'], autoplay: !0, loop: !0 });
        C.on('click', function() {
          C.hasClass('pause')
            ? (C.addClass('play').removeClass('pause'),
              C.find('i')
                .removeClass('icon-music-pause-button')
                .addClass('icon-music-play-button'),
              S.pause(),
              T.stop())
            : (C.removeClass('play').addClass('pause'),
              C.find('i')
                .removeClass('icon-music-play-button')
                .addClass('icon-music-pause-button'),
              S.play(),
              T.start());
        }),
          w.on('click', function() {
            w.hasClass('vol-1')
              ? (w.removeClass('vol-1').addClass('vol-0'),
                w
                  .find('i')
                  .removeClass('icon-music-volume-up')
                  .addClass('icon-music-volume-down'),
                S.volume(0))
              : (w.addClass('vol-1').removeClass('vol-0'),
                w
                  .find('i')
                  .removeClass('icon-music-volume-down')
                  .addClass('icon-music-volume-up'),
                S.volume(1));
          });
      }
    }
    var E,
      k,
      _,
      A,
      D,
      I,
      N = 0;
    function O() {
      window,
        window,
        (k.aspect = window.innerWidth / window.innerHeight),
        k.updateProjectionMatrix(),
        A.setSize(window.innerWidth, window.innerHeight);
    }
    window, window;
    var R = $('#particle-waves');
    R.length &&
      ((function(e, t) {
        (E = $('#particle-waves')[0]),
          ((k = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1e4)).position.z = 1e3),
          (_ = new THREE.Scene()),
          (D = []);
        for (
          var n = 2 * Math.PI,
            i = new THREE.SpriteCanvasMaterial({
              color: e,
              program: function(e) {
                e.beginPath(), e.arc(0, 0, 0.5, 0, n, !0), e.fill();
              }
            }),
            o = 0,
            r = 0;
          r < 50;
          r++
        )
          for (var a = 0; a < 50; a++)
            ((I = D[o++] = new THREE.Sprite(i)).position.x = 100 * r - 2500), (I.position.z = 100 * a - 2500), _.add(I);
        (A = new THREE.CanvasRenderer()).setPixelRatio(window.devicePixelRatio),
          A.setSize(window.innerWidth, window.innerHeight),
          A.setClearColor(t, 1),
          E.appendChild(A.domElement),
          window.addEventListener('resize', O, !1);
      })(R.data('wave-color'), R.data('bg-color')),
      (function e() {
        requestAnimationFrame(e),
          (function() {
            (k.position.x += 0.05 * (0 - k.position.x)),
              (k.position.y += 0.05 * (-0 - k.position.y)),
              k.lookAt(_.position);
            for (var e = 0, t = 0; t < 50; t++)
              for (var n = 0; n < 50; n++)
                ((I = D[e++]).position.y = 50 * Math.sin(0.3 * (t + N)) + 50 * Math.sin(0.5 * (n + N))),
                  (I.scale.x = I.scale.y = 4 * (Math.sin(0.3 * (t + N)) + 1) + 4 * (Math.sin(0.5 * (n + N)) + 1));
            A.render(_, k), (N += 0.1);
          })();
      })());
    var M = $('#waterpipe');
    M.length &&
      M.waterpipe({
        gradientStart: '#fa05fa',
        gradientEnd: '#6b71e3',
        smokeOpacity: 0.1,
        numCircles: 1,
        maxMaxRad: 'auto',
        minMaxRad: 'auto',
        minRadFactor: 0,
        iterations: 8,
        drawsPerFrame: 10,
        lineWidth: 2,
        speed: 20,
        bgColorInner: '#444444',
        bgColorOuter: '#000000'
      });
    var L = $('#page-ripple');
    !L.length ||
      e ||
      t ||
      (L.ripples({ resolution: 512, dropRadius: 10, perturbance: 0.04, interactive: !1 }),
      setInterval(function() {
        var e = L,
          t = Math.random() * e.outerWidth(),
          n = Math.random() * e.outerHeight(),
          i = 0.04 + 0.04 * Math.random();
        e.ripples('drop', t, n, 20, i);
      }, 3e3));
    var F = $('#page-starfield');
    F.length && F.starfield({ speed: 0, quantity: 500, ratio: 200 }),
      $('#alongway').length && (defaults = { dt: 0.015, time: 0 }),
      $('#raindrop').length &&
        (defaults = { length: 30, speed: 15, startColor: '#00a8ff', middleColor: '#00a8ff', endColor: 'white' }),
      $('#homing-particles').length &&
        (defaults = {
          BASE_PARTICLE_COUNT: 30,
          WATCH_INTERVAL: 1,
          THRESHOLD: 30,
          SPRING_AMOUNT: 0.01,
          LIMIT_RATE: 0.8,
          BACKGROUND_COLOR_HUE: 0,
          BACKGROUND_COLOR_SATURATION: 0,
          BACKGROUND_COLOR_LIGHTNESS: 0,
          BACKGROUND_COLOR_ALPHA: 0.3,
          PARTICLE_COLOR_SATURATION: 70,
          PARTICLE_COLOR_LIGHTNESS: 40
        }),
      $('#intertwined').length && (defaults = { dt: 0.005, time: 0 });
    var j = $('#page-square');
    j.length &&
      particlesJS('page-square', {
        particles: {
          number: { value: 20, density: { enable: !1, value_area: 800 } },
          color: { value: j.data('square-color') },
          opacity: { value: 0.5, random: !1, anim: { enable: !1, speed: 1, opacity_min: 0.1, sync: !1 } },
          shape: { type: 'edge', stroke: { width: 0, color: '#000000' } },
          size: { value: 58, random: !1, anim: { enable: !0, speed: 10, size_min: 40, sync: !1 } },
          line_linked: { enable: !1, distance: 200, color: '#ffffff', opacity: 1, width: 2 },
          move: {
            enable: !0,
            speed: 4,
            direction: 'right',
            random: !1,
            straight: !1,
            out_mode: 'out',
            bounce: !1,
            attract: { enable: !1, rotateX: 600, rotateY: 1200 }
          }
        },
        interactivity: {
          detect_on: 'window',
          events: { onhover: { enable: !1, mode: 'grab' }, onclick: { enable: !1, mode: 'push' }, resize: !0 },
          modes: {
            grab: { distance: 400, line_linked: { opacity: 1 } },
            bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
            repulse: { distance: 200, duration: 0.4 },
            push: { particles_nb: 4 },
            remove: { particles_nb: 2 }
          }
        },
        retina_detect: !0
      });
    var P = $('#page-bubbles');
    P.length &&
      particlesJS('page-bubbles', {
        particles: {
          number: { value: 30, density: { enable: !0, value_area: 800 } },
          color: { value: P.data('bubble-color') },
          opacity: { value: 0.3, random: !0, anim: { enable: !1, speed: 1, opacity_min: 0.1, sync: !1 } },
          size: { value: 58, random: !1, anim: { enable: !0, speed: 10, size_min: 40, sync: !1 } },
          line_linked: { enable: !1, distance: 200, color: '#ffffff', opacity: 1, width: 2 },
          move: {
            enable: !0,
            speed: 8,
            direction: 'none',
            random: !1,
            straight: !1,
            out_mode: 'out',
            bounce: !1,
            attract: { enable: !1, rotateX: 600, rotateY: 1200 }
          }
        },
        interactivity: {
          detect_on: 'canvas',
          events: { onhover: { enable: !1, mode: 'grab' }, onclick: { enable: !1, mode: 'push' }, resize: !0 },
          modes: {
            grab: { distance: 400, line_linked: { opacity: 1 } },
            bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
            repulse: { distance: 200, duration: 0.4 },
            push: { particles_nb: 4 },
            remove: { particles_nb: 2 }
          }
        },
        retina_detect: !0
      });
    var q = $('#page-fss');
    if (q.length)
      var H,
        z,
        B,
        W,
        U,
        Z,
        V,
        X = {
          width: 1.2,
          height: 1.2,
          depth: 0,
          segments: 16,
          slices: 8,
          xRange: 0.8,
          yRange: 0.1,
          zRange: 1,
          ambient: '#555555',
          diffuse: '#ffffff',
          speed: 0.002
        },
        K = {
          count: 2,
          xyScalar: 1,
          zOffset: 100,
          ambient: q.data('ambient-color'),
          diffuse: q.data('diffuse-color'),
          speed: 0.002,
          gravity: 500,
          dampening: 0.95,
          minLimit: 10,
          maxLimit: null,
          minDistance: 20,
          maxDistance: 800,
          autopilot: !0,
          draw: !1,
          bounds: FSS.Vector3.create(),
          step: FSS.Vector3.create(Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1), Math.randomInRange(0.2, 1))
        },
        Y = Date.now(),
        G = FSS.Vector3.create(),
        J = FSS.Vector3.create(),
        Q = q[0],
        ee = q[0];
    function te() {
      var e, t;
      for (
        B.remove(W),
          z.clear(),
          U = new FSS.Plane(X.width * z.width, X.height * z.height, X.segments, X.slices),
          Z = new FSS.Material(X.ambient, X.diffuse),
          W = new FSS.Mesh(U, Z),
          B.add(W),
          e = U.vertices.length - 1;
        e >= 0;
        e--
      )
        ((t = U.vertices[e]).anchor = FSS.Vector3.clone(t.position)),
          (t.step = FSS.Vector3.create(
            Math.randomInRange(0.2, 1),
            Math.randomInRange(0.2, 1),
            Math.randomInRange(0.2, 1)
          )),
          (t.time = Math.randomInRange(0, Math.PIM2));
    }
    function ne(e, t) {
      z.setSize(e, t), FSS.Vector3.set(G, z.halfWidth, z.halfHeight), te();
    }
    function ie() {
      var e, t, n, i;
      if ((z.render(B), K.draw))
        for (e = B.lights.length - 1; e >= 0; e--)
          (t = (i = B.lights[e]).position[0]),
            (n = i.position[1]),
            (z.context.lineWidth = 0.5),
            z.context.beginPath(),
            z.context.arc(t, n, 10, 0, Math.PIM2),
            (z.context.strokeStyle = i.ambientHex),
            z.context.stroke(),
            z.context.beginPath(),
            z.context.arc(t, n, 4, 0, Math.PIM2),
            (z.context.fillStyle = i.diffuseHex),
            z.context.fill();
    }
    q.length &&
      ((V = new FSS.CanvasRenderer()),
      z && ee.removeChild(z.element),
      (z = V).setSize(Q.offsetWidth, Q.offsetHeight),
      ee.appendChild(z.element),
      (B = new FSS.Scene()),
      te(),
      (function() {
        var e, t;
        for (e = B.lights.length - 1; e >= 0; e--) B.remove((t = B.lights[e]));
        for (z.clear(), e = 0; e < K.count; e++)
          ((t = new FSS.Light(K.ambient, K.diffuse)).ambientHex = t.ambient.format()),
            (t.diffuseHex = t.diffuse.format()),
            B.add(t),
            (t.mass = Math.randomInRange(0.5, 1)),
            (t.velocity = FSS.Vector3.create()),
            (t.acceleration = FSS.Vector3.create()),
            (t.force = FSS.Vector3.create()),
            (t.ring = document.createElementNS(FSS.SVGNS, 'circle')),
            t.ring.setAttributeNS(null, 'stroke', t.ambientHex),
            t.ring.setAttributeNS(null, 'stroke-width', '0.5'),
            t.ring.setAttributeNS(null, 'fill', 'none'),
            t.ring.setAttributeNS(null, 'r', '10'),
            (t.core = document.createElementNS(FSS.SVGNS, 'circle')),
            t.core.setAttributeNS(null, 'fill', t.diffuseHex),
            t.core.setAttributeNS(null, 'r', '4');
      })(),
      window.addEventListener('resize', function() {
        ne(Q.offsetWidth, Q.offsetHeight), ie();
      }),
      ne(Q.offsetWidth, Q.offsetHeight),
      (function e() {
        (H = Date.now() - Y),
          (function() {
            var e,
              t,
              n,
              i,
              o,
              r,
              a,
              s = X.depth / 2;
            for (
              FSS.Vector3.copy(K.bounds, G),
                FSS.Vector3.multiplyScalar(K.bounds, K.xyScalar),
                FSS.Vector3.setZ(J, K.zOffset),
                K.autopilot &&
                  ((e = Math.sin(K.step[0] * H * K.speed)),
                  (t = Math.cos(K.step[1] * H * K.speed)),
                  FSS.Vector3.set(J, K.bounds[0] * e, K.bounds[1] * t, K.zOffset)),
                i = B.lights.length - 1;
              i >= 0;
              i--
            ) {
              (o = B.lights[i]), FSS.Vector3.setZ(o.position, K.zOffset);
              var l = Math.clamp(FSS.Vector3.distanceSquared(o.position, J), K.minDistance, K.maxDistance),
                c = (K.gravity * o.mass) / l;
              FSS.Vector3.subtractVectors(o.force, J, o.position),
                FSS.Vector3.normalise(o.force),
                FSS.Vector3.multiplyScalar(o.force, c),
                FSS.Vector3.set(o.acceleration),
                FSS.Vector3.add(o.acceleration, o.force),
                FSS.Vector3.add(o.velocity, o.acceleration),
                FSS.Vector3.multiplyScalar(o.velocity, K.dampening),
                FSS.Vector3.limit(o.velocity, K.minLimit, K.maxLimit),
                FSS.Vector3.add(o.position, o.velocity);
            }
            for (r = U.vertices.length - 1; r >= 0; r--)
              (a = U.vertices[r]),
                (e = Math.sin(a.time + a.step[0] * H * X.speed)),
                (t = Math.cos(a.time + a.step[1] * H * X.speed)),
                (n = Math.sin(a.time + a.step[2] * H * X.speed)),
                FSS.Vector3.set(
                  a.position,
                  X.xRange * U.segmentWidth * e,
                  X.yRange * U.sliceHeight * t,
                  X.zRange * s * n - s
                ),
                FSS.Vector3.add(a.position, a.anchor);
            U.dirty = !0;
          })(),
          ie(),
          requestAnimationFrame(e);
      })()),
      $('#photoswipe').load('js/plugins/photoswipe/include/photoswipe.html', function() {
        !(function(e) {
          for (
            var t = function(e) {
                (e = e || window.event).preventDefault ? e.preventDefault() : (e.returnValue = !1);
                var t = (function e(t, n) {
                  return t && (n(t) ? t : e(t.parentNode, n));
                })(e.target || e.srcElement, function(e) {
                  return e.tagName && 'FIGURE' === e.tagName.toUpperCase();
                });
                if (t) {
                  for (var i, o = t.parentNode, r = t.parentNode.childNodes, a = r.length, s = 0, l = 0; l < a; l++)
                    if (1 === r[l].nodeType) {
                      if (r[l] === t) {
                        i = s;
                        break;
                      }
                      s++;
                    }
                  return i >= 0 && n(i, o), !1;
                }
              },
              n = function(e, t, n, i) {
                var o,
                  r,
                  a = document.querySelectorAll('.pswp')[0];
                if (
                  ((r = (function(e) {
                    for (var t, n, i, o, r = e.childNodes, a = r.length, s = [], l = 0; l < a; l++)
                      1 === (t = r[l]).nodeType &&
                        ((i = (n = t.children[0]).getAttribute('data-size').split('x')),
                        (o = { src: n.getAttribute('href'), w: parseInt(i[0], 10), h: parseInt(i[1], 10) }),
                        t.children.length > 1 &&
                          (o.title = '<h4>' + t.children[1].innerHTML + '</h4><p>' + t.children[2].innerHTML + '</p>'),
                        n.children.length > 0 && (o.msrc = n.children[0].getAttribute('src')),
                        (o.el = t),
                        s.push(o));
                    return s;
                  })(t)),
                  (o = {
                    barsSize: { top: 64, bottom: 'auto', padding: '10px' },
                    bgOpacity: 0.98,
                    closeEl: !0,
                    captionEl: !0,
                    fullscreenEl: !1,
                    zoomEl: !1,
                    shareEl: !1,
                    counterEl: !0,
                    arrowEl: !0,
                    preloaderEl: !0,
                    galleryUID: t.getAttribute('data-pswp-uid'),
                    getThumbBoundsFn: function(e) {
                      var t = r[e].el.getElementsByTagName('img')[0],
                        n = window.pageYOffset || document.documentElement.scrollTop,
                        i = t.getBoundingClientRect();
                      return { x: i.left, y: i.top + n, w: i.width };
                    }
                  }),
                  i)
                )
                  if (o.galleryPIDs) {
                    for (var s = 0; s < r.length; s++)
                      if (r[s].pid == e) {
                        o.index = s;
                        break;
                      }
                  } else o.index = parseInt(e, 10) - 1;
                else o.index = parseInt(e, 10);
                isNaN(o.index) ||
                  (n && (o.showAnimationDuration = 0), new PhotoSwipe(a, PhotoSwipeUI_Default, r, o).init());
              },
              i = document.querySelectorAll('.gallery'),
              o = 0,
              r = i.length;
            o < r;
            o++
          )
            i[o].setAttribute('data-pswp-uid', o + 1), (i[o].onclick = t);
          var a = (function() {
            var e = window.location.hash.substring(1),
              t = {};
            if (e.length < 5) return t;
            for (var n = e.split('&'), i = 0; i < n.length; i++)
              if (n[i]) {
                var o = n[i].split('=');
                o.length < 2 || (t[o[0]] = o[1]);
              }
            return t.gid && (t.gid = parseInt(t.gid, 10)), t;
          })();
          a.pid && a.gid && n(a.pid, i[a.gid - 1], !0, !0);
        })();
      });
    var oe = $('textarea');
    oe.length &&
      oe[0].addEventListener('keydown', function() {
        var e = this;
        setTimeout(function() {
          (e.style.cssText = 'height:50px; padding:15px'), (e.style.cssText = 'height:' + e.scrollHeight + 'px');
        }, 0);
      });
    var re = $('.form-control');
    re.on('focus', function() {
      $(this)
        .parent()
        .addClass('focus');
    }),
      re.on('blur', function() {
        $(this)
          .parent()
          .removeClass('focus');
      });
    var ae = $('#mc-form');
    function se(e) {
      var t,
        n,
        i = new google.maps.LatLng(40.72020106, -73.97163391),
        o = [
          [
            '<div class="wrapper"><h4>Kounter Office</h4><h5>Opening Hours</h5><p>Mo \u2013 Fr: 08:30 \u2013 20:00</p></div>',
            40.7486332,
            -73.9797129
          ],
          [
            '<div class="wrapper"><h4>Kounter Agency</h4><h5>Opening Hours</h5><p>Mo \u2013 Fr: 08:30 \u2013 20:00</p></div>',
            40.6681,
            -73.9448
          ]
        ],
        r = new google.maps.Size(50, 49),
        a = {
          zoom: 11,
          center: i,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          mapTypeControl: !1,
          streetViewControl: !1,
          scrollwheel: !1,
          styles: [
            { featureType: 'road', elementType: 'geometry', stylers: [{ visibility: 'simplified' }] },
            { featureType: 'poi', elementType: 'label', stylers: [{ visibility: 'off' }] },
            { featureType: 'all', stylers: [{ saturation: -100 }, { gamma: 0.9 }] }
          ]
        },
        s = new google.maps.Map(e[0], a),
        l = new google.maps.InfoWindow();
      google.maps.event.addListener(l, 'domready', function() {
        var e = $('.gm-style-iw').prev();
        e.children(':nth-child(2)').css({ 'box-shadow': 'none', 'background-color': 'rgba(0, 0, 0, 0.1)' }),
          e
            .children(':nth-child(3)')
            .find('div')
            .children()
            .css({ 'box-shadow': 'none', 'border-top-color': 'rgba(0, 0, 0, 0.1)' });
      });
      var c = { url: 'images/marker.png', scaledSize: r };
      for (n = 0; n < o.length; n++) {
        var u = o[n][0];
        (t = new google.maps.Marker({ position: new google.maps.LatLng(o[n][1], o[n][2]), map: s, icon: c })),
          google.maps.event.addListener(
            t,
            'click',
            (function(e) {
              return function() {
                l.setContent(e), l.open(s, this);
              };
            })(u)
          );
      }
    }
    ae.length &&
      ae.ajaxChimp({
        callback: function(e) {
          var t = $('#message-newsletter');
          t.removeClass('error'),
            ae.find('.form-group').removeClass('error'),
            'error' === e.result
              ? (ae.find('.input-group').addClass('error'), t.addClass('error'))
              : ae
                  .find('.form-control')
                  .fadeIn()
                  .val(''),
            t.slideDown('slow', 'swing'),
            setTimeout(function() {
              t.slideUp('slow', 'swing');
            }, 1e4);
        },
        url: ''
      });
    var le = $('#map');
    if (
      (le.length &&
        (google.maps.event.addDomListener(window, 'load', se(le)),
        google.maps.event.addDomListener(window, 'resize', se(le))),
      navigator.userAgent.match(/IEMobile\/10\.0/))
    ) {
      var ce = document.createElement('style');
      ce.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}')),
        document.querySelector('head').appendChild(ce);
    }
  });
